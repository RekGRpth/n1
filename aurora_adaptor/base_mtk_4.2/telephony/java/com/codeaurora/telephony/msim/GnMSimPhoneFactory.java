package com.codeaurora.telephony.msim;

import com.android.internal.telephony.PhoneFactory;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.gemini.GeminiPhone;
import android.telephony.TelephonyManager;

public class GnMSimPhoneFactory {
    public static int getDefaultSubscription() {
        return 0;
    }

    public static Phone getPhone(int subscription) {
//        return null;
    	Phone phone = PhoneFactory.getDefaultPhone();
        return ((GeminiPhone)phone).getPhonebyId(subscription);
    }
}