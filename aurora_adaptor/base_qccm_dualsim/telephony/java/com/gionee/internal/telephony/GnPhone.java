package com.gionee.internal.telephony;

import android.os.RemoteException;
import android.os.ServiceManager;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.RILConstants;
import com.android.internal.telephony.MSimConstants;
import com.android.internal.telephony.Phone;

/**
 * GnPhone replace for the com.android.internal.telephony.Phone
 * @author guoyangxu
 *
 */
public class GnPhone {
    public static ITelephony phone = ITelephony.Stub.asInterface(ServiceManager.checkService("phone"));
    
    public static String STATE_KEY = "state";//Phone.STATE_KEY;
    public static String FEATURE_ENABLE_MMS = Phone.FEATURE_ENABLE_MMS;
    
    public static String REASON_NO_SUCH_PDP = "noSuchPdp";//com.android.internal.telephony.Phone.REASON_NO_SUCH_PDP;
    public static String REASON_APN_FAILED = Phone.REASON_APN_FAILED;
    public static String REASON_RADIO_TURNED_OFF = Phone.REASON_RADIO_TURNED_OFF;
    public static String REASON_VOICE_CALL_ENDED = Phone.REASON_VOICE_CALL_ENDED;
    public static String APN_TYPE_MMS = "mms";//Phone.APN_TYPE_MMS;
    public static Object APN_TYPE_ALL = "*";//Phone.APN_TYPE_ALL;
    
    /**
     * Return codes for <code>enableApnType()</code>
     */
    public static final int APN_ALREADY_ACTIVE     = 0;//Phone.APN_ALREADY_ACTIVE;
    public static final int APN_REQUEST_STARTED    = 1;//Phone.APN_REQUEST_STARTED;
    public static final int APN_TYPE_NOT_AVAILABLE = 2;//Phone.APN_TYPE_NOT_AVAILABLE;
    public static final int APN_REQUEST_FAILED     = 3;//Phone.APN_REQUEST_FAILED;
    public static final int APN_ALREADY_INACTIVE   = 4;//Phone.APN_ALREADY_INACTIVE;
    
    // Used for preferred network type
    // Note NT_* substitute RILConstants.NETWORK_MODE_* above the Phone
    public static int NT_MODE_WCDMA_PREF   = RILConstants.NETWORK_MODE_WCDMA_PREF;
    public static int NT_MODE_GSM_ONLY     = RILConstants.NETWORK_MODE_GSM_ONLY;
    public static int NT_MODE_WCDMA_ONLY   = RILConstants.NETWORK_MODE_WCDMA_ONLY;
    public static int NT_MODE_GSM_UMTS     = RILConstants.NETWORK_MODE_GSM_UMTS;

    public static int NT_MODE_CDMA         = RILConstants.NETWORK_MODE_CDMA;

    public static int NT_MODE_CDMA_NO_EVDO = RILConstants.NETWORK_MODE_CDMA_NO_EVDO;
    public static int NT_MODE_EVDO_NO_CDMA = RILConstants.NETWORK_MODE_EVDO_NO_CDMA;
    public static int NT_MODE_GLOBAL       = RILConstants.NETWORK_MODE_GLOBAL;

    public static int NT_MODE_LTE_ONLY     = RILConstants.NETWORK_MODE_LTE_ONLY;
    public static int PREFERRED_NT_MODE    = RILConstants.PREFERRED_NETWORK_MODE;

    /**
     * slot ID for GEMINI
     */
    public static final int GEMINI_SIM_1 = 0;
    public static final int GEMINI_SIM_2 = 1;
    public static final int GEMINI_SIP_CALL = -1; //MTK added for SIP call
    /**
     * GEMINI_SIM_ID_KEY
	 *  card slot Id key is "subscription" when use the Qualcomm multi sim solution.
     */
    public static String GEMINI_SIM_ID_KEY = MSimConstants.SUBSCRIPTION_KEY;//MTK is simId
    public static String MULTI_SIM_ID_KEY = "simid";
    public static String GEMINI_DEFAULT_SIM_PROP = "persist.radio.default_sim";
    
    public static String APN_TYPE_DM = "dm";
    public static String APN_TYPE_WAP = "wap";
    public static String APN_TYPE_NET = "net";
    public static String APN_TYPE_CMMAIL = "cmmail";
    public static String APN_TYPE_TETHERING = "tethering";
	
    /** UNKNOWN, invalid value */
    public static final int SIM_INDICATOR_UNKNOWN = -1;
    
    /** ABSENT, no SIM/USIM card inserted for this phone */
    public static final int SIM_INDICATOR_ABSENT = 0;
    
    /** RADIOOFF,  has SIM/USIM inserted but not in use . */
    public static final int SIM_INDICATOR_RADIOOFF = 1;
    
    /** LOCKED,  has SIM/USIM inserted and the SIM/USIM has been locked. */
    public static final int SIM_INDICATOR_LOCKED = 2;
    
    /** INVALID : has SIM/USIM inserted and not be locked but failed to register to the network. */
    public static final int SIM_INDICATOR_INVALID = 3; 
    
    /** SEARCHING : has SIM/USIM inserted and SIM/USIM state is Ready and is searching for network. */
    public static final int SIM_INDICATOR_SEARCHING = 4; 
    
    /** NORMAL = has SIM/USIM inserted and in normal service(not roaming and has no data connection). */
    public static final int SIM_INDICATOR_NORMAL = 5; 
    
    /** ROAMING : has SIM/USIM inserted and in roaming service(has no data connection). */  
    public static final int SIM_INDICATOR_ROAMING = 6; 
    
    /** CONNECTED : has SIM/USIM inserted and in normal service(not roaming) and data connected. */
    public static final int SIM_INDICATOR_CONNECTED = 7; 
    
    /** ROAMINGCONNECTED = has SIM/USIM inserted and in roaming service(not roaming) and data connected.*/
    public static final int SIM_INDICATOR_ROAMINGCONNECTED = 8;
    
    public static final String GEMINI_DEFAULT_SIM_MODE = "persist.radio.default_sim_mode";

    public static final String GEMINI_GPRS_TRANSFER_TYPE = "gemini.gprs.transfer.type";
    
    public static boolean isTestIccCard() {
//        return phone.isTestIccCard();
        return false;
    }
    
    public static boolean isSimInsert() {       
        try {
           return phone.hasIccCard();
        } catch (RemoteException e) {
            e.printStackTrace();
            return false;
        }    
    }
}
