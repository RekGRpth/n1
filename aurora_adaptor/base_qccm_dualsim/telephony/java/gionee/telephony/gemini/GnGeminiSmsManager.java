
package gionee.telephony.gemini;

import static android.telephony.SmsMessage.ENCODING_16BIT;
import static android.telephony.SmsMessage.ENCODING_7BIT;
import static android.telephony.SmsMessage.ENCODING_UNKNOWN;
import static android.telephony.SmsMessage.MAX_USER_DATA_BYTES;
import static android.telephony.SmsMessage.MAX_USER_DATA_SEPTETS;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.android.internal.telephony.ISms;
import gionee.telephony.GnSmsMemoryStatus;

import android.app.PendingIntent;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.MSimSmsManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.telephony.SmsMemoryStatus;
import android.telephony.MSimTelephonyManager;
import android.telephony.MSimSmsManager;
import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.SmsHeader;
import com.android.internal.telephony.cdma.sms.BearerData;
import com.android.internal.telephony.cdma.sms.CdmaSmsAddress;
import com.android.internal.telephony.cdma.sms.SmsEnvelope;
import com.android.internal.telephony.cdma.sms.UserData;
import com.android.internal.telephony.EncodeException;
import com.android.internal.telephony.GsmAlphabet;
import java.lang.reflect.Method;

public class GnGeminiSmsManager {
    public static String TAG = "GnGeminiSmsManager";
    private static int TIMESTAMP_LENGTH = 7;  // See TS 23.040 9.2.3.11
    public static MSimSmsManager msimsms=MSimSmsManager.getDefault();
    public static void sendTextMessageGemini(String paramString1, String paramString2,
            String paramString3, int paramInt, PendingIntent paramPendingIntent1,
            PendingIntent paramPendingIntent2) {
        if (!isValidParameters(paramString1, paramString3, paramPendingIntent1)) {
            return;
        }

        String str = getSmsServiceName(paramInt);
        try {
            ISms localISms = ISms.Stub.asInterface(ServiceManager.getService(str));
            if (localISms != null)
              {
      //aurora change zhouxiaobing 20131202 start     
         
            if(android.os.Build.VERSION.SDK_INT<18)
                {
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("com.android.internal.telephony.ISms");
                  Method method=sPolicy.getMethod("sendText", String.class,String.class,String.class,PendingIntent.class,PendingIntent.class);
                  method.invoke(localISms,paramString1, paramString2, paramString3, paramPendingIntent1,
                        paramPendingIntent2);

                }
            else
               {
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("com.android.internal.telephony.ISms");
                  Method method=sPolicy.getMethod("sendText", String.class,String.class,String.class,String.class,PendingIntent.class,PendingIntent.class);
                  method.invoke(localISms,"com.android.mms",paramString1, paramString2, paramString3, paramPendingIntent1,
                        paramPendingIntent2);
               }
         
     //           localISms.sendText(paramString1, paramString2, paramString3, paramPendingIntent1,
     //                   paramPendingIntent2);
     //aurora change zhouxiaobing 20131202 end 
              }
        } catch (Exception localRemoteException) {
        }
     // msimsms.sendTextMessage(paramString1,paramString2,paramString3,paramPendingIntent1,paramPendingIntent2);
    }

    /**
     * sendMultipartTextMessageGemini is for other operators except of CMCC
     * @param mDest
     * @param mServiceCenter
     * @param messages
     * @param slotId
     * @param sentIntents
     * @param deliveryIntents
     */
    public static void sendMultipartTextMessageGemini(String mDest, String mServiceCenter,
            ArrayList<String> messages, int slotId, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
  //          SmsManager.getDefault().sendMultipartTextMessage(mDest, mServiceCenter, messages, 
  //              sentIntents, deliveryIntents, -1);
             MSimSmsManager.getDefault().sendMultipartTextMessage(mDest, mServiceCenter, messages, 
                sentIntents, deliveryIntents, slotId/*mSimId*/);
    }

    public static int copyTextMessageToIccCardGemini(String scAddress, String mAddress,
            List<String> messages, int smsStatus, long timeStamp, int slotId) {
        
        boolean ret = true;
        for (String message : messages) {
            if (SmsManager.STATUS_ON_ICC_READ != smsStatus) {
                //save the sent or unsend status message to the sim all change the type is sent message.
                SmsMessage.SubmitPdu pdu = SmsMessage.getSubmitPdu(null, mAddress, message, false);
                ret &= MSimSmsManager.getDefault()
                    .copyMessageToIcc(null, pdu.encodedMessage, SmsManager.STATUS_ON_ICC_SENT,slotId);
            } else {
                //save the read status message which is the received.  
                byte[] pdu = getDeliveryPdu(null, mAddress, message, timeStamp, slotId);
                ret &= MSimSmsManager.getDefault()
                    .copyMessageToIcc(null, pdu, SmsManager.STATUS_ON_ICC_READ,slotId);
            }
            if (!ret) {
                break;
            }
        }
        return ret ? 0 : -1;
    }
    
    /*--------------Methods for encoding sms delivery pdu on sim card begin-------------*/

    /**
     * Generate a Delivery PDU byte array. see getSubmitPdu for reference.
     */
    public static byte[] getDeliveryPdu(String scAddress, String destinationAddress, String message,
            long date, int subscription) {
        if (isCDMAActive(subscription)) {
            return getCDMADeliveryPdu(scAddress, destinationAddress, message, date);
        } else {
            return getDeliveryPdu(scAddress, destinationAddress, message, date, null,
                    ENCODING_UNKNOWN);
        }
    }
    
    /**
     * Generate a Delivery PDU byte array. see getSubmitPdu for reference.
     */
    public static byte[] getDeliveryPdu(String scAddress, String destinationAddress, String message,
            long date, byte[] header, int encoding) {
        // Perform null parameter checks.
        if (message == null || destinationAddress == null) {
            return null;
        }

        // MTI = SMS-DELIVERY, UDHI = header != null
        byte mtiByte = (byte)(0x00 | (header != null ? 0x40 : 0x00));
        ByteArrayOutputStream bo = getDeliveryPduHeader(destinationAddress, mtiByte);
        // User Data (and length)
        byte[] userData;
        if (encoding == ENCODING_UNKNOWN) {
            // First, try encoding it with the GSM alphabet
            encoding = ENCODING_7BIT;
        }
        try {
            if (encoding == ENCODING_7BIT) {
                userData = GsmAlphabet.stringToGsm7BitPackedWithHeader(message, header, 0, 0);
            } else { //assume UCS-2
                try {
                    userData = encodeUCS2(message, header);
                } catch (UnsupportedEncodingException uex) {
                    Log.e("GSM", "Implausible UnsupportedEncodingException ",
                            uex);
                    return null;
                }
            }
        } catch (EncodeException ex) {
            // Encoding to the 7-bit alphabet failed. Let's see if we can
            // encode it as a UCS-2 encoded message
            try {
                userData = encodeUCS2(message, header);
                encoding = ENCODING_16BIT;
            } catch (UnsupportedEncodingException uex) {
                Log.e("GSM", "Implausible UnsupportedEncodingException ",
                            uex);
                return null;
            }
        }

        if (encoding == ENCODING_7BIT) {
            if ((0xff & userData[0]) > MAX_USER_DATA_SEPTETS) {
                // Message too long
                return null;
            }
            bo.write(0x00);
        } else { //assume UCS-2
            if ((0xff & userData[0]) > MAX_USER_DATA_BYTES) {
                // Message too long
                return null;
            }
            // TP-Data-Coding-Scheme
            // Class 3, UCS-2 encoding, uncompressed
            bo.write(0x0b);
        }
        byte[] timestamp = getTimestamp(date);
        bo.write(timestamp, 0, timestamp.length);

        bo.write(userData, 0, userData.length);
        return bo.toByteArray();
    }
    
    private static byte[] encodeUCS2(String message, byte[] header)
        throws UnsupportedEncodingException {
        byte[] userData, textPart;
        textPart = message.getBytes("utf-16be");

        if (header != null) {
            // Need 1 byte for UDHL
            userData = new byte[header.length + textPart.length + 1];

            userData[0] = (byte)header.length;
            System.arraycopy(header, 0, userData, 1, header.length);
            System.arraycopy(textPart, 0, userData, header.length + 1, textPart.length);
        }
        else {
            userData = textPart;
        }
        byte[] ret = new byte[userData.length+1];
        ret[0] = (byte) (userData.length & 0xff );
        System.arraycopy(userData, 0, ret, 1, userData.length);
        return ret;
    }

    private static byte[] getTimestamp(long time) {
        // See TS 23.040 9.2.3.11
        byte[] timestamp = new byte[TIMESTAMP_LENGTH];
        SimpleDateFormat sdf = new SimpleDateFormat("yyMMddkkmmss:Z", Locale.US);
        String[] date = sdf.format(time).split(":");
        // generate timezone value
        String timezone = date[date.length - 1];
        String signMark = timezone.substring(0, 1);
        int hour = Integer.parseInt(timezone.substring(1, 3));
        int min = Integer.parseInt(timezone.substring(3));
        int timezoneValue = hour * 4 + min / 15;
        // append timezone value to date[0] (time string)
        String timestampStr = date[0] + timezoneValue;

        int digitCount = 0;
        for (int i = 0; i < timestampStr.length(); i++) {
            char c = timestampStr.charAt(i);
            int shift = ((digitCount & 0x01) == 1) ? 4 : 0;
            timestamp[(digitCount >> 1)] |= (byte)((charToBCD(c) & 0x0F) << shift);
            digitCount++;
        }

        if (signMark.equals("-")) {
            timestamp[timestamp.length - 1] = (byte) (timestamp[timestamp.length - 1] | 0x08);
        }

        return timestamp;
    }
        
    private static int charToBCD(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        } else {
            throw new RuntimeException ("invalid char for BCD " + c);
        }
    }
    
    private static boolean isCDMAActive(int subscription) {
        boolean isCDMA = false;
        int activePhone = MSimTelephonyManager.getDefault().isMultiSimEnabled()
                ? MSimTelephonyManager.getDefault().getCurrentPhoneType(subscription)
                        : TelephonyManager.getDefault().getPhoneType();
        if (TelephonyManager.PHONE_TYPE_CDMA == activePhone) {
            isCDMA = true;
        }
        return isCDMA;
    }
    
    public static byte[] getCDMADeliveryPdu(String scAddress, String destinationAddress,
            String message, long date) {
        // Perform null parameter checks.
        if (message == null || destinationAddress == null) {
            Log.d(TAG, "getCDMADeliveryPdu,message =null");
            return null;
        }

        // according to submit pdu encoding as written in privateGetSubmitPdu

        // MTI = SMS-DELIVERY, UDHI = header != null
        byte[] header = null;
        byte mtiByte = (byte) (0x00 | (header != null ? 0x40 : 0x00));
        ByteArrayOutputStream headerStream = getDeliveryPduHeader(destinationAddress, mtiByte);

        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(MAX_USER_DATA_BYTES + 40);

        DataOutputStream dos = new DataOutputStream(byteStream);
        // int status,Status of message. See TS 27.005 3.1, "<stat>"

        /* 0 = "REC UNREAD" */
        /* 1 = "REC READ" */
        /* 2 = "STO UNSENT" */
        /* 3 = "STO SENT" */

        try {
            // int uTeleserviceID;
            int uTeleserviceID = 0x1004;//SmsEnvelope.TELESERVICE_CT_WAP;// int
            dos.writeInt(uTeleserviceID);

            // unsigned char bIsServicePresent
            byte bIsServicePresent = 0;// byte
            dos.writeInt(bIsServicePresent);

            // uServicecategory
            int uServicecategory = 0;// int
            dos.writeInt(uServicecategory);

            // RIL_CDMA_SMS_Address
            // digit_mode
            // number_mode
            // number_type
            // number_plan
            // number_of_digits
            // digits[]
            CdmaSmsAddress destAddr = CdmaSmsAddress.parse(PhoneNumberUtils
                    .cdmaCheckAndProcessPlusCode(destinationAddress));
            if (destAddr == null)
                return null;
            dos.writeByte(destAddr.digitMode);// int
            dos.writeByte(destAddr.numberMode);// int
            dos.writeByte(destAddr.ton);// int
            Log.d(TAG, "message type=" + destAddr.ton + "destination add=" + destinationAddress
                    + "message=" + message);
            dos.writeByte(destAddr.numberPlan);// int
            dos.writeByte(destAddr.numberOfDigits);// byte
            dos.write(destAddr.origBytes, 0, destAddr.origBytes.length); // digits

            // RIL_CDMA_SMS_Subaddress
            // Subaddress is not supported.
            dos.writeByte(0); // subaddressType int
            dos.writeByte(0); // subaddr_odd byte
            dos.writeByte(0); // subaddr_nbr_of_digits byte

            SmsHeader smsHeader = new SmsHeader().fromByteArray(headerStream.toByteArray());
            UserData uData = new UserData();
            uData.payloadStr = message;
            // uData.userDataHeader = smsHeader;
            uData.msgEncodingSet = true;
            uData.msgEncoding = UserData.ENCODING_UNICODE_16;

            BearerData bearerData = new BearerData();
            bearerData.messageType = BearerData.MESSAGE_TYPE_DELIVER;

            bearerData.deliveryAckReq = false;
            bearerData.userAckReq = false;
            bearerData.readAckReq = false;
            bearerData.reportReq = false;

            bearerData.userData = uData;

            byte[] encodedBearerData = BearerData.encode(bearerData);
            if (null != encodedBearerData) {
                // bearer data len
                dos.writeByte(encodedBearerData.length);// int
                Log.d(TAG, "encodedBearerData length=" + encodedBearerData.length);

                // aBearerData
                dos.write(encodedBearerData, 0, encodedBearerData.length);
            } else {
                dos.writeByte(0);
            }

        } catch (IOException e) {
            Log.e(TAG, "Error writing dos", e);
        } finally {
            try {
                if (null != byteStream) {
                    byteStream.close();
                }

                if (null != dos) {
                    dos.close();
                }

                if (null != headerStream) {
                    headerStream.close();
                }
            } catch (IOException e) {
                Log.e(TAG, "Error close dos", e);
            }
        }

        return byteStream.toByteArray();
    }
    
    private static ByteArrayOutputStream getDeliveryPduHeader(
            String destinationAddress, byte mtiByte) {
        ByteArrayOutputStream bo = new ByteArrayOutputStream(
                MAX_USER_DATA_BYTES + 40);
        bo.write(mtiByte);

        byte[] daBytes;
        daBytes = PhoneNumberUtils.networkPortionToCalledPartyBCD(destinationAddress);

        // destination address length in BCD digits, ignoring TON byte and pad
        // TODO Should be better.
        bo.write((daBytes.length - 1) * 2
                - ((daBytes[daBytes.length - 1] & 0xf0) == 0xf0 ? 1 : 0));

        // destination address
        bo.write(daBytes, 0, daBytes.length);

        // TP-Protocol-Identifier
        bo.write(0);
        return bo;
    }
    
    /*---------------Methods for encoding sms delivery pdu on sim card end ----*/    

    private static String getSmsServiceName(int paramInt) {
        if (paramInt == 0)
            return "isms";
        if (paramInt == 1) {
            return "isms2";
        }
        return null;
    }

    private static boolean isValidParameters(String paramString1, String paramString2,
            PendingIntent paramPendingIntent) {
        ArrayList localArrayList1 = new ArrayList();

        ArrayList localArrayList2 = new ArrayList();

        localArrayList1.add(paramPendingIntent);
        localArrayList2.add(paramString2);

        if (TextUtils.isEmpty(paramString2)) {
            throw new IllegalArgumentException("Invalid message body");
        }

        return isValidParameters(paramString1, localArrayList2, localArrayList1);
    }

    private static boolean isValidParameters(String paramString, ArrayList<String> paramArrayList,
            ArrayList<PendingIntent> paramArrayList1) {
        if (!isValidSmsDestinationAddress(paramString)) {
            for (int i = 0; i < paramArrayList1.size(); ++i) {
                PendingIntent localPendingIntent = (PendingIntent) paramArrayList1.get(i);
                if (localPendingIntent == null)
                    continue;
                try {
                    localPendingIntent.send(1);
                } catch (PendingIntent.CanceledException localCanceledException) {
                }
            }
            Log.d("SMS", "Invalid destinationAddress: " + paramString);
            return false;
        }

        if (TextUtils.isEmpty(paramString)) {
            throw new IllegalArgumentException("Invalid destinationAddress");
        }
        if ((paramArrayList == null) || (paramArrayList.size() < 1)) {
            throw new IllegalArgumentException("Invalid message body");
        }

        return true;
    }

    private static boolean isValidSmsDestinationAddress(String paramString) {
        String str = PhoneNumberUtils.extractNetworkPortion(paramString);
        if (str == null) {
            return true;
        }
        return str.length() == paramString.length();
    }
    
    public static boolean copyMessageToIccGemini(SmsMessage sms) {
        boolean bSucceed = false;
        
        byte[] pdu = getDeliveryPdu(null, sms.getDisplayOriginatingAddress(),
                sms.getDisplayMessageBody(), sms.getTimestampMillis(), 0);
        
        bSucceed = SmsManager.getDefault().copyMessageToIcc(null, 
                pdu, SmsManager.STATUS_ON_ICC_READ);
        
        return bSucceed;
    }
    
    /**
     * sendMultipartTextMessageWithEncodingTypeGemini is for CMCC operator
     * @param dest
     * @param serviceCenter
     * @param slotId
     * @param codingType
     * @param messages
     * @param deliveryIntents
     * @param sentIntents
     */
    public static void sendMultipartTextMessageWithEncodingTypeGemini(String dest, String serviceCenter, 
            ArrayList<String> messages, int codingType, int slotId, 
            ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
/*        GnGeminiSmsManager.sendMultipartTextMessageWithEncodingTypeGemini(dest, serviceCenter, messages, 
            codingType, slotId, sentIntents, deliveryIntents);*/
          msimsms.sendMultipartTextMessage(dest,serviceCenter,messages,sentIntents,deliveryIntents,slotId);
    }

    public static SmsMemoryStatus getSmsSimMemoryStatusGemini(int currentSlotId) {
//        return GeminiSmsManager.getSmsSimMemoryStatusGemini(currentSlotId);
        return null;
    }
	
	    public static ArrayList<SmsMessage> getAllMessagesFromIccGemini(int slotId) {
//        return GeminiSmsManager.getAllMessagesFromIccGemini(slotId);
        return msimsms.getAllMessagesFromIcc(slotId);
    }

    public static boolean deleteMessageFromIccGemini(int i, int slotId) {
        //return GeminiSmsManager.deleteMessageFromIccGemini(i, slotId);
        return msimsms.deleteMessageFromIcc(i,slotId);//MSimSmsManager.getDefault().deleteMessageFromIcc(i, slotId);
    }
}
