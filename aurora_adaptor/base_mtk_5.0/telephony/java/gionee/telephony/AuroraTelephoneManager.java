package gionee.telephony;

import android.net.Uri;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.IIccPhoneBook;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.telephony.SubscriptionManager;

public class AuroraTelephoneManager {

	public static TelephonyManager msimtel = TelephonyManager.getDefault();
	public static final String SCHEME_TEL = "tel";
	public static final String EXTRA_IS_VIDEO_CALL = "com.android.phone.extra.video";
	public static final String EXTRA_IS_IP_DIAL = "com.android.phone.extra.ip";
	public static final int DIAL_NUMBER_INTENT_NORMAL = 0;
	public static final int DIAL_NUMBER_INTENT_IP = 1;
	public static final int DIAL_NUMBER_INTENT_VIDEO = 2;
	public static final String KEY_AUTO_IP_DIAL_IF_SUPPORT = "autoIpDial";

	public static boolean isMultiSimEnabled() {
		return msimtel.isMultiSimEnabled();
	}

	public static boolean isSimCardInsert(int slotid) {

		return msimtel.hasIccCard(slotid);
	}

	public static boolean isSimStateReady(int slotId) {

		return TelephonyManager.SIM_STATE_READY == msimtel.getSimState(slotId);
	}

	public static Uri getSimContactsUri(int slotid) {
	    return Uri.parse("content://icc/adn/subId/" + slotid);
	}

	public static Uri getSimContactsUri(int subId, boolean isUsim) {
            if (!isUsim) {
	        return Uri.parse("content://icc/adn/subId/" + subId);
            } else {
                return Uri.parse("content://icc/pbr/subId/" + subId);
            }
	}

	public static Intent getCallNumberIntent(String number, int slotid) {

		return getCallNumberIntent(number, slotid, DIAL_NUMBER_INTENT_NORMAL);
	}

	public static Intent getCallNumberIntent(String number, int slotid, int type) {
		Intent intent = new Intent(Intent.ACTION_CALL_PRIVILEGED,
				Uri.fromParts(SCHEME_TEL, number, null));

		// gionee xuhz 20120910 modify for CR00688935 end

		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(KEY_AUTO_IP_DIAL_IF_SUPPORT, true);

		intent.putExtra(EXTRA_IS_IP_DIAL, false);
		intent.putExtra(EXTRA_IS_VIDEO_CALL, false);
		intent.putExtra("subscription", slotid);
		if ((type & DIAL_NUMBER_INTENT_IP) != 0)
			intent.putExtra(EXTRA_IS_IP_DIAL, true);

		if ((type & DIAL_NUMBER_INTENT_VIDEO) != 0)
			intent.putExtra(EXTRA_IS_VIDEO_CALL, true);

		return intent;
	}

	public static boolean isMtkGemini() {

		return true;
	}

	public static boolean auroraaddIccRecordToEf(int efType, String name,
			String number, String[] emails, String pin2, int slotid) {
		boolean success = false;
		try {
			IIccPhoneBook iccIpb = IIccPhoneBook.Stub
					.asInterface(ServiceManager.getService("simphonebook"));
			if (iccIpb != null) {
				// success = iccIpb.updateAdnRecordsInEfBySearch(efType,
				// "", "", name, number, pin2);
				success = iccIpb.updateAdnRecordsInEfBySearchWithError(
						SubscriptionManager.getDefaultSubId(), efType, "", "",
						name, number, pin2) >= 1;
			}
		} catch (RemoteException ex) {
			// ignore it
		} catch (SecurityException ex) {
		}
		return success;
	}

	public static boolean auroraupdateIccRecordInEf(int efType, String oldName,
			String oldNumber, String newName, String newNumber, String pin2,
			int slotid) {

		boolean success = false;
		try {
			IIccPhoneBook iccIpb = IIccPhoneBook.Stub
					.asInterface(ServiceManager.getService("simphonebook"));
			if (iccIpb != null) {
				success = iccIpb.updateAdnRecordsInEfBySearch(efType, oldName,
						oldNumber, newName, newNumber, pin2);
			}
		} catch (RemoteException ex) {
			// ignore it
		} catch (SecurityException ex) {
		}
		return success;
	}

	public static boolean deleteIccRecordFromEf(int efType, String name,
			String number, String[] emails, String pin2, int slotid) {
		boolean success = false;
		try {
			IIccPhoneBook iccIpb = IIccPhoneBook.Stub
					.asInterface(ServiceManager.getService("simphonebook"));
			if (iccIpb != null) {
				success = iccIpb.updateAdnRecordsInEfBySearch(efType, name,
						number, "", "", pin2);
			}
		} catch (RemoteException ex) {
			// ignore it
		} catch (SecurityException ex) {
		}
		return success;
	}

}
