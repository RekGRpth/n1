package gionee.telephony;
import static android.telephony.TelephonyManager.PHONE_TYPE_CDMA;

import com.android.internal.telephony.SmsMessageBase;

import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import com.android.internal.telephony.GsmAlphabet;
import com.android.internal.telephony.SmsConstants;
import java.util.ArrayList;
import com.android.internal.telephony.GsmAlphabet.TextEncodingDetails;
import com.android.internal.telephony.cdma.sms.UserData;
import static android.telephony.SmsMessage.ENCODING_16BIT;
import static android.telephony.SmsMessage.ENCODING_7BIT;
import static android.telephony.SmsMessage.ENCODING_UNKNOWN;
import static android.telephony.SmsMessage.MAX_USER_DATA_BYTES;
import static android.telephony.SmsMessage.MAX_USER_DATA_SEPTETS;
import com.android.internal.telephony.SmsHeader;

public class GnSmsMessage{

//    private android.telephony.SmsMessage mMsg;
    
    public SmsMessageBase mMsg;
    private static GnSmsMessage sMe;
     //Gionee jialf 20120105 removed for CR00494057 start
//     public GnSmsMessage() {
//         mMsg = msg;
//     }
     //Gionee jialf 20120105 removed for CR00494057 end
    
    public GnSmsMessage() {
        getSmsFacility();
    }
    public static  GnSmsMessage getInstance()
    {
       if(sMe==null)
            sMe=new GnSmsMessage();
       return sMe;
    } 
   
    public void SmsMessage(SmsMessageBase smb) {
        mMsg = smb;
    }

    private static SmsMessageBase getSmsFacility() {
        int activePhone = TelephonyManager.getDefault().getCurrentPhoneType();
        if (PHONE_TYPE_CDMA == activePhone) {
            return new com.android.internal.telephony.cdma.SmsMessage();
        } else {
            return new com.android.internal.telephony.gsm.SmsMessage();
        }
    }

    public void setTelephonyMsg(SmsMessageBase msg) {
        mMsg = msg;
    }

    public boolean isReplace() {
        return mMsg.isReplace();
    }

    public String getMessageBody() {
        return mMsg.getMessageBody();
    }

    public String getDisplayMessageBody() {
        return mMsg.getDisplayMessageBody();
    }

    public String getOriginatingAddress() {
        return mMsg.getOriginatingAddress();
    }

    public int getProtocolIdentifier() {
        return mMsg.getProtocolIdentifier();
    }

    public String getDisplayOriginatingAddress() {
        return mMsg.getDisplayOriginatingAddress();
    }

    public int getMessageSimId() {
        return 0;
    }
    
    public int getMessageSimId(SmsMessage msg) {
        return 0;//msg.getSubId();
    }

    public String getPseudoSubject() {
        return mMsg.getPseudoSubject();
    }

    public boolean isReplyPathPresent() {
        return mMsg.isReplyPathPresent();
    }

    public String getServiceCenterAddress() {
        return mMsg.getServiceCenterAddress();
    }

    public static int[] calculateLength(CharSequence text, boolean b,
            int encodingType) {
//        return SmsMessage.calculateLength(text, b, encodingType);
        return null;
    }

    public static String getDisplayOriginatingAddress(SmsMessage message) {
        //return message.getDestinationAddress();
        return message.getDisplayOriginatingAddress();
    }
    
	//Gionee <guoyx> <2013-04-28> added for CR00797539 begin
    public static String getDestinationAddress(SmsMessage message) {
        //return message.getRecipientddress();
		return null;
    }
	//Gionee <guoyx> <2013-04-28> added for CR00797539 end
//aurora zhouxiaobing 20131115 start
    public byte[] getSmsc(SmsMessage sms) {

            byte[] pdu = sms.getPdu();
            if(pdu == null) {

                return null;
            }
    
            int smsc_len = (pdu[0] & 0xff) + 1;
            byte[] smsc = new byte[smsc_len];
    
            try {
                System.arraycopy(pdu, 0, smsc, 0, smsc.length);
                return smsc;
            } catch(ArrayIndexOutOfBoundsException e) {
                return null;
            }
        
        
    }
    public byte[] getTpdu(SmsMessage sms) {

            byte[] pdu = sms.getPdu();
            if(pdu == null) {
                return null;
            }
    
            int smsc_len = (pdu[0] & 0xff) + 1;
            int tpdu_len = pdu.length - smsc_len;
            byte[] tpdu = new byte[tpdu_len];
    
            try {
                System.arraycopy(pdu, smsc_len, tpdu, 0, tpdu.length);
                return tpdu;
            } catch(ArrayIndexOutOfBoundsException e) {
                return null;
            }
        
    }	
    

    public static ArrayList<String> fragmentText(String text, int encodingType) {
        int activePhone = TelephonyManager.getDefault().getPhoneType();
        TextEncodingDetails ted = (PHONE_TYPE_CDMA == activePhone) ?
            CDMAcalcTextEncodingDetails(text, false, encodingType) :
            GSMcalculateLength(text, false, encodingType);

        // TODO(cleanup): The code here could be rolled into the logic
        // below cleanly if these MAX_* constants were defined more
        // flexibly...

        int limit;
        if (ted.codeUnitSize == SmsMessage.ENCODING_7BIT) {
            int udhLength;
            if (ted.languageTable != 0 && ted.languageShiftTable != 0) {
                udhLength = GsmAlphabet.UDH_SEPTET_COST_TWO_SHIFT_TABLES;
            } else if (ted.languageTable != 0 || ted.languageShiftTable != 0) {
                udhLength = GsmAlphabet.UDH_SEPTET_COST_ONE_SHIFT_TABLE;
            } else {
                udhLength = 0;
            }

        if (ted.msgCount > 1) {
                udhLength += GsmAlphabet.UDH_SEPTET_COST_CONCATENATED_MESSAGE;
            }

            if (udhLength != 0) {
                udhLength += GsmAlphabet.UDH_SEPTET_COST_LENGTH;
            }

            limit = SmsConstants.MAX_USER_DATA_SEPTETS - udhLength;
        } else {
            if (ted.msgCount > 1) {
                limit = SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER;
            } else {
                limit = SmsConstants.MAX_USER_DATA_BYTES;
            }
        }

        int pos = 0;  // Index in code units.
        int textLen = text.length();
        ArrayList<String> result = new ArrayList<String>(ted.msgCount);
        while (pos < textLen) {
            int nextPos = 0;  // Counts code units.
            if (ted.codeUnitSize == SmsMessage.ENCODING_7BIT) {
                if (activePhone == PHONE_TYPE_CDMA && ted.msgCount == 1) {
                    // For a singleton CDMA message, the encoding must be ASCII...
                    nextPos = pos + Math.min(limit, textLen - pos);
                } else {
                    // For multi-segment messages, CDMA 7bit equals GSM 7bit encoding (EMS mode).
                    nextPos = GsmAlphabet.findGsmSeptetLimitIndex(text, pos, limit,
                            ted.languageTable, ted.languageShiftTable);
                }
            } else {  // Assume unicode.
                nextPos = pos + Math.min(limit / 2, textLen - pos);
            }
            if ((nextPos <= pos) || (nextPos > textLen)) {
                break;
            }
            result.add(text.substring(pos, nextPos));
            pos = nextPos;
        }
        return result;
    }
    public static TextEncodingDetails GSMcalculateLength(CharSequence msgBody,
            boolean use7bitOnly, int encodingType) {
        TextEncodingDetails ted = GsmAlphabet.countGsmSeptets(msgBody, use7bitOnly);

        if (encodingType == ENCODING_16BIT) {
            ted = null;
        }
        if (ted == null) {
            ted = new TextEncodingDetails();
            int octets = msgBody.length() * 2;
            ted.codeUnitCount = msgBody.length();
            if (octets > MAX_USER_DATA_BYTES) {
                ted.msgCount = (octets + (SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER - 1)) /
                        SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER;
                ted.codeUnitsRemaining = ((ted.msgCount *
                        SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER) - octets) / 2;
            } else {
                ted.msgCount = 1;
                ted.codeUnitsRemaining = (SmsConstants.MAX_USER_DATA_BYTES - octets) / 2;
            }
            ted.codeUnitSize = ENCODING_16BIT;
        }
        return ted;
    }

    public static TextEncodingDetails CDMAcalcTextEncodingDetails(CharSequence msg,
            boolean force7BitEncoding, int encodingType) {
        TextEncodingDetails ted;
        int septets = countAsciiSeptets(msg, force7BitEncoding);
        if(encodingType == SmsConstants.ENCODING_16BIT) {
            septets = -1;
        }
        if (septets != -1 && septets <= SmsConstants.MAX_USER_DATA_SEPTETS) {
            ted = new TextEncodingDetails();
            ted.msgCount = 1;
            ted.codeUnitCount = septets;
            ted.codeUnitsRemaining = SmsConstants.MAX_USER_DATA_SEPTETS - septets;
            ted.codeUnitSize = SmsConstants.ENCODING_7BIT;
        } else {
            ted = GSMcalculateLength(
                    msg, force7BitEncoding, encodingType);
            if (ted.msgCount == 1 && ted.codeUnitSize == SmsConstants.ENCODING_7BIT) {
                // We don't support single-segment EMS, so calculate for 16-bit
                // TODO: Consider supporting single-segment EMS
                ted.codeUnitCount = msg.length();
                int octets = ted.codeUnitCount * 2;
                if (octets > SmsConstants.MAX_USER_DATA_BYTES) {
                    ted.msgCount = (octets + (SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER - 1)) /
                            SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER;
                    ted.codeUnitsRemaining = ((ted.msgCount *
                            SmsConstants.MAX_USER_DATA_BYTES_WITH_HEADER) - octets) / 2;
                } else {
                    ted.msgCount = 1;
                    ted.codeUnitsRemaining = (SmsConstants.MAX_USER_DATA_BYTES - octets)/2;
                }
                ted.codeUnitSize = SmsConstants.ENCODING_16BIT;
            }
        }
        return ted;
    }
    private static int countAsciiSeptets(CharSequence msg, boolean force) {
        int msgLen = msg.length();
        if (force) return msgLen;
        for (int i = 0; i < msgLen; i++) {
            if (UserData.charToAscii.get(msg.charAt(i), -1) == -1) {
                return -1;
            }
        }
        return msgLen;
    }
    public SmsHeader getUserDataHeader(SmsMessage sms) {
        return sms.mWrappedSmsMessage.getUserDataHeader();
    }
//aurora zhouxiaobing 20131115 end
}
