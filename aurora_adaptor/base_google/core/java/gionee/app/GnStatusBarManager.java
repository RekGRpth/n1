package gionee.app;

import com.android.internal.statusbar.IStatusBarService;
import android.app.StatusBarManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Slog;

/**
 * this class not used should be deleted. 2012.08.04
 * @author guoyangxu
 *
 */
public class GnStatusBarManager /*extends StatusBarManager*/{

    private static final String TAG = "GnStatusBarManager";
    private static Context mContext;
    private static IStatusBarService mService;
    
    GnStatusBarManager(Context context) {
        mContext = context;
    }
        
    private static synchronized IStatusBarService getService() {
        if (mService == null) {
            mService = IStatusBarService.Stub.asInterface(
                    ServiceManager.getService(Context.STATUS_BAR_SERVICE));
            if (mService == null) {
                Slog.w("StatusBarManager", "warning: no STATUS_BAR_SERVICE");
            }
        }
        return mService;
    }
    
    //Gionee guoyangxu 20120618 add for support_MTK_mms_app
    public static void showSIMIndicator(StatusBarManager statusBar, ComponentName componentName, String businessType) {
        /*String pkgName = componentName == null ? "null" : componentName.getPackageName();
        try {
            Slog.d(TAG, "Show SIM indicator from " + pkgName + ", businiss is " + businessType + ".");
            final IStatusBarService svc = getService();
            if (svc != null) {
                svc.showSIMIndicator(businessType);
            }
        } catch (RemoteException ex) {
            Slog.d(TAG, "Show SIM indicator from " + pkgName + " occurs exception.");
            // system process is dead anyway.
            throw new RuntimeException(ex);
        }*/
    }
    
    public static void hideSIMIndicator(StatusBarManager statusBar, ComponentName componentName) {
        /*String pkgName = componentName == null ? "null" : componentName.getPackageName();
        try {
            Slog.d(TAG, "Hide SIM indicator from " + pkgName + ".");
            final IStatusBarService svc = getService();
            if (svc != null) {
                mService.hideSIMIndicator();
            }
        } catch (RemoteException ex) {
            Slog.d(TAG, "Hide SIM indicator from " + pkgName + " occurs exception.");
            // system process is dead anyway.
            throw new RuntimeException(ex);
        }*/
    }

}
