# Copyright 2006 The Android Open Source Project
###############################################################################
LOCAL_PATH:= $(call my-dir)
###############################################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := libhwm.c
LOCAL_MODULE := libhwm
LOCAL_SHARED_LIBRARIES += libnvram liblog libfile_op
#bobule workaround pdk build error, needing review
#LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_C_INCLUDES:= \
	$(MTK_PATH_SOURCE)/external/nvram/libnvram \
	$(MTK_PATH_SOURCE)/external/nvram/libfile_op \

LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)
###############################################################################

ifneq ($(BOARD_MTK_LIBSENSORS_NAME),)
ifneq ($(BOARD_MTK_LIB_SENSOR),)
ifneq ($(BOARD_MTK_LIB_SENSOR_NO),)
###############################################################################
include $(CLEAR_VARS)
LOCAL_SRC_FILES := cmdtool.c
LOCAL_MODULE := sensor_cmd
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)	#install to system/xbin
LOCAL_UNSTRIPPED_PATH := $(TARGET_ROOT_OUT_SBIN_UNSTRIPPED)
LOCAL_STATIC_LIBRARIES += libcutils libc libm
LOCAL_SHARED_LIBRARIES += libnvram libhwm libfile_op
include $(BUILD_EXECUTABLE)
endif
endif
endif
