LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libviagpsrpc
LOCAL_SRC_FILES_32 := arm/libviagpsrpc.so
LOCAL_SHARED_LIBRARIES := libc2kutils librpc libstdc++
LOCAL_MULTILIB := 32
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)
