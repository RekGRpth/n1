
#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}

#export COMP_PATH_ROOT=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
#export DRRPMB_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/drrpmb
#export DRRPMB_DIR=${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/

DRV_NAME=drrpmb

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release ${DRV_NAME}"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug ${DRV_NAME}"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Debug
  OPTIM=debug
fi

source ${t_sdk_root}/setup.sh

if [ -n "$t_sdk_root" ]; then
    echo "t_sdk_root =$t_sdk_root"
else
    echo "Please set t_sdk_root"
    exit 1
fi

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export DRSDK_DIR_SRC=${COMP_PATH_DrSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
export DRSDK_DIR=${COMP_PATH_DrSdk}

cd $(dirname $(readlink -f $0))
cd ../..

if [ ! -d Locals ]; then
  exit 1
fi

#if [ ! -d Out/Public ]; then
#  mkdir -p Out/Public
#fi


#cp -f \
#  Locals/Code/public/* \
#  Out/Public/

echo "Running make..."	
make -f Locals/Code/makefile.mk "$@"

if [ $? -ne 0 ]; then
	echo "[ERROR] <t-driver ${DRV_NAME} build failed!"
	exit 1;
fi


mkdir -p ${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/${OPTIM}/
mkdir -p ${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/public/

cp -f \
        Locals/Code/public/* \
        ${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/public/

cp -f ${OUT_DIR}/${DRV_NAME}.lib  ${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/${OPTIM}/
cp -f ${OUT_DIR}/${DRV_NAME}.axf  ${MKTOPDIR}/vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustlets/rpmb/drrpmb/${OPTIM}/
