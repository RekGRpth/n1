#!/bin/bash
# Copyright Statement:
#
# This software/firmware and related documentation ("MediaTek Software") are
# protected under relevant copyright laws. The information contained herein
# is confidential and proprietary to MediaTek Inc. and/or its licensors.
# Without the prior written permission of MediaTek inc. and/or its licensors,
# any reproduction, modification, use or disclosure of MediaTek Software,
# and information contained herein, in whole or in part, shall be strictly prohibited.

# MediaTek Inc. (C) 2011. All rights reserved.
#
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
# THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
# RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
# AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
# NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
# SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
# CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
# CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
# AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
# MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
# The following software/firmware and/or related documentation ("MediaTek Software")
# have been modified by MediaTek Inc. All revisions are subject to any receiver's
# applicable license agreements with MediaTek Inc.

##############################################################
# Program:
# Program to create t-base binary & trustlet/drivers
#

TBASE_MODE=$1
TBASE_IMAGE_RAW=$2
TBASE_OUTPUT_FOLDER=$3
LIB_RELEASE_PATH=$4
PRIVATE_RELEASE_PATH=$5
COMMON_SRC_PATH=$6
SRC_RELEASE_PATH=$7
APP_INSTALL_PATH=$8
export TEE_MACH_TYPE=$9
PLAT_COMMON_PATH=${10}

SAMPLE_RELEASE_PATH=../trustzone/trustlets/vendor
TEE_RELEASE_PATH=../trustzone
export TBASE_BUILD_ROOT=../t-base
export MKTOPDIR=${MKTOPDIR}

export TEE_IMAGE_OUTPUT_PATH=${TBASE_OUTPUT_FOLDER}/bin
export TEE_DRIVER_OUTPUT_PATH=${TBASE_OUTPUT_FOLDER}/driver
export TEE_TRUSTLET_OUTPUT_PATH=${TBASE_OUTPUT_FOLDER}/trustlet
export TEE_TLC_OUTPUT_PATH=${TBASE_OUTPUT_FOLDER}/tlc
export TEE_MODE=${TBASE_MODE}

if [ "$TBASE_MODE" = "Debug" ] ; then
TBASE_MODE_LC=debug
TBASE_ENDORSEMENT_PUB_KEY=${TEE_RELEASE_PATH}/kernel/debugEndorsementPubKey.pem
TEE_INSTALL_MODE=Debug
TEE_EXTRA_BUILD_MODE=Release
else
TBASE_MODE_LC=release
TBASE_ENDORSEMENT_PUB_KEY=${TEE_RELEASE_PATH}/kernel/endorsementPubKey.pem
TEE_INSTALL_MODE=Release
TEE_EXTRA_BUILD_MODE=Debug
fi
TBASE_TRUSTLET_KEY=${TEE_RELEASE_PATH}/kernel/pairVendorTltSig.pem
TBASE_IMAGE_ORI=${TEE_RELEASE_PATH}/kernel/${TBASE_MODE}/${ARCH_MTK_PLATFORM}_mobicore_${TBASE_MODE_LC}.raw

function build_tbase ()
{
    ##############################################################
    # Binary Generation
    #

    echo "[ Building t-base images ]"
    echo "Protect SRC path : ${LIB_RELEASE_PATH}"
    echo "PRIVATE SRC path : ${PRIVATE_RELEASE_PATH}"
    echo "COMMON SRC path : ${COMMON_SRC_PATH}"
    echo "PLATFORM COMMON SRC path : ${PLAT_COMMON_PATH}"
    echo "JDK PATH : ${JAVA_HOME}/bin"
    echo "APP_INSTALL_PATH = ${APP_INSTALL_PATH}"
    echo "TBASE_OUTPUT_FOLDER = ${TBASE_OUTPUT_FOLDER}"
    echo "TEE_MACH_TYPE = ${TEE_MACH_TYPE}"

    # java -jar MobiConfig/Out/Bin/MobiConfig.jar -c -i mobicore.img.raw -o mobicore.img -k VendorTltSig_pub.pem -ek endorsementPubKey.pem
    echo "${JAVA_HOME}/bin/java -jar ${TEE_RELEASE_PATH}/tools/MobiConfig/Bin/MobiConfig.jar -c -i ${TBASE_IMAGE_ORI} -o ${TBASE_IMAGE_RAW} -k $TBASE_TRUSTLET_KEY -ek $TBASE_ENDORSEMENT_PUB_KEY"
    ${JAVA_HOME}/bin/java -jar ${TEE_RELEASE_PATH}/tools/MobiConfig/Bin/MobiConfig.jar -c -i ${TBASE_IMAGE_ORI} -o ${TBASE_IMAGE_RAW} -k $TBASE_TRUSTLET_KEY -ek $TBASE_ENDORSEMENT_PUB_KEY
}

function clean_trustlets ()
{
    echo "clean TEE drivers"
    rm -r -f ${TEE_DRIVER_OUTPUT_PATH}
    echo "clean TEE trustlets"
    rm -r -f ${TEE_TRUSTLET_OUTPUT_PATH}
    echo "clean TEE trustlet connector"
    rm -r -f ${TEE_TLC_OUTPUT_PATH}
}

function build_single_trustlet ()
{
    echo "[DEBUG] try executing $1"
    if [ -f $1 ]; then
        echo "[INFO]executing $1"
        $1
        if [ $? -ne 0 ]; then 
            echo "[ERROR] executing script failed : $1"
            exit 1; 
        fi
    fi
}

function build_trustlets ()
{
    echo "MKTOPDIR = ${MKTOPDIR}"

#    build_single_trustlet ./${SAMPLE_RELEASE_PATH}/foo/Drfoo/Locals/Build/build.sh
#    build_single_trustlet ./${SAMPLE_RELEASE_PATH}/foo/Tlfoo/Locals/Build/build.sh
#    build_single_trustlet ./${SAMPLE_RELEASE_PATH}/foo/Tlcfoo/Locals/Build/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/utils/Drutils/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/utils/Drutils/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/utils/Tlutils/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/utils/Tlutils/build.sh
#    build_single_trustlet ${LIB_RELEASE_PATH}/utils/Tlcutils/Locals/Build/build.sh
    build_single_trustlet ${PRIVATE_RELEASE_PATH}/sec/Drsec/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/sec/Drsec/build.sh
    build_single_trustlet ${PRIVATE_RELEASE_PATH}/sec/Tlsec/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/sec/Tlsec/build.sh
#    build_single_trustlet ${PRIVATE_RELEASE_PATH}/sec/Tlcsec/Locals/Build/build.sh
#    build_single_trustlet ${SRC_RELEASE_PATH}/storage/DrSecureStorage/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/cm/tlCm/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/keymaster/TlTeeKeymaster/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/keymaster/TlTeeKeymaster/build.sh
if [ "$TEE_MACH_TYPE" = "mt6753" ] ; then
    build_single_trustlet ${SRC_RELEASE_PATH}/tui/DrTui/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/tui/TlSamplePinpad/Locals/Build/build.sh
fi
    build_single_trustlet ${SRC_RELEASE_PATH}/spi/Drspi/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/spi/Tlspi/Locals/Build/build.sh
    #build_single_trustlet ${SRC_RELEASE_PATH}/spi/Tlcspi/Locals/Build/build.sh

    build_single_trustlet ${LIB_RELEASE_PATH}/m4u/m4u_drv/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/m4u/tlM4uApi/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/m4u/m4u_tl/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/m4u/tlm4u/build.sh

    build_single_trustlet ${PRIVATE_RELEASE_PATH}/tplay/Drtplay/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/tplay/Drtplay/build.sh
    build_single_trustlet ${PRIVATE_RELEASE_PATH}/tplay/Tltplay/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/tplay/Tltplay/build.sh
#    build_single_trustlet ${PRIVATE_RELEASE_PATH}/tplay/Tlctplay/Locals/Build/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Vdec/MtkH264SecVdecDrv/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/video/MtkH264SecVdecDrv/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Vdec/MtkH264SecVdecTL/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/video/MtkH264SecVdecTL/build.sh
    #build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Vdec/MtkH264SecVdecTLC/Locals/Build/build.sh

    build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Venc/MtkH264SecVencDrv/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/video/MtkH264SecVencDrv/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Venc/MtkH264SecVencTL/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/video/MtkH264SecVencTL/build.sh
#   build_single_trustlet ${LIB_RELEASE_PATH}/video/MtkH264Venc/MtkH264SecVencTLC/Locals/Build/build.sh

    #CMDQ
    build_single_trustlet ${LIB_RELEASE_PATH}/cmdq/cmdq_sec_dr/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/cmdq/cmdq_sec_dr/build.sh
    build_single_trustlet ${LIB_RELEASE_PATH}/cmdq/cmdq_sec_tl/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/cmdq/cmdq_sec_tl/build.sh
 
    echo "COMMON_SRC_PATH = ${COMMON_SRC_PATH}"
    echo "LIB_RELEASE_PATH = ${LIB_RELEASE_PATH}"
    echo "SRC_RELEASE_PATH = ${SRC_RELEASE_PATH}"

    #DRM key install
    build_single_trustlet ${COMMON_SRC_PATH}/key_install/DrKeyInstall/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/key_install/DrKeyInstall/build.sh
    build_single_trustlet ${COMMON_SRC_PATH}/key_install/TlKeyInstall/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/key_install/TlKeyInstall/build.sh
    
    ##############################################################
    #RPMB
    build_single_trustlet ${PLAT_COMMON_PATH}/rpmb/Drrpmb/Locals/Build/build.sh
    build_single_trustlet ${PLAT_COMMON_PATH}/rpmb/Tlrpmb/Locals/Build/build.sh
    
    ##############################################################
    #Widevine DRM
    
    #Widevine Classic DRM
    build_single_trustlet ${COMMON_SRC_PATH}/widevine/classic_drm/DrWidevineClassicDrm/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/widevine/classic_drm/DrWidevineClassicDrm/build.sh
    build_single_trustlet ${COMMON_SRC_PATH}/widevine/classic_drm/TlWidevineClassicDrm/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/widevine/classic_drm/TlWidevineClassicDrm/build.sh
    build_single_trustlet ${COMMON_SRC_PATH}/widevine/classic_drm/TlWidevineClassicDrmDecrypt/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/widevine/classic_drm/TlWidevineClassicDrmDecrypt/build.sh
    
    #Widevine Modular DRM
    build_single_trustlet ${COMMON_SRC_PATH}/widevine/modular_drm/DrWidevineModularDrm/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/widevine/modular_drm/DrWidevineModularDrm/build.sh
    build_single_trustlet ${COMMON_SRC_PATH}/widevine/modular_drm/TlWidevineModularDrm/Locals/Build/build.sh
    build_single_trustlet ${SRC_RELEASE_PATH}/widevine/modular_drm/TlWidevineModularDrm/build.sh

    ##############################################################
}

function install_trustlets ()
{
    local TBASE_TA_LIST
    local TBASE_DRIVER_LIST

    echo "TEE_INSTALL_MODE = ${TEE_INSTALL_MODE} ==> install mode"
    echo "TEE_EXTRA_BUILD_MODE = ${TEE_EXTRA_BUILD_MODE}"

    TBASE_TA_LIST=`ls ${TBASE_OUTPUT_FOLDER}/*/*/${TEE_INSTALL_MODE}/*.tlbin`
    TBASE_DRIVER_LIST=`ls ${TBASE_OUTPUT_FOLDER}/*/*/${TEE_INSTALL_MODE}/*.drbin`

    mkdir -p ${APP_INSTALL_PATH}

    for INSTALL_TA in $TBASE_TA_LIST
    do
        echo "INSTALL TA = ${INSTALL_TA}"
        cp ${INSTALL_TA} ${APP_INSTALL_PATH}
    done
    for INSTALL_TDRIVER in $TBASE_DRIVER_LIST
    do
        echo "INSTALL <t-driver = ${INSTALL_TDRIVER}"
        cp ${INSTALL_TDRIVER} ${APP_INSTALL_PATH}
    done
}

##############################################################
# Main Flow
#

case "$1" in
    clean)
        clean_trustlets;
        exit 0;
    ;;
    *)
        build_tbase;
        build_trustlets;
        export TEE_MODE=${TEE_EXTRA_BUILD_MODE}
        build_trustlets;
        export TEE_MODE=${TEE_INSTALL_MODE}
	install_trustlets;
    ;;
esac
