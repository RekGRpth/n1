################################################################################
#
# <t-base Makefile Template
#
################################################################################

# output binary name without path or extension
OUTPUT_NAME := MtkH264SecVencDrv


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------
DRIVER_UUID := 07070000000000000000000000000000
DRIVER_MEMTYPE := 2 # 0: iram preferred; 1: iram; 2: eram
DRIVER_NO_OF_THREADS := 4 # min =1; max =8
DRIVER_SERVICE_TYPE := 1 # 1: driver; 2: service provider trustlet; 3: system trustlet
DRIVER_KEYFILE := pairVendorTltSig.pem
DRIVER_FLAGS:= 0 # 0: no flag; 1: permanent; 2: service has no WSM control interface; 3: both (permanent and service has not WSM control interface)
DRIVER_VENDOR_ID :=0 # Trustonic
DRIVER_NUMBER := 0x0204 # DrTemplate
DRIVER_ID := $$(($(DRIVER_VENDOR_ID)<<16|$(DRIVER_NUMBER)))
DRIVER_INTERFACE_VERSION_MAJOR=1
DRIVER_INTERFACE_VERSION_MINOR=0
DRIVER_INTERFACE_VERSION := $(DRIVER_INTERFACE_VERSION_MAJOR).$(DRIVER_INTERFACE_VERSION_MINOR)
TBASE_API_LEVEL := 3

#-------------------------------------------------------------------------------
# use generic make file
DRIVER_DIR ?= Locals/Code
DRLIB_DIR := $(DRIVER_DIR)/api
DRSDK_DIR_SRC ?= $(DRSDK_DIR)
TLSDK_DIR_SRC ?= $(TLSDK_DIR)

#-------------------------------------------------------------------------------
# standalone build
OUTPUT_ROOT := $(TEE_DRIVER_OUTPUT_PATH)/$(OUTPUT_NAME)
OUTPUT_PATH := $(OUTPUT_ROOT)/$(TEE_MODE)
DR_BIN := $(OUTPUT_PATH)/$(DRIVER_UUID).drbin
DR_AXF := $(OUTPUT_PATH)/$(OUTPUT_NAME).axf
DR_LST2 := $(OUTPUT_PATH)/$(OUTPUT_NAME).lst2
CROSS=arm-none-eabi
READELF=$(CROSS_GCC_PATH_BIN)/$(CROSS)-readelf
READ_OPT=-a $(DR_AXF) > $(DR_LST2)
DR_PARAM := -servicetype $(DRIVER_SERVICE_TYPE) \
            -numberofthreads $(DRIVER_NO_OF_THREADS) \
            -bin $(DR_AXF) \
            -output $(DR_BIN) \
            -d $(DRIVER_ID) \
            -memtype $(DRIVER_MEMTYPE) \
            -flags $(DRIVER_FLAGS) \
            -interfaceversion $(DRIVER_INTERFACE_VERSION) \
            -keyfile $(DRIVER_KEYFILE)

all : $(DR_BIN)

$(DR_BIN) : $(DR_AXF)
	$(info ******************************************)
	$(info ** READELF & MobiConvert *****************)
	$(info ******************************************)
	$(READELF) $(READ_OPT)
	$(JAVA_HOME)/bin/java -jar $(DRSDK_DIR_SRC)/Bin/MobiConvert/MobiConvert.jar $(DR_PARAM) >$(DR_BIN).log
	cp $(DR_BIN) $(OUTPUT_PATH)/$(DRIVER_UUID).tlbin
