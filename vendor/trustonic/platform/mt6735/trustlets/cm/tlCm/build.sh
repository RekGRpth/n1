#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}

TL_NAME=tlCm

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Debug
  OPTIM=debug
fi

source ${t_sdk_root}/setup.sh

cd $(dirname $(readlink -f $0))

if [ ! -d "${OPTIM}" ]; then
  exit 1
fi

mkdir -p ${OUT_DIR}/
cp -f ${OPTIM}/tlCm.axf  ${OUT_DIR}/

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
export MOBICORE_TOOL_DIR=${COMP_PATH_MOBICONFIG}
echo "Running make..."	
make -f makefile.mk "$@"