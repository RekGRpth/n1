package com.aurora.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;

public class FileUtils {
	public static void setValue(String path, String key, String value) {
		BufferedWriter bwrite = null;
		try {
			File file = new File(path);
			if (!file.exists()) {
				file.createNewFile();
			}
			bwrite = new BufferedWriter(new FileWriter(file));
			if (key != null && !"".equals(key.trim())) {
				bwrite.write(key + ":" + value + "\n");
			}
			bwrite.flush();
			bwrite.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (bwrite != null) {
					bwrite.flush();
					bwrite.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}

	public static boolean deleteFile(String path) {
		return deleteFile(new File(path));
	}

	public static boolean deleteFile(File dir) {
		if (dir == null || (dir != null && !dir.exists()))
			return false;
		if (dir.isDirectory()) {
			String[] children = dir.list();
			// 递归删除目录中的子目录下
			if(children!=null){
				for (int i = 0; i < children.length; i++) {
					boolean success = deleteFile(new File(dir, children[i]));
					if (!success) {
						return false;
					}
				}
			}
		}
		// 目录此时为空，可以删除
		boolean returnB = dir.delete();
		return returnB;
	}

	public static boolean deleteFileExeptRoot(String path) {
		File file = new File(path);
		if (file == null || (file != null && !file.exists()))
			return false;
		if (file.isDirectory()) {
			String[] children = file.list();
			if(children!=null){
				for (int i = 0; i < children.length; i++) {
					boolean success = deleteFile(new File(file, children[i]));
					if (!success) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public void findFiles(String baseDirName, String targetFileName,
			List fileList) {
		/**
		 * 算法简述： 从某个给定的需查找的文件夹出发，搜索该文件夹的所有子文件夹及文件，
		 * 若为文件，则进行匹配，匹配成功则加入结果集，若为子文件夹，则进队列。 队列不空，重复上述操作，队列为空，程序结束，返回结果。
		 */
		String tempName = null;
		// 判断目录是否存在
		File baseDir = new File(baseDirName);
		if (!baseDir.exists() || !baseDir.isDirectory()) {
			System.out.println("文件查找失败：" + baseDirName + "不是一个目录！");
		} else {
			String[] filelist = baseDir.list();
			for (int i = 0; i < filelist.length; i++) {
				File readfile = new File(baseDirName + "\\" + filelist[i]);
				// System.out.println(readfile.getName());
				if (!readfile.isDirectory()) {
					tempName = readfile.getName();
					if (wildcardMatch(targetFileName, tempName)) {
						// 匹配成功，将文件名添加到结果集
						fileList.add(readfile.getAbsoluteFile());
					}
				} else if (readfile.isDirectory()) {
					findFiles(baseDirName + "\\" + filelist[i], targetFileName,
							fileList);
				}
			}
		}
	}

	/**
	 * 通配符匹配
	 * 
	 * @param pattern
	 *            通配符模式
	 * @param str
	 *            待匹配的字符串
	 * @return 匹配成功则返回true，否则返回false
	 */
	private boolean wildcardMatch(String pattern, String str) {
		int patternLength = pattern.length();
		int strLength = str.length();
		int strIndex = 0;
		char ch;
		for (int patternIndex = 0; patternIndex < patternLength; patternIndex++) {
			ch = pattern.charAt(patternIndex);
			if (ch == '*') {
				// 通配符星号*表示可以匹配任意多个字符
				while (strIndex < strLength) {
					if (wildcardMatch(pattern.substring(patternIndex + 1),
							str.substring(strIndex))) {
						return true;
					}
					strIndex++;
				}
			} else if (ch == '?') {
				// 通配符问号?表示匹配任意一个字符
				strIndex++;
				if (strIndex > strLength) {
					// 表示str中已经没有字符匹配?了。
					return false;
				}
			} else {
				if ((strIndex >= strLength) || (ch != str.charAt(strIndex))) {
					return false;
				}
				strIndex++;
			}
		}
		return (strIndex == strLength);
	}
}
