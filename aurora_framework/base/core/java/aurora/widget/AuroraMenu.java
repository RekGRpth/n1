package aurora.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import aurora.app.AuroraDialog;
import aurora.widget.AuroraMenuBase.OnAuroraMenuItemClickListener;

/**
 * @author leftaven
 * @2013年9月12日 aurora menu
 */
public class AuroraMenu extends AuroraMenuBase {
	private ViewGroup customView;
	private ImageView imageView1;
	private ImageView imageView2;
	private ImageView imageView3;
	private ImageView imageView4;
	private ImageView imageView5;
	private LinearLayout layout1;
	private LinearLayout layout2;
	private LinearLayout layout3;
	private LinearLayout layout4;
	private LinearLayout layout5;
	private TextView textView1;
	private TextView textView2;
	private TextView textView3;
	private TextView textView4;
	private TextView textView5;
	private ImageView resultImageView;
	private TextView resultTextView;
	private int position;
	public static final int ITEM_WITHTEXT_MOST = 5;
	
	/*first menu id for aurora menu*/
	public static final int FIRST = 1;

	public AuroraMenu(Context context,
			AuroraActionBar auroraActionBar,
			final OnAuroraMenuItemClickListener auroraMenuCallBack,
			AuroraMenuAdapterBase auroraMenuAdapter, int aniTabMenu, int resId) {
		this(auroraActionBar, context, resId, aniTabMenu);
		menuAdapter = auroraMenuAdapter;
		this.auroraMenuCallBack = auroraMenuCallBack;
		this.auroraActionBar = auroraActionBar;
		setClickListener(menuAdapter.getCount());// 设置回调监听
	}

	// Aurora <aven> <2013年9月16日> modify for actionbar menu
	public AuroraMenu(final AuroraActionBar auroraActionBar,
			Context context, int resId, int animMenu) {
		super(context);
		initCustomView(context, resId, animMenu);
	}
	
	public void setMenuItemClickListener(OnAuroraMenuItemClickListener auroraMenuCallBack){
	    this.auroraMenuCallBack = auroraMenuCallBack;
	}

	/**
	 * 初始化自定义的menu视图
	 * 
	 * @param context
	 * @param resId
	 * @param animMenu
	 */
	private void initCustomView(Context context, int resId, int animMenu) {
		mContext = context;
		customView = (ViewGroup) LayoutInflater.from(context).inflate(resId,
				null);
		defaultMenu(customView, animMenu);
		
		this.setWidth(LayoutParams.FILL_PARENT);
		
		// 设置menu外部可点击
		customView.setFocusable(true);
		customView.setFocusableInTouchMode(true);
		// 设置点击外部menu不消失
		setFocusable(false);
		setOutsideTouchable(false);
		customView.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// 单击menu菜单时拦截事件
				if (keyCode == KeyEvent.KEYCODE_MENU && isShowing()) {
					dismissActionBarMenu();
				} else if (keyCode == KeyEvent.KEYCODE_BACK && isShowing()) {
					dismissActionBarMenu();
					// dismiss();
				}
				return false;
			}
		});
	}

	/**
	 * 底部菜单回调监听
	 */
	private View.OnClickListener onlClickListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			position = 0;
			switch (v.getId()) {
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1:
				position = 0;
				break;
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout2:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text2:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image2:
				position = 1;
				break;
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout3:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text3:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image3:
				position = 2;
				break;
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout4:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text4:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image4:
				position = 3;
				break;
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image5:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout5:
			case com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text5:
				position = 4;
				break;
			}
			Log.e("101010", "--onlClickListener position = ---" + position);
			if (auroraMenuCallBack != null) {
				auroraMenuItem = (AuroraMenuItem) menuAdapter.getItem(position);
				auroraMenuCallBack.auroraMenuItemClick(auroraMenuItem.getId());
				AuroraDialog.showFromPopupWindow();
	            //Log.e("menu", "clicked");
			}
		}
	};

	/**
	 * @param count
	 *            菜单项数
	 */
	public void setClickListener(int count) {
		menuItems = menuAdapter.getMenuItems();
		initViewByCount(count);
		setViewClickListener(count);
	}

	/**
	 * 点击菜单，图标变化
	 * 
	 * @param count
	 */
	private void setImageChangeListener(int count) {
       
		switch (count) {
		case 1:
			layout1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){ 
					    //if(TextUtils.isEmpty(textView1.getText())){
					        imageView1.onTouchEvent(event);
					    //}
						textView1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						Log.e("101010", "--layout1 setOnTouchListener MotionEvent.ACTION_UP---");
					    //if(TextUtils.isEmpty(textView1.getText())){
					        imageView1.setPressed(false);
					    //}
						//imageView1.setPressed(false);
						textView1.setPressed(false);
		            }
					return false;
				}
			});
			textView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView1.setPressed(false);
//						layout1.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView1)){
//							imageView1.setPressed(false);
//							layout1.setPressed(false);
//						}
//					}
					return false;
				}
			});
			imageView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						Log.e("101010", "--imageView1 setOnTouchListener MotionEvent.ACTION_UP---");
						textView1.setPressed(false);
						layout1.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView1)){
							textView1.setPressed(false);
							layout1.setPressed(false);
						}
					}
					return false;
				}
			});

			break;
		case 2:
			layout1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView1.onTouchEvent(event);
						textView1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView1.setPressed(false);
						textView1.setPressed(false);
		            }
					return false;
				}
			});
			textView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView1.setPressed(false);
//						layout1.setPressed(false);
//		            } 
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView1)){
//							imageView1.setPressed(false);
//							layout1.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView1.setPressed(false);
						layout1.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView1)){
							textView1.setPressed(false);
							layout1.setPressed(false);
						}
					}
					return false;
				}
			});

			layout2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView2.onTouchEvent(event);
						textView2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView2.setPressed(false);
						textView2.setPressed(false);
		            }
					return false;
				}
			});
			textView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView2.setPressed(false);
//						layout2.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView2)){
//							imageView2.setPressed(false);
//							layout2.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView2.setPressed(false);
						layout2.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView2)){
							textView2.setPressed(false);
							layout2.setPressed(false);
						}
					}
					return false;
				}
			});

			break;

		case 3:
			layout1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView1.onTouchEvent(event);
						textView1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView1.setPressed(false);
						textView1.setPressed(false);
		            }
					return false;
				}
			});
			textView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView1.setPressed(false);
//						layout1.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView1)){
//							imageView1.setPressed(false);
//							layout1.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView1.setPressed(false);
						layout1.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView1)){
							textView1.setPressed(false);
							layout1.setPressed(false);
						}
					}
					return false;
				}
			});

			layout2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView2.onTouchEvent(event);
						textView2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView2.setPressed(false);
						textView2.setPressed(false);
		            }
					return false;
				}
			});
			textView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView2.setPressed(false);
//						layout2.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView2)){
//							imageView2.setPressed(false);
//							layout2.setPressed(false);
//						}
//					}
					return false;
				}
			});

			imageView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView2.setPressed(false);
						layout2.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView2)){
							textView2.setPressed(false);
							layout2.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView3.onTouchEvent(event);
						textView3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView3.setPressed(false);
						textView3.setPressed(false);
		            }
					return false;
				}
			});
			textView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView3.setPressed(false);
//						layout3.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView3.setPressed(false);
//							layout3.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView3.setPressed(false);
						layout3.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView3.setPressed(false);
							layout3.setPressed(false);
						}
					}
					return false;
				}
			});

			break;
			
		case 4:
			layout1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView1.onTouchEvent(event);
						textView1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView1.setPressed(false);
						textView1.setPressed(false);
		            }
					return false;
				}
			});
			textView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView1.setPressed(false);
//						layout1.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView1)){
//							imageView1.setPressed(false);
//							layout1.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView1.setPressed(false);
						layout1.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView1)){
							textView1.setPressed(false);
							layout1.setPressed(false);
						}
					}
					return false;
				}
			});

			layout2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView2.onTouchEvent(event);
						textView2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView2.setPressed(false);
						textView2.setPressed(false);
		            }
					return false;
				}
			});
			textView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView2.setPressed(false);
//						layout2.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView2)){
//							imageView2.setPressed(false);
//							layout2.setPressed(false);
//						}
//					}
					return false;
				}
			});

			imageView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView2.setPressed(false);
						layout2.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView2)){
							textView2.setPressed(false);
							layout2.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView3.onTouchEvent(event);
						textView3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView3.setPressed(false);
						textView3.setPressed(false);
		            }
					return false;
				}
			});
			textView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView3.setPressed(false);
//						layout3.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView3.setPressed(false);
//							layout3.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView3.setPressed(false);
						layout3.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView3.setPressed(false);
							layout3.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView4.onTouchEvent(event);
						textView4.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView4.setPressed(false);
						textView4.setPressed(false);
		            }
					return false;
				}
			});
			textView4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView4.onTouchEvent(event);
						layout4.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView4.setPressed(false);
//						layout4.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView4.setPressed(false);
//							layout4.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView4.onTouchEvent(event);
						layout4.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView4.setPressed(false);
						layout4.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView4.setPressed(false);
							layout4.setPressed(false);
						}
					}
					return false;
				}
			});

			break;
			
		case 5:
			layout1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView1.onTouchEvent(event);
						textView1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView1.setPressed(false);
						textView1.setPressed(false);
		            }
					return false;
				}
			});
			textView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView1.setPressed(false);
//						layout1.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView1)){
//							imageView1.setPressed(false);
//							layout1.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView1.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView1.onTouchEvent(event);
						layout1.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView1.setPressed(false);
						layout1.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView1)){
							textView1.setPressed(false);
							layout1.setPressed(false);
						}
					}
					return false;
				}
			});

			layout2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView2.onTouchEvent(event);
						textView2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView2.setPressed(false);
						textView2.setPressed(false);
		            }
					return false;
				}
			});
			textView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView2.setPressed(false);
//						layout2.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView2)){
//							imageView2.setPressed(false);
//							layout2.setPressed(false);
//						}
//					}
					return false;
				}
			});

			imageView2.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView2.onTouchEvent(event);
						layout2.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView2.setPressed(false);
						layout2.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView2)){
							textView2.setPressed(false);
							layout2.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView3.onTouchEvent(event);
						textView3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView3.setPressed(false);
						textView3.setPressed(false);
		            }
					return false;
				}
			});
			textView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView3.setPressed(false);
//						layout3.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView3.setPressed(false);
//							layout3.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView3.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView3.onTouchEvent(event);
						layout3.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView3.setPressed(false);
						layout3.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView3.setPressed(false);
							layout3.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView4.onTouchEvent(event);
						textView4.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView4.setPressed(false);
						textView4.setPressed(false);
		            }
					return false;
				}
			});
			textView4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView4.onTouchEvent(event);
						layout4.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView4.setPressed(false);
//						layout4.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView4.setPressed(false);
//							layout4.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView4.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView4.onTouchEvent(event);
						layout4.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView4.setPressed(false);
						layout4.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView4.setPressed(false);
							layout4.setPressed(false);
						}
					}
					return false;
				}
			});
			
			layout5.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						imageView5.onTouchEvent(event);
						textView5.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						imageView5.setPressed(false);
						textView5.setPressed(false);
		            }
					return false;
				}
			});
			textView5.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
//					if(event.getAction()==MotionEvent.ACTION_DOWN){  
//						imageView5.onTouchEvent(event);
						layout5.onTouchEvent(event);
//		            }
//					if(event.getAction()==MotionEvent.ACTION_UP){  
//						imageView5.setPressed(false);
//						layout5.setPressed(false);
//		            }
//					if(event.getAction() == MotionEvent.ACTION_MOVE){
//						if(auroraMenuIsOutOfBounds(mContext, event, textView3)){
//							imageView5.setPressed(false);
//							layout5.setPressed(false);
//						}
//					}
					return false;
				}
			});
			
			imageView5.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if(event.getAction()==MotionEvent.ACTION_DOWN){  
						textView5.onTouchEvent(event);
						layout5.onTouchEvent(event);
		            }
					if(event.getAction()==MotionEvent.ACTION_UP){  
						textView5.setPressed(false);
						layout5.setPressed(false);
		            }
					if(event.getAction() == MotionEvent.ACTION_MOVE){
						if(auroraMenuIsOutOfBounds(mContext, event, imageView3)){
							textView5.setPressed(false);
							layout5.setPressed(false);
						}
					}
					return false;
				}
			});

			break;
		}

	}

	/**
	 * 设置各个项的监听
	 * 
	 * @param count
	 */
	private void setViewClickListener(int count) {
		try {
			for (int i = 0; i < count; i++) {
				resultImageView = getImageViewByPosition(i);
				if (getActionBottomBarMenuImage(i) != 0) {
					resultImageView
							.setImageResource(getActionBottomBarMenuImage(i));
				}
				resultImageView.setOnClickListener(onlClickListener);
				if (count <= ITEM_WITHTEXT_MOST) {
					resultTextView = getTitleViewByPosition(i);
					//Log.e("222222", "getActionBottomBarMenuImage = " + getActionBottomBarMenuImage(i));
					//Log.e("222222", "getActionBottomBarMenuTitle = " + getActionBottomBarMenuTitle(i));
					if ( getActionBottomBarMenuImage(i) == 0 ) {
						LayoutParams lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
								LayoutParams.WRAP_CONTENT);
						lp.leftMargin = 0;
						resultTextView.setLayoutParams(lp);
						resultTextView.setTextSize(13);
						resultTextView.setTextColor(mContext.getResources().getColor(com.aurora.R.color.aurora_action_bottom_bar_title_change_color_with_noicon));
					}
					if("".equals(getActionBottomBarMenuTitle(i))){
						resultTextView.setVisibility(View.GONE);
					}else{
						resultTextView.setText(getActionBottomBarMenuTitle(i));
						//resultTextView.setOnClickListener(onlClickListener);
						if ( resultImageView instanceof AuroraAnimationImageView ) {
							((AuroraAnimationImageView)resultImageView).playAnim(false);
						}
					}
					if (i <= ITEM_WITHTEXT_MOST - 1) {
						getLayoutByPosition(i).setOnClickListener(
								onlClickListener);// 设置layout监听
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 初始化各个视图
	 * 
	 * @param count
	 */
	private void initViewByCount(int count) {
		switch (count) {
		case 1:
			imageView1 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1);
			textView1 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1);
			layout1 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1);
			if(!TextUtils.isEmpty(textView1.getText()) && imageView1 instanceof AuroraAnimationImageView){
			    ((AuroraAnimationImageView)imageView1).playAnim(false);
			}
			break;
		case 2:
			imageView1 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1);
			imageView2 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image2);

			textView1 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1);
			textView2 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text2);
			layout1 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1);
			layout2 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout2);
			break;
		case 3:
			imageView1 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1);
			imageView2 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image2);
			imageView3 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image3);

			textView1 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1);
			textView2 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text2);
			textView3 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text3);

			layout1 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1);
			layout2 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout2);
			layout3 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout3);
			break;
		case 4:
			imageView1 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1);
			imageView2 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image2);
			imageView3 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image3);
			imageView4 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image4);
			
			textView1 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1);
			textView2 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text2);
			textView3 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text3);
			textView4 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text4);

			layout1 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1);
			layout2 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout2);
			layout3 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout3);
			layout4 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout4);
			break;
		case 5:
			imageView1 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image1);
			imageView2 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image2);
			imageView3 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image3);
			imageView4 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image4);
			imageView5 = (ImageView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_image5);
			
			textView1 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text1);
			textView2 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text2);
			textView3 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text3);
			textView4 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text4);
			textView5 = (TextView) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_text5);

			layout1 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout1);
			layout2 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout2);
			layout3 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout3);
			layout4 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout4);
			layout5 = (LinearLayout) customView
					.findViewById(com.aurora.internal.R.id.aurora_action_bottom_bar_menu_item_layout5);
			break;
		}
		if (count <= ITEM_WITHTEXT_MOST)
			setImageChangeListener(count);
	}

	/**
	 * @param i
	 * @return bottom menu title
	 */
	private String getActionBottomBarMenuTitle(int position) {
		int title = menuItems.get(position).getTitle();
		if (title == 0)
			return "";
		return mContext.getResources().getString(title);
	}

	private int getActionBottomBarMenuImage(int position) {
		return menuItems.get(position).getIcon();
	}

	/**
	 * 增加菜单项
	 * 
	 * @param itemId
	 */
	public void addMenuItemById(int itemId) {
		findViewByItemId(itemId).setVisibility(View.VISIBLE);
	}

	/**
	 * 删除菜单项
	 * 
	 * @param itemId
	 */
	public void removeMenuItemById(int itemId) {
		findViewByItemId(itemId).setVisibility(View.GONE);
	}

	/**
	 * @param itemId
	 * @return 通过itemid 找到点击的view
	 */
	private View findViewByItemId(int itemId) {
		View resultView = null;
		for (int position = 0; position < menuItems.size(); position++) {
			auroraMenuItem = (AuroraMenuItem) menuAdapter.getItem(position);
			if (auroraMenuItem.getId() == itemId) {
				if (menuItems.size() <= ITEM_WITHTEXT_MOST) {
					resultView = getLayoutByPosition(position);
				} else {
					resultView = getImageViewByPosition(position);
				}
				break;
			}
		}
		return resultView;
	}
	
	/**
	 * @param itemId
	 * @return 通过itemid 找到点击的position
	 */
	private int findPoisitonByItemId(int itemId) {
		int position = 0;
		for (int i = 0; position < menuItems.size(); i++) {
			AuroraMenuItem tmpAuroraMenuItem = (AuroraMenuItem) menuAdapter.getItem(position);
			if (tmpAuroraMenuItem.getId() == itemId) {
				position = i;
				break;
			}
		}
		return position;
	}

	/**
	 * @param position
	 * @return 得到imageview
	 */
	private ImageView getImageViewByPosition(int position) {
		ImageView resultView = null;
		switch (position) {
		case 0:
			resultView = imageView1;
			break;
		case 1:
			resultView = imageView2;
			break;
		case 2:
			resultView = imageView3;
			break;
		case 3:
			resultView = imageView4;
			break;
		case 4:
			resultView = imageView5;
			break;
		}
		return resultView;
	}

	public LinearLayout getLayoutByPosition(int position) {
		LinearLayout linearLayout = null;
		switch (position) {
		case 0:
			linearLayout = layout1;
			break;
		case 1:
			linearLayout = layout2;
			break;
		case 2:
			linearLayout = layout3;
			break;
		case 3:
			linearLayout = layout4;
			break;
		case 4:
			linearLayout = layout5;
			break;
		}
		return linearLayout;
	}

	public TextView getTitleViewByPosition(int position) {
		TextView resultView = null;
		switch (position) {
		case 0:
			resultView = textView1;
			break;
		case 1:
			resultView = textView2;
			break;
		case 2:
			resultView = textView3;
			break;
		case 3:
			resultView = textView4;
			break;
		case 4:
			resultView = textView5;
			break;
		}
		return resultView;
	}

	/**
	 * 设置底部button是否可用
	 * 
	 * @param position
	 * @param isEnable
	 */
	public void setBottomMenuItemEnable(int position, boolean isEnable) {
		if ( position >= 0 && position < menuItems.size() ) {
			if (menuItems.size() <= ITEM_WITHTEXT_MOST && position < ITEM_WITHTEXT_MOST) {
				getLayoutByPosition(position).setEnabled(isEnable);
				getImageViewByPosition(position).setEnabled(isEnable);
				getTitleViewByPosition(position).setEnabled(isEnable);
			} else {
				getImageViewByPosition(position).setEnabled(isEnable);
			}
		}
	}

	public ViewGroup getActionMenuView() {
		return customView;
	}

	/**
	 * @return 得到批量操作左边视图
	 */
	public View getActionMenuLeftView() {
		return customView.findViewById(com.aurora.internal.R.id.aurora_action_bar_btn_left);
	}

	/**
	 * @return 得到批量操作右边视图
	 */
	public View getActionMenuRightView() {
		return customView.findViewById(com.aurora.internal.R.id.aurora_action_bar_btn_right);
	}

	/**
	 * @param index
	 * @return 获取底部bar的view
	 */
	public View getAuroraBottomBarViewAt(int index) {
		View bottomView = null;
		switch (index) {
		case 1:
			bottomView = imageView1;
			break;
		case 2:
			bottomView = imageView2;
			break;
		case 3:
			bottomView = imageView3;
			break;
		case 4:
			bottomView = imageView4;
			break;
		case 5:
			bottomView = imageView5;
			break;
		}
		return bottomView;
	}

	private boolean auroraMenuIsOutOfBounds(Context context, MotionEvent event,View target) {
		final int x = (int) event.getX();
		final int y = (int) event.getY();
		final int slop = ViewConfiguration.get(context).getScaledWindowTouchSlop();
		return (x < -slop) || (y < -slop)
				|| (x > (target.getWidth()+slop))
				|| (y > (target.getHeight()+slop));
	}
	
	//Aurora add by tangjun 2015.1.14 begin
	public void setMenuIconByItemId(int menuIconRes, int itemId) {
		/*
		menuAdapter.setMenuIconByItemId(menuIconRes, itemId);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getImageViewByPosition(findPoisitonByItemId(itemId)).setImageResource(menuIconRes);
	}
	
	public void setMenuIconByPosition(int menuIconRes, int position) {
		/*
		menuAdapter.setMenuIconByItemId(menuIconRes, position);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getImageViewByPosition(position).setImageResource(menuIconRes);
	}
	
	public void setMenuTextByItemId(int menuTextResId, int itemId) {
		/*
		menuAdapter.setMenuTextByItemId(menuTextResId, itemId);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getTitleViewByPosition(findPoisitonByItemId(itemId)).setText(mContext.getResources().getText(menuTextResId));
	}
	
	public void setMenuTextByItemId(CharSequence menuText, int itemId) {
		/*
		menuAdapter.setMenuTextByItemId(menuText, itemId);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getTitleViewByPosition(findPoisitonByItemId(itemId)).setText(menuText);
	}
	
	public void setMenuTextByPosition(int menuTextResId, int position) {
		/*
		menuAdapter.setMenuTextByPosition(menuTextResId, position);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getTitleViewByPosition(position).setText(mContext.getResources().getText(menuTextResId));
	}
	
	public void setMenuTextByPosition(CharSequence menuText, int position) {
		/*
		menuAdapter.setMenuTextByPosition(menuText, position);
		menuAdapter.notifyDataSetChanged();
		update();
		*/
		getTitleViewByPosition(position).setText(menuText);
	}
	//Aurora add by tangjun 2015.1.14 end
}
