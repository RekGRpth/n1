package aurora.widget;

import android.content.Context;
import android.graphics.Outline;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.QuickContactBadge;

/**
 * this class not only can used to show a cycle image,but also used 
 * like QuickContactBadge,@see{android.widget.QuickContackBadge}
 * for detail
 * @author alexluo
 *
 */
public class AuroraQuickContactBadge extends QuickContactBadge {

	private MyOutLineProvider mOutLineProvider;
	
	private boolean mHasMeasured = false;
	
	public AuroraQuickContactBadge(Context context, AttributeSet attrs,
			int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		// TODO Auto-generated constructor stub
	}

	public AuroraQuickContactBadge(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
	}

	public AuroraQuickContactBadge(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	public AuroraQuickContactBadge(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * clip this view to cycle by set outline
	 */
	private void clip(){
		if(mOutLineProvider == null){
			mOutLineProvider = new MyOutLineProvider();
		}
		setClipToOutline(true);
		setOutlineProvider(mOutLineProvider);
	}
	
	/**
	 * ensure this view is a cycle
	 */
	private void resize(){
		ViewGroup.LayoutParams params = getLayoutParams();
		int width = getMeasuredWidth();
		int height = getMeasuredHeight();
		if(width > height){
			height = width;
		}else{
			width = height;
		}
		params.width = width;
		params.height = height;
		
		setLayoutParams(params);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(!mHasMeasured){
			resize();
			clip();
			mHasMeasured = true;
		}
	}
	
	class MyOutLineProvider extends ViewOutlineProvider{

		@Override
		public void getOutline(View view, Outline outline) {
			// TODO Auto-generated method stub
			outline.setOval(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		}
		
	}
	
	
	
	
}
