/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fcntl.h>
#include <ctype.h>
#include <errno.h>
#include <inttypes.h>
#include <math.h>
#include <poll.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/select.h>
#include <pthread.h>
#include <cutils/properties.h>
#include <cutils/log.h>
#include <utils/BitSet.h>
#include <math.h>
#include <stdlib.h>
#include <sys/stat.h>
#include "CwMcuSensor.h"

/*****************************************************************************/
#define CWMCU_CALIB_SAVE_IN_FLASH
#define IIO_MAX_BUFF_SIZE 8192
#define IIO_MAX_DATA_SIZE 24
#define IIO_BUF_SIZE_RETRY 8

#define INT32_CHAR_LEN 12

#define IIO_MAX_NAME_LENGTH 30

#define PERIODIC_SYNC_TIME_SEC     (30)

static const char *iio_dir = "/sys/bus/iio/devices/";

typedef union _Bits
{
    float f;
    int32_t si;
    uint32_t ui;
}Bits;

#define shift  13
#define shiftSign  16

#define infN 0x7F800000 // flt32 infinity
#define maxN 0x477FE000 // max flt16 normal as a flt32
#define minN 0x38800000 // min flt16 normal as a flt32
#define signN 0x80000000 // flt32 sign bit
#define subC 0x003FF // max flt32 subnormal down shifted

//static const int32_t minC = ((int32_t)minN >> shift);
//static const int32_t infC = ((int32_t)infN >> shift);
static const int32_t nanN = (((int32_t)infN >> shift) + 1) << shift; // minimum flt16 nan as a flt32
static const int32_t maxC = ((int32_t)maxN >> shift);
static const int32_t signC = (int32_t)signN >> shiftSign; // flt16 sign bit
static const int32_t mulN = 0x52000000; // (1 << 23) / minN
static const int32_t mulC = 0x33800000; // minN / (1 << (23 - shift))
static const int32_t norC = 0x00400; // min flt32 normal down shifted
static const int32_t maxD = ((int32_t)infN >> shift) - ((int32_t)maxN >> shift) - 1;
static const int32_t minD = ((int32_t)minN >> shift) - (int32_t)subC - 1;

static int logCnt = 0;

uint16_t
CompressFloat32To16(float value)
{
    Bits v, s;
    v.f = value;
    uint32_t sign = v.si & signN;
    v.si ^= sign;
    sign >>= shiftSign; // logical shift
    s.si = mulN;
    s.si = (int32_t)(s.f * v.f); // correct subnormals
    v.si ^= (s.si ^ v.si) & -(minN > v.si);
    v.si ^= (infN ^ v.si) & -((infN > v.si) & (v.si > maxN));
    v.si ^= (nanN ^ v.si) & -((nanN > v.si) & (v.si > infN));
    v.ui >>= shift; // logical shift
    v.si ^= ((v.si - maxD) ^ v.si) & -(v.si > maxC);
    v.si ^= ((v.si - minD) ^ v.si) & -(v.si > subC);
    return v.ui | sign;
}

float
DecompressFloat16To32(uint16_t value)
{
    Bits v;
    v.ui = value;
    int32_t sign = v.si & signC;
    v.si ^= sign;
    sign <<= shiftSign;
    v.si ^= ((v.si + minD) ^ v.si) & -(v.si > subC);
    v.si ^= ((v.si + maxD) ^ v.si) & -(v.si > maxC);
    Bits s;
    s.si = mulC;
    s.f *= v.si;
    int32_t mask = -(norC > v.si);
    v.si <<= shift;
    v.si ^= (s.si ^ v.si) & mask;
    v.si |= sign;
    return v.f;
}

static int chomp(char *buf, int len)
{
    if (buf == NULL || len < 0) {
        return -1;
    }

    while (len > 0 && (buf[len - 1] == '\n' || buf[len - 1] == '\r')) {
        buf[len - 1] = '\0';
        len--;
    }
    return 0;
}

static void reverse(char s[])
{
    int c, i, j;
    for (i = 0, j = (int)strlen(s) - 1; i < j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = (char)c;
    }
}

static int int2a(char *buf, int value, int bufsize)
{
    unsigned int tmp;
    int i, sign;
    if (buf == NULL) {
        return -1;
    }

    sign = value;
    if (sign < 0) {
        tmp = (unsigned int)(-value);
        bufsize--;
    } else {
        tmp = (unsigned int)value;
    }

    if (bufsize <= 0) {
        return -1;
    }

    i = 0;
    do {
        if (i >= bufsize - 1) {
            return -1;
        }
        buf[i++] = (char)(tmp % 10 + '0');
    } while ((tmp /= 10) > 0);

    if (sign < 0) {
        buf[i++] = '-';
    }
    buf[i] = '\0';
    reverse(buf);
    return 0;
}

static int sysfs_get_input_attr(char *devpath, const char *attr, char *value, int len)
{
    char fname[PATH_MAX];
    int fd;
    int nread;

    snprintf(fname, sizeof(fname), "%s/%s", devpath, attr);
    fname[sizeof(fname) - 1] = '\0';
    fd = open(fname, O_RDONLY);
    if (fd < 0) {
        ALOGE("cyw:%s:%d: open %s failed!\n", __func__, __LINE__, fname);
        return -errno;
    }

    nread = (int)read(fd, value, (size_t)len);
    if (nread < 0) {
        ALOGE("cyw:%s:%d: read %s failed!\n", __func__, __LINE__, fname);
        close(fd);
        return -errno;
    }
    value[nread - 1] = '\0';
    close(fd);
    chomp(value, (int)strlen(value));
    return 0;
}

static int sysfs_get_input_attr_by_int(char *devpath, const char *attr, int *value)
{
    char buf[INT32_CHAR_LEN];
    int rt;

    if (value == NULL) {
        ALOGE("cyw:%s:%d: value=NULL!\n", __func__, __LINE__);
        return -EINVAL;
    }

    rt = sysfs_get_input_attr(devpath, attr, buf, sizeof(buf));
    if (rt < 0) {
        ALOGE("cyw:%s:%d: sysfs_get_input_attr() failed!\n", __func__, __LINE__);
        return rt;
    }

    *value = atoi(buf);
    return 0;
}

static inline int find_type_by_name(const char *name, const char *type)
{
    const struct dirent *ent;
    int number, numstrlen;
    FILE *nameFile;
    DIR *dp;
    char thisname[IIO_MAX_NAME_LENGTH];
    char *filename;

    dp = opendir(iio_dir);
    if (dp == NULL) {
        ALOGE("cyw:%s:%d: dp=NULL!\n", __func__, __LINE__);
        return -ENODEV;
    }

    while (ent = readdir(dp), ent != NULL) {
        if (strcmp(ent->d_name, ".") != 0 &&
                strcmp(ent->d_name, "..") != 0 &&
                strlen(ent->d_name) > strlen(type) &&
                strncmp(ent->d_name, type, strlen(type)) == 0) {
            numstrlen = sscanf(ent->d_name + strlen(type), "%d", &number);

            /* verify the next character is not a colon */
            if (strncmp(ent->d_name + strlen(type) + numstrlen, ":", 1) != 0) {
                filename = (char *)malloc(strlen(iio_dir) + strlen(type) + numstrlen + 6);
                if (filename == NULL) {
                    ALOGE("cyw:%s:%d: filename=NULL!\n", __func__, __LINE__);
                    return -ENOMEM;
                }
                sprintf(filename, "%s%s%d/name", iio_dir, type, number);
                nameFile = fopen(filename, "r");
                if (!nameFile) {
                    ALOGE("cyw:%s:%d: fopen %s failed!\n", __func__, __LINE__, filename);
                    continue;
                }

                free(filename);
                fscanf(nameFile, "%s", thisname);
                if (strcmp(name, thisname) == 0) {
                    return number;
                }
                fclose(nameFile);
            }
        }
    }
    return -ENODEV;
}

pthread_mutex_t sys_fs_mutex = PTHREAD_MUTEX_INITIALIZER;

void *sync_time_thread_run(void *context) {
    CwMcuSensor *myClass = (CwMcuSensor *)context;
    while (1) {
        ALOGE("cyw:%s\n", __func__);
        if (myClass->mEnabled) {
            pthread_mutex_lock(&sys_fs_mutex);
            myClass->SyncSlopeOffset();
            pthread_mutex_unlock(&sys_fs_mutex);
            sleep(PERIODIC_SYNC_TIME_SEC);
        } else {
            sleep(PERIODIC_SYNC_TIME_SEC);
        }
    }
    return NULL;
}

CwMcuSensor::CwMcuSensor()
    : SensorBase(NULL, "CwMcuSensor")
    , mEnabled(0)
    , mInputReader(IIO_MAX_BUFF_SIZE)
    , init_trigger_done(false) {

    mPendingEvents[ACCELERATION].version = sizeof(sensors_event_t);
    mPendingEvents[ACCELERATION].sensor = ACCELERATION;
    mPendingEvents[ACCELERATION].type = SENSOR_TYPE_ACCELEROMETER;

    mPendingEvents[MAGNETIC].version = sizeof(sensors_event_t);
    mPendingEvents[MAGNETIC].sensor = MAGNETIC;
    mPendingEvents[MAGNETIC].type = SENSOR_TYPE_MAGNETIC_FIELD;

    mPendingEvents[GYRO].version = sizeof(sensors_event_t);
    mPendingEvents[GYRO].sensor = GYRO;
    mPendingEvents[GYRO].type = SENSOR_TYPE_GYROSCOPE;
    mPendingEvents[GYRO].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[LIGHT].version = sizeof(sensors_event_t);
    mPendingEvents[LIGHT].sensor = LIGHT;
    mPendingEvents[LIGHT].type = SENSOR_TYPE_LIGHT;

    mPendingEvents[PROXIMITY].version = sizeof(sensors_event_t);
    mPendingEvents[PROXIMITY].sensor = PROXIMITY;
    mPendingEvents[PROXIMITY].type = SENSOR_TYPE_PROXIMITY;

    mPendingEvents[PROXIMITY_GESTURE].version = sizeof(sensors_event_t);
    mPendingEvents[PROXIMITY_GESTURE].sensor = PROXIMITY_GESTURE;
    mPendingEvents[PROXIMITY_GESTURE].type = SENSOR_TYPE_ROTATION_VECTOR;

    mPendingEvents[MOTION].version = sizeof(sensors_event_t);
    mPendingEvents[MOTION].sensor = MOTION;
    mPendingEvents[MOTION].type = SENSOR_TYPE_ROTATION_VECTOR;
    mPendingEvents[MOTION].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;
	
    mPendingEvents[ORIENTATION].version = sizeof(sensors_event_t);
    mPendingEvents[ORIENTATION].sensor = ORIENTATION;
    mPendingEvents[ORIENTATION].type = SENSOR_TYPE_ORIENTATION;
    mPendingEvents[ORIENTATION].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[ROTATIONVECTOR].version = sizeof(sensors_event_t);
    mPendingEvents[ROTATIONVECTOR].sensor = ROTATIONVECTOR;
    mPendingEvents[ROTATIONVECTOR].type = SENSOR_TYPE_ROTATION_VECTOR;
    mPendingEvents[ROTATIONVECTOR].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[LINEARACCELERATION].version = sizeof(sensors_event_t);
    mPendingEvents[LINEARACCELERATION].sensor = LINEARACCELERATION;
    mPendingEvents[LINEARACCELERATION].type = SENSOR_TYPE_LINEAR_ACCELERATION;
    mPendingEvents[LINEARACCELERATION].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[GRAVITY].version = sizeof(sensors_event_t);
    mPendingEvents[GRAVITY].sensor = GRAVITY;
    mPendingEvents[GRAVITY].type = SENSOR_TYPE_GRAVITY;
    mPendingEvents[GRAVITY].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[MAGNETIC_UNCALIBRATED].version = sizeof(sensors_event_t);
    mPendingEvents[MAGNETIC_UNCALIBRATED].sensor = MAGNETIC_UNCALIBRATED;
    mPendingEvents[MAGNETIC_UNCALIBRATED].type = SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED;
    mPendingEvents[MAGNETIC_UNCALIBRATED].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[GYROSCOPE_UNCALIBRATED].version = sizeof(sensors_event_t);
    mPendingEvents[GYROSCOPE_UNCALIBRATED].sensor = GYROSCOPE_UNCALIBRATED;
    mPendingEvents[GYROSCOPE_UNCALIBRATED].type = SENSOR_TYPE_GYROSCOPE_UNCALIBRATED;
    mPendingEvents[GYROSCOPE_UNCALIBRATED].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[GAME_ROTATION_VECTOR].version = sizeof(sensors_event_t);
    mPendingEvents[GAME_ROTATION_VECTOR].sensor = GAME_ROTATION_VECTOR;
    mPendingEvents[GAME_ROTATION_VECTOR].type = SENSOR_TYPE_GAME_ROTATION_VECTOR;
    mPendingEvents[GAME_ROTATION_VECTOR].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR].version = sizeof(sensors_event_t);
    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR].sensor = GEOMAGNETIC_ROTATION_VECTOR;
    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR].type = SENSOR_TYPE_GEOMAGNETIC_ROTATION_VECTOR;
    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[STEP_COUNTER].version = sizeof(sensors_event_t);
    mPendingEvents[STEP_COUNTER].sensor = STEP_COUNTER;
    mPendingEvents[STEP_COUNTER].type = SENSOR_TYPE_STEP_COUNTER;
    mPendingEvents[STEP_COUNTER].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;
    mPendingEvents[STEP_COUNTER].u64.step_counter = 0;

    mPendingEvents[STEP_DETECTOR].version = sizeof(sensors_event_t);
    mPendingEvents[STEP_DETECTOR].sensor = STEP_DETECTOR;
    mPendingEvents[STEP_DETECTOR].type = SENSOR_TYPE_STEP_DETECTOR;
    mPendingEvents[STEP_DETECTOR].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[SIGNIFICANT_MOTION].version = sizeof(sensors_event_t);
    mPendingEvents[SIGNIFICANT_MOTION].sensor = SIGNIFICANT_MOTION;
    mPendingEvents[SIGNIFICANT_MOTION].type = SENSOR_TYPE_SIGNIFICANT_MOTION;
    mPendingEvents[SIGNIFICANT_MOTION].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[TILT].version = sizeof(sensors_event_t);
    mPendingEvents[TILT].sensor = TILT;
    mPendingEvents[TILT].type = SENSOR_TYPE_ROTATION_VECTOR;
    mPendingEvents[TILT].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[PDR].version = sizeof(sensors_event_t);
    mPendingEvents[PDR].sensor = PDR;
    mPendingEvents[PDR].type = SENSOR_TYPE_ROTATION_VECTOR;
    mPendingEvents[PDR].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[PICKUP].version = sizeof(sensors_event_t);
    mPendingEvents[PICKUP].sensor = PICKUP;
    mPendingEvents[PICKUP].type = SENSOR_TYPE_PICK_UP_GESTURE;
    mPendingEvents[PICKUP].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;
    
    mPendingEvents[BRUSH].version = sizeof(sensors_event_t);
    mPendingEvents[BRUSH].sensor = BRUSH;
    mPendingEvents[BRUSH].type = SENSOR_TYPE_BRUSH;
    mPendingEvents[BRUSH].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[PRIVATE_PEDOMETER].version = sizeof(sensors_event_t);
    mPendingEvents[PRIVATE_PEDOMETER].sensor = PRIVATE_PEDOMETER;
    mPendingEvents[PRIVATE_PEDOMETER].type = SENSOR_TYPE_PRIVATE_SENSOR_A;
    mPendingEvents[PRIVATE_PEDOMETER].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[ACCELERATION_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[ACCELERATION_WAKE_UP].sensor = ACCELERATION_WAKE_UP;
    mPendingEvents[ACCELERATION_WAKE_UP].type = SENSOR_TYPE_ACCELEROMETER;
    mPendingEvents[ACCELERATION_WAKE_UP].acceleration.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[MAGNETIC_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[MAGNETIC_WAKE_UP].sensor = MAGNETIC_WAKE_UP;
    mPendingEvents[MAGNETIC_WAKE_UP].type = SENSOR_TYPE_MAGNETIC_FIELD;

    mPendingEvents[GYRO_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[GYRO_WAKE_UP].sensor = GYRO_WAKE_UP;
    mPendingEvents[GYRO_WAKE_UP].type = SENSOR_TYPE_GYROSCOPE;
    mPendingEvents[GYRO_WAKE_UP].gyro.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[ORIENTATION_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[ORIENTATION_WAKE_UP].sensor = ORIENTATION_WAKE_UP;
    mPendingEvents[ORIENTATION_WAKE_UP].type = SENSOR_TYPE_ORIENTATION;
    mPendingEvents[ORIENTATION_WAKE_UP].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents[ROTATIONVECTOR_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[ROTATIONVECTOR_WAKE_UP].sensor = ROTATIONVECTOR_WAKE_UP;
    mPendingEvents[ROTATIONVECTOR_WAKE_UP].type = SENSOR_TYPE_ROTATION_VECTOR;

    mPendingEvents[LINEARACCELERATION_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[LINEARACCELERATION_WAKE_UP].sensor = LINEARACCELERATION_WAKE_UP;
    mPendingEvents[LINEARACCELERATION_WAKE_UP].type = SENSOR_TYPE_LINEAR_ACCELERATION;

    mPendingEvents[GRAVITY_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[GRAVITY_WAKE_UP].sensor = GRAVITY_WAKE_UP;
    mPendingEvents[GRAVITY_WAKE_UP].type = SENSOR_TYPE_GRAVITY;

    mPendingEvents[MAGNETIC_UNCALIBRATED_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[MAGNETIC_UNCALIBRATED_WAKE_UP].sensor = MAGNETIC_UNCALIBRATED_WAKE_UP;
    mPendingEvents[MAGNETIC_UNCALIBRATED_WAKE_UP].type = SENSOR_TYPE_MAGNETIC_FIELD_UNCALIBRATED;

    mPendingEvents[GYROSCOPE_UNCALIBRATED_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[GYROSCOPE_UNCALIBRATED_WAKE_UP].sensor = GYROSCOPE_UNCALIBRATED_WAKE_UP;
    mPendingEvents[GYROSCOPE_UNCALIBRATED_WAKE_UP].type = SENSOR_TYPE_GYROSCOPE_UNCALIBRATED;

    mPendingEvents[GAME_ROTATION_VECTOR_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[GAME_ROTATION_VECTOR_WAKE_UP].sensor = GAME_ROTATION_VECTOR_WAKE_UP;
    mPendingEvents[GAME_ROTATION_VECTOR_WAKE_UP].type = SENSOR_TYPE_GAME_ROTATION_VECTOR;

    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP].sensor = GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP;
    mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP].type = SENSOR_TYPE_GEOMAGNETIC_ROTATION_VECTOR;

    mPendingEvents[STEP_DETECTOR_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[STEP_DETECTOR_WAKE_UP].sensor = STEP_DETECTOR_WAKE_UP;
    mPendingEvents[STEP_DETECTOR_WAKE_UP].type = SENSOR_TYPE_STEP_DETECTOR;

    mPendingEvents[STEP_COUNTER_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[STEP_COUNTER_WAKE_UP].sensor = STEP_COUNTER_WAKE_UP;
    mPendingEvents[STEP_COUNTER_WAKE_UP].type = SENSOR_TYPE_STEP_COUNTER;

    mPendingEvents[PRIVATE_PEDOMETER_WAKE_UP].version = sizeof(sensors_event_t);
    mPendingEvents[PRIVATE_PEDOMETER_WAKE_UP].sensor = PRIVATE_PEDOMETER_WAKE_UP;
    mPendingEvents[PRIVATE_PEDOMETER_WAKE_UP].type = SENSOR_TYPE_PRIVATE_SENSOR_A;
    mPendingEvents[PRIVATE_PEDOMETER_WAKE_UP].orientation.status = SENSOR_STATUS_ACCURACY_HIGH;

    mPendingEvents_flush.version = META_DATA_VERSION;
    mPendingEvents_flush.sensor = 0;
    mPendingEvents_flush.type = SENSOR_TYPE_META_DATA;

    sync_time_thread_time = 0;
    mag_en = 0;
    TimeOffset = 0;
    TimeOffsetBias = 0;
    PreEnableTime = 0;
    TimeOffSyncTime = 0;
    Slope = 1.0f;
    PreSlope = 1.0f;
    memset(McuTime, 0x00, sizeof(CwTime_t)*SENSORS_ID_END);

    char buffer_access[PATH_MAX];
    const char *device_name = "CwMcuSensor";
    int rate = 20, dev_num, enabled = 0, i;
    int rc = 0;
    int iio_buf_size;

    dev_num = find_type_by_name(device_name, "iio:device");
    if (dev_num < 0) {
        dev_num = 0;
    }
    for(i = 0;i<SENSORS_ID_END;i++){
        McuTime[i].ready = 0;
        McuTime[i].u16Time = 0;
        McuTime[i].u32Time = 0;
        McuTime[i].i64Time = 0;
        McuTime[i].TimeOffset = 0;
        McuTime[i].TimeOffsetBias = 0;
        McuTime[i].reportTime = 0;
    }

    snprintf(buffer_access, sizeof(buffer_access),"/dev/iio:device%d", dev_num);

    data_fd = open(buffer_access, O_RDWR);
    if (data_fd < 0)  {
        ALOGE("cyw:%s:%d: open file '%s' failed: %s\n", __func__, __LINE__, buffer_access, strerror(errno));
    } else {
        ALOGE("cyw:%s:%d: fd =%d", __func__, __LINE__, data_fd);
    }

    if (data_fd >= 0) {
        strcpy(fixed_sysfs_path,"/sys/class/cywee_sensorhub/sensor_hub/");
        fixed_sysfs_path_len = strlen(fixed_sysfs_path);

        snprintf(mDevPath, sizeof(mDevPath), "%s%s", fixed_sysfs_path, "iio");

        snprintf(mTriggerName, sizeof(mTriggerName), "%s-dev%d", device_name, dev_num);

        ALOGE("cyw:%s:%d: mTriggerName = %s\n", __func__, __LINE__, mTriggerName);

        if (sysfs_set_input_attr_by_int(mDevPath, "buffer/length", IIO_MAX_BUFF_SIZE) < 0) 
            ALOGE("cyw:%s:%d:set IIO buffer enable failed00: %s\n", __func__, __LINE__, strerror(errno));

        // This is a piece of paranoia that retry for current_trigger
        for (i = 0; i < 5; i++) 
        {
            rc = sysfs_set_input_attr(mDevPath, "trigger/current_trigger",
                    mTriggerName, strlen(mTriggerName));
            if (rc < 0) {
                if (sysfs_set_input_attr_by_int(mDevPath, "buffer/enable", 1) < 0) {
                    ALOGE("cyw:%s:%d: set IIO buffer enable failed11: %s\n", __func__, __LINE__, strerror(errno));
                }
                ALOGE("cyw:%s:%d: set current trigger failed: rc = %d, strerr() = %s, i = %d\n", __func__, __LINE__, rc, strerror(errno), i);
            } else {
                init_trigger_done = true;
                ALOGE("cyw:%s:%d: set IIO buffer enable success: %s\n", __func__, __LINE__, strerror(errno));
                break;
            }
        }

        iio_buf_size = IIO_MAX_BUFF_SIZE;
        for (i = 0; i < IIO_BUF_SIZE_RETRY; i++) {
            if (sysfs_set_input_attr_by_int(mDevPath, "buffer/length", IIO_MAX_BUFF_SIZE) < 0) {
                ALOGE("cyw:%s:%d: set IIO buffer length (%d) failed: %s\n", __func__, __LINE__ , iio_buf_size, strerror(errno));
            } else {
                if (sysfs_set_input_attr_by_int(mDevPath, "buffer/enable", 1) < 0) {
                    ALOGE("cyw:%s:%d: set IIO buffer enable failed22: %s, i = %d, iio_buf_size = %d\n", __func__, __LINE__, strerror(errno), i, iio_buf_size);
                } else {
                    ALOGE("cyw:%s:%d: set IIO buffer enable success: %d\n", __func__, __LINE__, iio_buf_size);
                    break;
                }
            }
            iio_buf_size /= 2;
        }
    }
    
#ifndef CWMCU_CALIB_SAVE_IN_FLASH
    cw_calibrator(RapWmcu,ACCELERATION);
    cw_calibrator(RapWmcu,MAGNETIC);
    cw_calibrator(RapWmcu,GYRO);    
#endif
    pthread_create(&sync_time_thread, (const pthread_attr_t *) NULL,  sync_time_thread_run, (void *)this);
    ALOGE("cyw:%s exit!\n", __func__);
}

CwMcuSensor::~CwMcuSensor() {
}

int CwMcuSensor::setInitialState() {
    return 0;
}

int CwMcuSensor::find_handle(int handle, int id) {
    if (handle == NonWakeUpHandle || id > SENSORS_ID_END) {
        return id;
    }

	switch (id) {
		case ACCELERATION:
			return ACCELERATION_WAKE_UP;
		case MAGNETIC:
			return MAGNETIC_WAKE_UP;
		case GYRO:
			return GYRO_WAKE_UP;
		case ORIENTATION:
			return ORIENTATION_WAKE_UP;
		case ROTATIONVECTOR:
			return ROTATIONVECTOR_WAKE_UP;
		case LINEARACCELERATION:
			return LINEARACCELERATION_WAKE_UP;
		case GRAVITY:
			return GRAVITY_WAKE_UP;
		case MAGNETIC_UNCALIBRATED:
			return MAGNETIC_UNCALIBRATED_WAKE_UP;
		case GYROSCOPE_UNCALIBRATED:
			return GYROSCOPE_UNCALIBRATED_WAKE_UP;
		case GAME_ROTATION_VECTOR:
			return GAME_ROTATION_VECTOR_WAKE_UP;
		case GEOMAGNETIC_ROTATION_VECTOR:
			return GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP;
		case STEP_DETECTOR:
			return STEP_DETECTOR_WAKE_UP;
		case STEP_COUNTER:
			return STEP_COUNTER_WAKE_UP;
        case PRIVATE_PEDOMETER:
            return PRIVATE_PEDOMETER_WAKE_UP;
		default:
            ALOGE("cyw:%s:%d:find_handle Can not find this handle %d\n",__func__,__LINE__, id);
			return -1;
	}
	
    ALOGE("cyw:%s exit!\n", __func__);
    return -1;
}

int CwMcuSensor::find_sensor(int handle, int *data) {
	switch (handle) {
		case ACCELERATION:
		case MAGNETIC:
		case GYRO:
		case LIGHT:
		case PROXIMITY:
		case PROXIMITY_GESTURE:
		case MOTION:
		case ORIENTATION:
		case ROTATIONVECTOR:
		case LINEARACCELERATION:
		case GRAVITY:
		case MAGNETIC_UNCALIBRATED:
		case GYROSCOPE_UNCALIBRATED:
		case GAME_ROTATION_VECTOR:
		case GEOMAGNETIC_ROTATION_VECTOR:
		case STEP_DETECTOR:
		case STEP_COUNTER:
		case SIGNIFICANT_MOTION:
		case TILT:
		case PDR:
        case STATIC_MOTION:
        case PRIVATE_PEDOMETER:
        case PICKUP:
        case BRUSH:
			data[0] = NonWakeUpHandle;
			data[1] = handle;
			break;
/*=============Wake Up==========================*/
		case ACCELERATION_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = ACCELERATION;
			break;
		case MAGNETIC_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = MAGNETIC;
			break;
		case GYRO_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = GYRO;
			break;
		case ORIENTATION_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = ORIENTATION;
			break;
		case ROTATIONVECTOR_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = ROTATIONVECTOR;
			break;
		case LINEARACCELERATION_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = LINEARACCELERATION;
			break;
		case GRAVITY_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = GRAVITY;
			break;
		case MAGNETIC_UNCALIBRATED_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = MAGNETIC_UNCALIBRATED;
			break;
		case GYROSCOPE_UNCALIBRATED_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = GYROSCOPE_UNCALIBRATED;
			break;
		case GAME_ROTATION_VECTOR_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = GAME_ROTATION_VECTOR;
			break;
		case GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = GEOMAGNETIC_ROTATION_VECTOR;
			break;
		case STEP_DETECTOR_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = STEP_DETECTOR;
			break;
		case STEP_COUNTER_WAKE_UP:
			data[0] = WakeUpHandle;
			data[1] = STEP_COUNTER;
			break;
        case PRIVATE_PEDOMETER_WAKE_UP:
            data[0] = WakeUpHandle;
            data[1] = PRIVATE_PEDOMETER;
            break;
		default:
			ALOGE("cyw:%s:%d: Can not find this handle %d\n",__func__,__LINE__, handle);
			return -1;
	}
	
    return 0;
}

int CwMcuSensor::getEnable(int handle) {
    ALOGV("cyw: CwHal:%s:%d: handle = %d\n", __func__, __LINE__, handle);
    return 0;
}

int CwMcuSensor::setEnable(int handle, int en) {
    ALOGE("cyw: %s: handle=%d, en=%d\n", __func__, handle, en);
	int err = 0;
	int flags = en ? 1 : 0;
	int fd;
	char buf[30];
	int mcu_id[2];
    int i = 0;
    int clean_flag = 1;
        McuTime[handle].ready =0;
	if (find_sensor(handle,mcu_id) < 0) {
        ALOGE("cyw: %s exit! find_sensor() failed!\n", __func__);
		return -EINVAL;
	}

#ifndef CWMCU_CALIB_SAVE_IN_FLASH
	if (flags == 1) {
       switch (handle) {
	  	 case MAGNETIC:
		 case ORIENTATION:
		 case ROTATIONVECTOR:
		 case GRAVITY:
		 case MAGNETIC_UNCALIBRATED:
              mag_en |= 1 << handle;
			  ALOGE("cyw:%s: mag_count=%d",__func__,mag_en);
              break;
         default:
             break;
      }	
	}	
	
    if(flags == 0){
        switch (handle) {
            case MAGNETIC:
            case ORIENTATION:
            case ROTATIONVECTOR:
            case GRAVITY:
            case MAGNETIC_UNCALIBRATED:
                mag_en &= ~(1<< handle);
                ALOGE("cyw:%s: mag_count=%d",__func__,mag_en);
                cw_calibrator(RmcuWap,MAGNETIC);
                break;
            default:
                break;
        }	 
    }
#endif

	strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "enable");
	fd = open(fixed_sysfs_path, O_RDWR);
	if (fd >= 0) {
		sprintf(buf, "%d %d %d\n", mcu_id[0], mcu_id[1], flags);		
		err = write(fd, buf, sizeof(buf));
		close(fd);
        if (err < 0) {
            ALOGE("cyw: %s: write() failed!\n", __func__);
            return err;
        }
		mEnabled &= ~(1<<handle);
		mEnabled |= (uint64_t(flags)<<handle);
	} else {
        ALOGE("cyw: %s: open() %s failed!\n", __func__, fixed_sysfs_path);  
        return -EINVAL;
    }
#if 0
	if (mEnabled == 0) {
    	if (sysfs_set_input_attr_by_int(mDevPath, "buffer/enable", 0) < 0) {
                ALOGE("cyw:%s:%d: set buffer disable failed: %s\n",__func__,__LINE__, strerror(errno));
        } else {
                ALOGV("cyw:%s:%d: set IIO buffer enable = 0\n",__func__,__LINE__);
        }
    }
#endif
    ALOGE("cyw:%s:%d:exit: now time:%lld",__func__,__LINE__,getTimestamp());
    return 0;
}

int CwMcuSensor::batch(int handle, int flags, int64_t period_ns, int64_t timeout)
{
	int fd = 0;
	char buf[128] = {0};
	int err = 0;
	int delay_ms = 0;
	int timeout_ms = 0;
	bool dryRun = false;
	int mcu_id[2];

	if (find_sensor(handle, mcu_id) < 0) {
	    ALOGE("cyw:%s: find_sensor() failed!\n", __func__);
		return -EINVAL;
	}

	ALOGE("cyw:%s: handle=%d, flags=%d\n", __func__, handle, flags);

    SenPeriodNs[handle] = period_ns;
    SenTimeout[handle] = timeout;
    delay_ms = (int)(period_ns/1000000ll);
	if (delay_ms < 10) {
		delay_ms = 10;
        SenPeriodNs[handle] = 10000000ll;
	}
    timeout_ms = (int)(timeout/1000000ll);

	if (flags == SENSORS_BATCH_DRY_RUN) {
		ALOGE("--SENHAL-- SENSORS_BATCH_DRY_RUN~!!\n");
		dryRun = true;
	}

	if (flags == SENSORS_BATCH_WAKE_UPON_FIFO_FULL) {
		ALOGE("--SENHAL-- SENSORS_BATCH_WAKE_UPON_FIFO_FULL~!!\n");
	}

#if 0
    if (mEnabled == 0) {
        int i;
        int iio_buf_size;
        int set_flag = 0;

        if (!init_trigger_done) {
            err = sysfs_set_input_attr(mDevPath, "trigger/current_trigger",
                    mTriggerName, strlen(mTriggerName));
            if (err < 0) {
                ALOGE("cyw:%s:%d: set current trigger failed: err = %d, strerr() = %s\n",__func__,__LINE__,
                      err, strerror(errno));
                return err;
            } else {
                init_trigger_done = true;
            }
        }
        iio_buf_size = IIO_MAX_BUFF_SIZE;
        for (i = 0; i < IIO_BUF_SIZE_RETRY; i++) {
            if (sysfs_set_input_attr_by_int(mDevPath, "buffer/length", IIO_MAX_BUFF_SIZE) < 0) {
                ALOGE("cyw:%s:%d: set IIO buffer length (%d) failed: %s\n", __func__, __LINE__, iio_buf_size, strerror(errno));
            } else {
                if (sysfs_set_input_attr_by_int(mDevPath, "buffer/enable", 1) < 0) {
                    ALOGE("cyw:%s:%d: set IIO buffer enable failed22: %s, i = %d, iio_buf_size = %d\n", __func__, __LINE__, strerror(errno), i, iio_buf_size);
                } else {
                    set_flag = 1;
                    ALOGE("cyw:%s:%d: set IIO buffer enable success: %d\n", __func__, __LINE__, iio_buf_size);
                    break;
                }
            }
            iio_buf_size /= 2;
        }

        if (set_flag == 0) {
            ALOGE("cyw:%s:%d: set IIO buffer failed!\n", __func__, __LINE__);
            return -EINVAL;
        }
    }
#endif

	strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "batch");

    ALOGE("cyw:%s:%d: begin to open %s!\n", __func__, __LINE__, fixed_sysfs_path);
	fd = open(fixed_sysfs_path, O_RDWR);
	if (fd >= 0)
	{
		sprintf(buf, "%d %d %d %d %d\n", mcu_id[0],mcu_id[1], flags, delay_ms, timeout_ms);
		err = write(fd, buf, sizeof(buf));
		close(fd);
        if (err < 0) {
            ALOGE("cyw:%s:%d: write %s failed!\n", __func__, __LINE__, fixed_sysfs_path);
            return err;
        }
	} else {
        ALOGE("cyw:%s:%d: open %s failed!\n", __func__, __LINE__, fixed_sysfs_path);
        return -EINVAL;
    }
    ALOGE("cyw:%s:%d:fd:%d,handle:%d,id:%d, flags:%d,delay_ms:%d,timeout:%d, path:%s\n",__func__,__LINE__,fd ,mcu_id[0],mcu_id[1], flags, delay_ms, timeout_ms, fixed_sysfs_path);

    ALOGE("cyw:%s:%d:exit: now time:%lld",__func__,__LINE__,getTimestamp());
	return 0;
}

int CwMcuSensor::flush(int handle)
{
	int fd = 0;
	char buf[10] = {0};
	int err = 0;
	int mcu_id[2];

    ALOGE("cyw:%s\n", __func__);
    if (find_sensor(handle,mcu_id) < 0) {
        ALOGE("cyw:%s:%d: find_sensor() failed!\n", __func__, __LINE__);
		return -EINVAL;
    }

	strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "flush");

	fd = open(fixed_sysfs_path, O_RDWR);
	if (fd >= 0) {
		sprintf(buf, "%d %d\n", mcu_id[0], mcu_id[1]);
		err = write(fd, buf, sizeof(buf));
		close(fd);
        if (err < 0) {
            ALOGE("cyw:%s:%d: write %s failed!\n", __func__, __LINE__, fixed_sysfs_path);
            return err;
        }
	} else {
        ALOGE("cyw:%s:%d: open %s failed!\n", __func__, __LINE__, fixed_sysfs_path);
        return -EINVAL;
    }
    mHasPendingFlushEvent[handle] = true;
    ALOGE("cyw:%s:%d:exit: now time:%lld",__func__,__LINE__,getTimestamp());
	return 0;
}

bool CwMcuSensor::hasPendingEvents() const {
    return mHasPendingEvent;
}

int CwMcuSensor::setDelay(int handle, int64_t delay_ns)
{
    return 0;
}

int CwMcuSensor::readEvents(sensors_event_t* data, int count)
{
    int numEventReceived = 0;
    cw_event const* event;
    uint8_t data_temp[24];
    int id;
	int64_t time = getTimestamp();

	if (count < 1) {
        ALOGE("cyw:%s:%d: count < 1!\n", __func__, __LINE__);
		return -EINVAL;
    }

	ssize_t n = mInputReader.fill(data_fd);	
	if (n < 0) {
		return n;
    }

	while (count && mInputReader.readEvent(&event)) {
		memcpy(data_temp, event->data, sizeof(data_temp));

		id = processEvent(data_temp);

		if ((id == PROXIMITY) || (id == PROXIMITY_GESTURE) || (id == STEP_COUNTER) || (id == PICKUP) || (id == BRUSH))
			ALOGE("cyw:%s:%d McuTime[%d].ready:%d\n", __func__,__LINE__,id,McuTime[id].ready);
		
        if((id >=0&&id<SENSORS_ID_END && McuTime[id].ready == 1) || (id == BRUSH)){
            if (check_sensors_type(id) == 1) {
                mPendingEvents[id].timestamp = time;
            }
            *data++ = mPendingEvents[id];
            count--;
            numEventReceived++;
            if (mEnabled& (1<<id)) {
                if (id == SIGNIFICANT_MOTION) {
                    setEnable(SIGNIFICANT_MOTION, 0);
                }
				
				if (id == PICKUP) {
					setEnable(PICKUP, 0);
				}
			}
			if ((id != ACCELERATION) && (id != MAGNETIC) && (id != GYRO) && (id != LIGHT)) {
				ALOGE("cyw:%s:%d:id:%d,data:%.2f:%.2f:%.2f:T:%lld:now time:%lld:diff:%lld\n",__func__,__LINE__
	                ,id,mPendingEvents[id].data[0],mPendingEvents[id].data[1],mPendingEvents[id].data[2]
	                ,mPendingEvents[id].timestamp
	                ,getTimestamp()
	                ,(getTimestamp() -mPendingEvents[id].timestamp)/NS
	            );
			} else {
				if (logCnt == 30) {
		            ALOGE("cyw:%s:%d:id:%d,data:%.2f:%.2f:%.2f:T:%lld:now time:%lld:diff:%lld\n",__func__,__LINE__
		                ,id,mPendingEvents[id].data[0],mPendingEvents[id].data[1],mPendingEvents[id].data[2]
		                ,mPendingEvents[id].timestamp
		                ,getTimestamp()
		                ,(getTimestamp() -mPendingEvents[id].timestamp)/NS
		            );
					logCnt = 0;
				} else {
					logCnt++;
				}
			}
        }
        if(flush_event ==1)
        {
            ALOGE("cyw:%s:%d: send flush event~!!\n",__func__,__LINE__);
                flush_event = 0;
                *data++ = mPendingEvents_flush;
                count--;
                numEventReceived++;
		}
		mInputReader.next();
	}
	return numEventReceived;
}

void CwMcuSensor::Uint16FloatToFloat(uint16_t *u16float, float *data) {
    data[0] = DecompressFloat16To32(u16float[0]);
    data[1] = DecompressFloat16To32(u16float[1]);
    data[2] = DecompressFloat16To32(u16float[2]);
}

int CwMcuSensor::processEvent(uint8_t *event) {
    int handle=0;
    int sensorsid = 0;
    uint8_t datapackage[8];
    uint16_t data[3];
    uint16_t timeu16;
    uint32_t timeu32;
    uint32_t pedometer;
    int update_id = -1;

    memcpy(datapackage, &event[2], 8);

    handle = (int)event[0];
    sensorsid = find_handle((int)event[0], (int)event[1]);
    memcpy(data, &event[2], 6);
    memcpy(&timeu16, &event[8], 2);

	switch (sensorsid) {
		case TimestampSync:
			UpdateMcuTimestamp(datapackage);
			break;

	    case ACCURACY_UPDATE:
            mPendingEvents[MAGNETIC].orientation.status = data[0];
            mPendingEvents[MAGNETIC_UNCALIBRATED].orientation.status = data[0];
            mPendingEvents[ORIENTATION].orientation.status = data[0];
            mPendingEvents[ROTATIONVECTOR].orientation.status = data[0];
            mPendingEvents[GEOMAGNETIC_ROTATION_VECTOR].orientation.status = data[0];
			mPendingEvents[MAGNETIC_WAKE_UP].orientation.status = data[0];
			mPendingEvents[ORIENTATION_WAKE_UP].orientation.status = data[0];
			mPendingEvents[MAGNETIC_UNCALIBRATED_WAKE_UP].orientation.status = data[0];
		    break;

		case ACCELERATION:
		case ACCELERATION_WAKE_UP:
		case GYRO:
		case GYRO_WAKE_UP:
		case ORIENTATION:
		case ORIENTATION_WAKE_UP:
		case LINEARACCELERATION:
		case LINEARACCELERATION_WAKE_UP:
		case GRAVITY:
		case GRAVITY_WAKE_UP:
            if (datapackage[0] == 1) {
                memcpy(&uPackageSensors[sensorsid][0], &datapackage[1], 7);
            } else if (datapackage[0] == 2) {
                memcpy(&uPackageSensors[sensorsid][7], &datapackage[1], 5);
                memcpy(&timeu16, &datapackage[6], 2);
                SyncMcutimestamp((int)event[0], sensorsid, timeu16);
                update_id = sensorsid;
                memcpy(mPendingEvents[sensorsid].data, uPackageSensors[sensorsid], sizeof(float)*3);                
            }
            break;

        case ROTATIONVECTOR:
        case ROTATIONVECTOR_WAKE_UP:
        case GAME_ROTATION_VECTOR:
        case GAME_ROTATION_VECTOR_WAKE_UP:
        case GEOMAGNETIC_ROTATION_VECTOR:
        case GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP:
            if (datapackage[0] == 1) {
                memcpy(&uPackageSensors[sensorsid][0], &datapackage[1], 7);
            } else if (datapackage[0] == 2) {
                memcpy(&uPackageSensors[sensorsid][7], &datapackage[1], 5);
                memcpy(&timeu16, &datapackage[6], 2);
                SyncMcutimestamp((int)event[0], sensorsid, timeu16);
                update_id = sensorsid;
                memcpy(mPendingEvents[sensorsid].data, uPackageSensors[sensorsid], sizeof(float)*3);                
            }
            mPendingEvents[sensorsid].data[3] =
                1 - mPendingEvents[sensorsid].data[0]*mPendingEvents[sensorsid].data[0]
                - mPendingEvents[sensorsid].data[1]*mPendingEvents[sensorsid].data[1]
                - mPendingEvents[sensorsid].data[2]*mPendingEvents[sensorsid].data[2];
            
            mPendingEvents[sensorsid].data[3] = (mPendingEvents[sensorsid].data[3] > 0) ? (float)sqrt(mPendingEvents[sensorsid].data[3]) : 0;
            break;

		case LIGHT:
        case PROXIMITY_GESTURE:
            update_id = sensorsid;
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);
			// Gionee BSP chengx 20150316 add for gesture function begin
			if (PROXIMITY_GESTURE == sensorsid) {
				if ((int)mPendingEvents[sensorsid].data[0] == 4){
					mPendingEvents[sensorsid].data[0] = 1.0;
				}
				else if ((int)mPendingEvents[sensorsid].data[0] == 3){
					mPendingEvents[sensorsid].data[0] = 2.0;
				}
				else if ((int)mPendingEvents[sensorsid].data[0] == 2){
					mPendingEvents[sensorsid].data[0] = 4.0;
				}
				else if ((int)mPendingEvents[sensorsid].data[0] == 1){
					mPendingEvents[sensorsid].data[0] = 3.0;
				}
			    ALOGE("[sensorhub-hal] gesture: %f\n", mPendingEvents[sensorsid].data[0]);
			} else if (LIGHT == sensorsid) {
                if (mPendingEvents[sensorsid].data[0] < 4.0) {
                    mPendingEvents[sensorsid].data[0] = 0.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 4.0 && mPendingEvents[sensorsid].data[0] < 8.0) {
                    mPendingEvents[sensorsid].data[0] = 10.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 8.0 && mPendingEvents[sensorsid].data[0] < 12.0) {
                    mPendingEvents[sensorsid].data[0] = 20.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 12.0 && mPendingEvents[sensorsid].data[0] < 25.0) {
                    mPendingEvents[sensorsid].data[0] = 50.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 25.0 && mPendingEvents[sensorsid].data[0] < 90.0) {
                    mPendingEvents[sensorsid].data[0] = 100.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 90.0 && mPendingEvents[sensorsid].data[0] < 130.0) {
                    mPendingEvents[sensorsid].data[0] = 200.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 130.0 && mPendingEvents[sensorsid].data[0] < 200.0) {
                    mPendingEvents[sensorsid].data[0] = 300.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 200.0 && mPendingEvents[sensorsid].data[0] < 430.0) {
                    mPendingEvents[sensorsid].data[0] = 500.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 430.0 && mPendingEvents[sensorsid].data[0] < 850.0) {
                    mPendingEvents[sensorsid].data[0] = 1000.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 850.0 && mPendingEvents[sensorsid].data[0] < 1500.0) {
                    mPendingEvents[sensorsid].data[0] = 2000.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 1500.0 && mPendingEvents[sensorsid].data[0] < 3000.0) {
                    mPendingEvents[sensorsid].data[0] = 4000.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 3000.0 && mPendingEvents[sensorsid].data[0] < 5000.0) {
                    mPendingEvents[sensorsid].data[0] = 6000.0;
                } else if (mPendingEvents[sensorsid].data[0] >= 5000.0 && mPendingEvents[sensorsid].data[0] < 10000.0) {
                    mPendingEvents[sensorsid].data[0] = 12000.0;
                } else {
                    mPendingEvents[sensorsid].data[0] = 65535.0;
                }

			    ALOGE("[sensorhub-hal] light: %f %f %f\n", mPendingEvents[sensorsid].data[0], mPendingEvents[sensorsid].data[1], mPendingEvents[sensorsid].data[2]);
            } else {
			    ALOGE("[sensorhub-hal] id%d: %f %f %f\n", sensorsid, mPendingEvents[sensorsid].data[0], mPendingEvents[sensorsid].data[1], mPendingEvents[sensorsid].data[2]);
            }
            // Gionee BSP chengx 20150316 add for gesture function end
            break;

        case TILT:
            update_id = sensorsid;
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);
            SyncMcutimestamp(handle,update_id, timeu16);
            break;
			
        case MAGNETIC:
        case MAGNETIC_WAKE_UP:
            update_id = sensorsid;
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);
            SyncMcutimestamp(handle,update_id, timeu16);
            break;

		case PROXIMITY:
            update_id = sensorsid;
            mPendingEvents[sensorsid].data[0] = (float)data[0];
            mPendingEvents[sensorsid].data[1] = (float)data[1];
            mPendingEvents[sensorsid].data[2] = (float)data[2];
            ALOGE("[sensorhub-hal] proximity: %f %f %f\n", mPendingEvents[sensorsid].data[0], mPendingEvents[sensorsid].data[1], mPendingEvents[sensorsid].data[2]);
			break;

		case MAGNETIC_UNCALIBRATED:
		case MAGNETIC_UNCALIBRATED_WAKE_UP:
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);            
            break;    
    
		case MAGNETIC_UNCALIBRATED_BIAS:
			if (handle == NonWakeUpHandle) {
	            update_id = MAGNETIC_UNCALIBRATED;
	            Uint16FloatToFloat(data, &mPendingEvents[MAGNETIC_UNCALIBRATED].data[3]);
                SyncMcutimestamp(handle,update_id, timeu16);
			}
			if (handle == WakeUpHandle) {
	            update_id = MAGNETIC_UNCALIBRATED_WAKE_UP;
	            Uint16FloatToFloat(data, &mPendingEvents[MAGNETIC_UNCALIBRATED_WAKE_UP].data[3]);
                SyncMcutimestamp(handle,update_id, timeu16);
			}
			break;

		case GYROSCOPE_UNCALIBRATED:
		case GYROSCOPE_UNCALIBRATED_WAKE_UP:
			Uint16FloatToFloat(data, mPendingEvents[sensorsid].data);
			break;

		case GYRO_UNCALIBRATED_BIAS:
			if (handle == NonWakeUpHandle) {
	            update_id = GYROSCOPE_UNCALIBRATED;
	            Uint16FloatToFloat(data, &mPendingEvents[GYROSCOPE_UNCALIBRATED].data[3]);
                SyncMcutimestamp(handle,update_id, timeu16);
			}
			if (handle == WakeUpHandle) {
	            update_id = GYROSCOPE_UNCALIBRATED;
	            Uint16FloatToFloat(data,&mPendingEvents[GYROSCOPE_UNCALIBRATED].data[3]);
                SyncMcutimestamp(handle,update_id, timeu16);
			}
			break;
		case STEP_COUNTER:
            update_id = sensorsid;
			memcpy(&pedometer, &event[2], sizeof(pedometer));
			if (PrePedometerCount[NonWakeUpHandle] > pedometer) {
				PrePedometerCount[NonWakeUpHandle] = 0;
			}
			mPendingEvents[sensorsid].u64.step_counter = mPendingEvents[sensorsid].u64.step_counter + (uint64_t)(pedometer - PrePedometerCount[NonWakeUpHandle]);
			PrePedometerCount[NonWakeUpHandle] = pedometer;
            timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
            ALOGE("--SENHAL-- sensors_id=%d,data=%lld,time=%lld", sensorsid, mPendingEvents[sensorsid].u64.step_counter);
			break;

		case STEP_COUNTER_WAKE_UP:
            update_id = sensorsid;
			memcpy(&pedometer, &event[2], sizeof(pedometer));
			if (PrePedometerCount[WakeUpHandle] > pedometer) {
				PrePedometerCount[WakeUpHandle] = 0;
			}
			mPendingEvents[sensorsid].u64.step_counter = mPendingEvents[sensorsid].u64.step_counter + (uint64_t)(pedometer - PrePedometerCount[WakeUpHandle]);
			PrePedometerCount[WakeUpHandle] = pedometer;
            
            timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
            SyncMcuWakeTriggertimestamp(handle, sensorsid, timeu32);
            ALOGE("--SENHAL-- sensors_id=%d,data=%lld,time=%lld", sensorsid, mPendingEvents[sensorsid].u64.step_counter);
			break;

		case STEP_DETECTOR:
            update_id = sensorsid;
            memcpy(&pedometer, &event[2], sizeof(pedometer));
            mPendingEvents[sensorsid].data[0] =(float)pedometer;
            timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
            ALOGE("cyw:%s:%d:id:%d:data:%f", __func__, __LINE__, sensorsid, mPendingEvents[sensorsid].data[0]);
            break;

		case STEP_DETECTOR_WAKE_UP:
            update_id = sensorsid;
            memcpy(&pedometer, &event[2], sizeof(pedometer));
            mPendingEvents[sensorsid].data[0] = (float)pedometer;
            timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
            SyncMcuWakeTriggertimestamp(handle, sensorsid, timeu32);
            ALOGE("cyw:%s:%d:id:%d:data:%f", __func__, __LINE__, sensorsid, mPendingEvents[sensorsid].data[0]);
            break;

        case PRIVATE_PEDOMETER:
            if (datapackage[0] == 1) {
                memcpy(&uPackage[NonWakeUpHandle][0], &datapackage[1], 7);
            } else if (datapackage[0] == 2) {
                memcpy(&uPackage[NonWakeUpHandle][7], &datapackage[1], 7);
            } else if (datapackage[0] == 3) {
                memcpy(&uPackage[NonWakeUpHandle][14], &datapackage[1], 7);
            } else if (datapackage[0] == 4) {
                memcpy(&uPackage[NonWakeUpHandle][21], &datapackage[1], 7);
                update_id = sensorsid;
                memcpy(mPendingEvents[sensorsid].data, uPackage[NonWakeUpHandle], sizeof(float)*6);
                timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
                SyncMcuWakeTriggertimestamp(handle, sensorsid, timeu32);
                ALOGE("--SENHAL-- sensors_id=%d,data:%.1f:%.1f:%.1f:%.1f:%.1f:%.1f,time=%lld,%lld", sensorsid, mPendingEvents[sensorsid].data[0], mPendingEvents[sensorsid].data[1], mPendingEvents[sensorsid].data[2], mPendingEvents[sensorsid].data[3], mPendingEvents[sensorsid].data[4], mPendingEvents[sensorsid].data[5],getTimestamp());
            }
            break;

        case PRIVATE_PEDOMETER_WAKE_UP:
            if (datapackage[0] == 1) {
                memcpy(&uPackage[WakeUpHandle][0], &datapackage[1], 7);
            } else if (datapackage[0] == 2) {
                memcpy(&uPackage[WakeUpHandle][7], &datapackage[1], 7);
            } else if (datapackage[0] == 3) {
                memcpy(&uPackage[WakeUpHandle][14], &datapackage[1], 7);
            } else if (datapackage[0] == 4) {
                memcpy(&uPackage[WakeUpHandle][21], &datapackage[1], 7);
                update_id = sensorsid;
                memcpy(mPendingEvents[sensorsid].data, uPackage[WakeUpHandle], sizeof(float)*6);
                timeu32 = (uint32_t)datapackage[7]<<24 | (uint32_t)datapackage[6]<<16 | (uint32_t)datapackage[5]<<8 | (uint32_t)datapackage[4];
                SyncMcuWakeTriggertimestamp(handle, sensorsid, timeu32);
                ALOGE("--SENHAL-- sensors_id=%d,data:%.1f:%.1f:%.1f:%.1f:%.1f:%.1f,time=%lld,%lld", sensorsid, mPendingEvents[sensorsid].data[0], mPendingEvents[sensorsid].data[1], mPendingEvents[sensorsid].data[2], mPendingEvents[sensorsid].data[3], mPendingEvents[sensorsid].data[4], mPendingEvents[sensorsid].data[5],getTimestamp());
            }
            break;
                
		case SIGNIFICANT_MOTION:
            update_id = sensorsid;
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);
            ALOGE("cyw:%s:%d:id:%d,data:%.2f:%.2f:%.2f:T:%lld\n",__func__,__LINE__
                ,sensorsid,mPendingEvents[sensorsid].data[0],mPendingEvents[sensorsid].data[1],mPendingEvents[sensorsid].data[2]
                ,mPendingEvents[sensorsid].timestamp
            );			
			break;
        case MOTION:
		case PDR:
		case PICKUP:
		case BRUSH:
            update_id = sensorsid;
            Uint16FloatToFloat(data,mPendingEvents[sensorsid].data);
            // Gionee BSP chengx 20150316 add for gesture function begin
			ALOGE("cyw:%s:%d: id:%d, data:%f\n", __func__, __LINE__, sensorsid, mPendingEvents[sensorsid].data[0]);
			if (BRUSH == sensorsid) {
                if ((int)mPendingEvents[sensorsid].data[0] != 0) {
    				mPendingEvents[sensorsid].data[0] = 1.0;
	    		    ALOGE("cyw: brush: %f\n", mPendingEvents[sensorsid].data[0]);
                }
                else
                    mPendingEvents[sensorsid].data[0] = 0.0;
			}
            // Gionee BSP chengx 20150316 add for gesture function end
			break;

		case META_DATA:
            GetMetaData(datapackage);
			break;

		case CALIBRATOR_UPDATE:
#ifndef CWMCU_CALIB_SAVE_IN_FLASH
			cw_calibrator(RmcuWap, data[0]);
#endif
            ALOGE("cyw:%s:%d: CALIBRATOR_UPDATE: Id:%u, Status%u\n", __func__, __LINE__, data[0], data[1]);
			break;

		case MCU_REINITIAL:
            ALOGE("cyw:%s:%d: MCU_REINITIAL\n", __func__, __LINE__);
            McuInitial();
			break;

		default:
			break;
	}
    return update_id;
}

int CwMcuSensor::check_sensors_type(int id) {
    switch (id) {
        case SIGNIFICANT_MOTION:
        case LIGHT:
        case PROXIMITY:
        case PROXIMITY_GESTURE:
        case TILT:
        case BRUSH:
        case PICKUP:
        case PDR:
        case STEP_COUNTER:
        case STEP_DETECTOR:
        case PRIVATE_PEDOMETER:
            return 1;

        case ACCELERATION:
        case ACCELERATION_WAKE_UP:
        case GYRO:
        case GYRO_WAKE_UP:
        case ORIENTATION:
        case ORIENTATION_WAKE_UP:
        case LINEARACCELERATION:
        case LINEARACCELERATION_WAKE_UP:
        case GRAVITY:
        case GRAVITY_WAKE_UP:
        case ROTATIONVECTOR:
        case ROTATIONVECTOR_WAKE_UP:
        case GAME_ROTATION_VECTOR:
        case GAME_ROTATION_VECTOR_WAKE_UP:
        case GEOMAGNETIC_ROTATION_VECTOR:
        case GEOMAGNETIC_ROTATION_VECTOR_WAKE_UP:
        case STEP_COUNTER_WAKE_UP:
        case STEP_DETECTOR_WAKE_UP:
        case PRIVATE_PEDOMETER_WAKE_UP:
            return 2;

        default:
            break;
    }
    return 0;
}

int CwMcuSensor::McuInitial(void) {
    uint32_t time = 0;
    int i = 0;
    int err = 0;

    ALOGE("cyw:%s comming!\n", __func__);
    TimeOffset = 0;
    TimeOffSyncTime = 0;
    TimeOffsetBias = 0;
    err = GetMcutimestamp(&time);
    if(err){
        ALOGE("cyw:%s:%d:Get Mcu time fail\n",__func__,__LINE__);
        return 0;
    }else{
        ALOGE("cyw:%s:%d:Get Mcu time %d\n",__func__,__LINE__,time);
    }
    for(i = 0;i<SENSORS_ID_END;i++){
        McuTime[i].ready = 0;
        McuTime[i].u16Time = 0;
        McuTime[i].i64Time = 0;
        McuTime[i].reportTime = 0;
        McuTime[i].TimeOffsetBias = 0;
        
        McuTime[i].u32Time = time;
        McuTime[i].TimeOffset =getTimestamp() -(int64_t)((uint64_t)(time +300) *NS);//because sometimes 
        ALOGE("cyw:%s:%d:H:%d:mcu time:%d,offset%lld,now time:%lld",__func__,__LINE__,i,time,McuTime[i].TimeOffset,getTimestamp());
    }
    ALOGE("cyw:%s:%d:Mcu ReInit\n", __func__, __LINE__);
    return 0;
}

int CwMcuSensor::cw_calibrator(CalibratorCmd cmd, int id) {
	int temp_data[30];
	int rc = 0;

    ALOGE("cyw:%s:%d: id=%d\n", __func__, __LINE__, id);
	if (data_fd) {
		strcpy(fixed_sysfs_path, "/sys/class/cywee_sensorhub/sensor_hub/");
		fixed_sysfs_path_len = strlen(fixed_sysfs_path);
	}
	
	switch (id) {
		case ACCELERATION:
			if (cmd == RapWmcu) {
				strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
			    cw_set_command_sysfs(CALIB_DATA_WRITE, id, fixed_sysfs_path);
			
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				rc = cw_read_calibrator_file((char *)SAVE_PATH_ACC, temp_data);
				if (rc >= 0) {
                    cw_save_calibrator_sysfs(fixed_sysfs_path, temp_data);
				}
			} else if (cmd == RmcuWap) {
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
				cw_set_command_sysfs(CALIB_DATA_READ, id, fixed_sysfs_path);
			
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				rc = cw_read_calibrator_sysfs(fixed_sysfs_path, temp_data);
				if (rc >= 0) {
					cw_save_calibrator_file((char *)SAVE_PATH_ACC, temp_data);
				}
			}
		    break;

		case MAGNETIC:
			if (cmd == RapWmcu) {
				strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
			    cw_set_command_sysfs(CALIB_DATA_WRITE,id, fixed_sysfs_path);
				
				strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				rc = cw_read_calibrator_file((char *)SAVE_PATH_MAG, temp_data);
                if (rc >= 0) {
                    cw_save_calibrator_sysfs(fixed_sysfs_path, temp_data);
				}
			} else if(cmd == RmcuWap) {
                strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
				cw_set_command_sysfs(CALIB_DATA_READ,id, fixed_sysfs_path);
			
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				cw_read_calibrator_sysfs(fixed_sysfs_path, temp_data);
				if (rc >= 0) {
					cw_save_calibrator_file((char *)SAVE_PATH_MAG, temp_data);
				}
			}
		    break;

		case GYRO:
			if (cmd == RapWmcu) {
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
			    cw_set_command_sysfs(CALIB_DATA_WRITE,id, fixed_sysfs_path);
			
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				rc = cw_read_calibrator_file((char *)SAVE_PATH_GYRO, temp_data);
				if (rc >= 0) {
                    cw_save_calibrator_sysfs(fixed_sysfs_path, temp_data);
				}
			} else if(cmd == RmcuWap) {
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_cmd");
				cw_set_command_sysfs(CALIB_DATA_READ,id, fixed_sysfs_path);
			
			    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "calibrator_data");
				cw_read_calibrator_sysfs(fixed_sysfs_path, temp_data);
				if (rc >= 0) {
					cw_save_calibrator_file((char *)SAVE_PATH_GYRO, temp_data);
				}
			}
			break;

		case LIGHT:
			break;

		case PROXIMITY:
			break;
	}
	return 0;
}

int CwMcuSensor::GetMetaData(uint8_t *event){
    uint32_t time = 0;
    int64_t TimeOffset;
    int handle = find_handle(event[0], event[1]);
    if(handle <0|| handle>=SENSORS_ID_END)
        return 0;
    
    memcpy(&time,&event[4],sizeof(uint32_t));
        McuTime[handle].u32Time = time;
    if(McuTime[handle].ready ==0){
        McuTime[handle].TimeOffset =getTimestamp() -(int64_t)((uint64_t)(time +300) *NS);
        McuTime[handle].TimeOffsetBias = 0;
    ALOGE("cyw:%s:%d:H:%d:mcu time:%d,offset%lld,now time:%lld",__func__,__LINE__,handle,time,McuTime[handle].TimeOffset,getTimestamp());
        
        if(handle ==GYRO){
            if(McuTime[GYROSCOPE_UNCALIBRATED].ready >0){
                McuTime[GYRO].TimeOffset =McuTime[GYROSCOPE_UNCALIBRATED].TimeOffset;
                McuTime[GYRO].TimeOffsetBias = McuTime[GYROSCOPE_UNCALIBRATED].TimeOffsetBias;
            }
        }
        if(handle ==GYROSCOPE_UNCALIBRATED){
            if(McuTime[GYRO].ready >0){
                McuTime[GYROSCOPE_UNCALIBRATED].TimeOffset =McuTime[GYRO].TimeOffset;
                McuTime[GYROSCOPE_UNCALIBRATED].TimeOffsetBias = McuTime[GYRO].TimeOffsetBias;
            }
        }
        McuTime[handle].ready = 1;
    }else{
        TimeOffset = getTimestamp() -(int64_t)((uint64_t)(time +300) *NS);
        if(llabs(TimeOffset - McuTime[handle].TimeOffset) > 10*NS){
            McuTime[handle].TimeOffsetBias = TimeOffset -McuTime[handle].TimeOffset;
            ALOGE("cyw:%s:%d:H:%d:mcu time:%d,offset%lld,now time:%lld",__func__,__LINE__,handle,time,McuTime[handle].TimeOffset,getTimestamp());
        }
    }
    if(handle ==SIGNIFICANT_MOTION)
        return 0;

    if(handle ==PICKUP)
        return 0;
    mPendingEvents_flush.meta_data.what = META_DATA_FLUSH_COMPLETE;
    mPendingEvents_flush.meta_data.sensor = handle;
    mPendingEvents_flush.timestamp = getTimestamp();
    flush_event = 1;
    ALOGE("cyw:%s:%d:META_DATA:id:%d:sensors id:%d,t%d",__func__,__LINE__, handle, mPendingEvents_flush.meta_data.sensor,time);
    return 0;
}

int CwMcuSensor::UpdateMcuTimestamp(uint8_t *event) {
    int i = 0;
    int mcu_id[2];
    uint32_t time = 0;
    int64_t TimeOffset;

    ALOGE("cyw:%d:%s comming\n", __LINE__, __func__);
    memcpy(&time, &event[4], sizeof(uint32_t));
    for (i=0; i<SENSORS_ID_END; i++) {
        if(find_sensor(i,mcu_id)==0){
            if (mcu_id[0] == event[0]) {
                McuTime[i].u32Time = time;
                //ALOGE("cyw:%d:%s:H:%d,Time:%d,i:%d,Ti:%d\n",__LINE__,__func__,(int)event[0],(int)time,i,McuTime[i].u32Time);
                TimeOffset = getTimestamp() -(int64_t)((uint64_t)(time +100) *NS);
                if(llabs(TimeOffset - McuTime[i].TimeOffset) > 10*NS &&(McuTime[i].ready >0)){
                    McuTime[i].TimeOffsetBias = TimeOffset -McuTime[i].TimeOffset;
                    //ALOGE("cyw:%s:%d:H:%d:mcu time:%d,offset%lld,now time:%lld",__func__,__LINE__,i,time,McuTime[i].TimeOffset,getTimestamp());
                }
            }
        }
    }
    ALOGE("cyw:%d:%s:H:%d,Time:%d:now time:%lld\n",__LINE__,__func__,(int)event[0],(int)time,getTimestamp());
    return 0;
}

int CwMcuSensor::SyncMcutimestamp(int handle, int id, uint16_t mcu_time) {
    uint32_t sensor_time = 0;
    uint32_t mcu_current_time = 0;
    int64_t bias = 0;
    int64_t diff = 0;
    if(McuTime[id].ready == 0)
        return 0;
    
    pCwTime_t ptr;
    int err = 0;
    ptr = &McuTime[id];
    ptr->u16Time = mcu_time;
    sensor_time = (ptr->u32Time&0xFFFF0000) | (uint32_t)mcu_time;
    ptr->i64Time = (int64_t)sensor_time * NS;
    
    bias = SenPeriodNs[id]/10ll;
        if(ptr->TimeOffsetBias > 0 && ptr->TimeOffsetBias > bias){
            ptr->TimeOffset +=bias;
            ptr->TimeOffsetBias -=bias;
        }else if(ptr->TimeOffsetBias <0 && (llabs(ptr->TimeOffsetBias) > bias)){
            ptr->TimeOffset -=bias;
            ptr->TimeOffsetBias +=bias;
        }
    ptr->reportTime = ptr->i64Time + ptr->TimeOffset;
    /*recheck time*/
    diff = ptr->reportTime - getTimestamp();
    if(diff >0){
        ptr->TimeOffset -=diff;
        ptr->TimeOffsetBias +=diff;
        ALOGE("cyw:%s:%d:H:%d:Id:%d:mcu time:%lld,now time:%lld,diff:%lld",__func__,__LINE__,handle,id,ptr->reportTime/NS,getTimestamp()/NS,diff/NS);
        ptr->reportTime = ptr->i64Time + ptr->TimeOffset;
    }
    
    /*sync offset*/
    mPendingEvents[id].timestamp = ptr->reportTime * Slope;
    return 0;
}

int CwMcuSensor::SyncMcuWakeTriggertimestamp(int handle, int id, uint32_t mcu_time) {
    uint32_t sensor_time = 0;
    uint32_t mcu_current_time = 0;
    pCwTime_t ptr;
    int err = 0;
    ptr = &McuTime[id];
    
    sensor_time = mcu_time;
    ptr->i64Time = (int64_t)sensor_time * NS;
    ptr->reportTime = ptr->i64Time + TimeOffset;
    mPendingEvents[id].timestamp = ptr->reportTime * Slope;
  
    return 0;
}

int CwMcuSensor::GetMcutimestamp(uint32_t *mcu_time) {
    char value[128];
    int rty = 3;

    strcpy(&fixed_sysfs_path[fixed_sysfs_path_len], "timestamp");
    do {
        if (cw_read_sysfs(fixed_sysfs_path, value, sizeof(value)) >= 0) {
            sscanf(value, "%u\n", &mcu_time[0]);
            return 0;
        }
        rty--;
    } while (rty > 0);
    ALOGE("cyw:%s:%dl:MCU sync time fail\n", __func__, __LINE__);
    return -1;
}

int CwMcuSensor::SyncSlopeOffset(void) {
    #if 0
    uint32_t mcu_current_time = 0;
    int err = 0;
    double	mcu_to_cpu_time = 0.0f;
    double	offset = 0.0f;
    int i = 0;
    
    if(TimeOffset ==0||getTimestamp() > (sync_time_thread_time+(PERIODIC_SYNC_TIME_SEC*1000000000ll))){
        sync_time_thread_time = getTimestamp();
	    err = GetMcutimestamp(&mcu_current_time);
        if(err){
	        ALOGE("cyw:%s:%d:Get Mcu time fail\n", __func__, __LINE__);
            return 0;
        }else{
	        ALOGE("cyw:%s:%d:Get Mcu time %u\n", __func__, __LINE__, mcu_current_time);
        }
	    if (TimeOffset == 0) {
	        TimeOffsetBias = 0;
	        TimeOffset = getTimestamp() - (int64_t)((uint64_t)mcu_current_time * NS);
            ALOGE("cyw:%s:%d:CpuTime:%lld,McuTime:%d,Offset:%lld\n",__func__,__LINE__,getTimestamp()/NS, (int)mcu_current_time, TimeOffset/NS);
            for(i = 0;i<SENSORS_ID_END;i++){
                McuTime[i].u32Time = mcu_current_time;
            }
	    } else {
	        if (Slope == 1.0f) {
	            Slope = (double)getTimestamp()/(double)(TimeOffset +(int64_t)((uint64_t)mcu_current_time * NS));
	            if (Slope > 1.2f || Slope < 0.8f) {
	                Slope = 1.0f;
                }
	        }
        
	        mcu_to_cpu_time = (double)(TimeOffset + (int64_t)((uint64_t)mcu_current_time * NS));
            
	        offset = getTimestamp() - (mcu_to_cpu_time*Slope);
	        TimeOffsetBias = (int64_t)offset;

	        ALOGE("cyw:%s:%d:Slope:%f:%f,offset:%f:%lld\n", __func__, __LINE__, PreSlope, Slope, offset/1000000.0f, TimeOffsetBias/NS);
	        PreSlope = Slope;
    	}
    }

    #endif
    return 0;
}

int CwMcuSensor::cw_read_sysfs(char *path, char *data, int size) {
	int fd = 0;
	int readBytes = 0;
	if ((fd = open(path, O_RDONLY)) < 0) {
		ALOGE("cyw:%s:%d:Can't open '%s': %s", __func__, __LINE__, path, strerror(errno));
		return -1;
	}

	if ((readBytes = read(fd, data, size)) < 0) {
		ALOGE("cyw:%s:%d:Can't read\n", __func__, __LINE__);
		close(fd);
		return -1;
	}
	close(fd);

	return readBytes;
}

int CwMcuSensor::cw_read_calibrator_sysfs(char *path, int *calib)
{
	int fd = 0;
	char value[128];
	int readBytes = 0;
	if ((fd = open(path, O_RDONLY)) < 0) {
		ALOGE("cyw:%s:%d:  Can't open '%s': %s", __func__, __LINE__, path, strerror(errno));
		return -1;
	}

	if ((readBytes = read(fd, value, sizeof(value))) < 0) {
		ALOGE("cyw:%s:%d:Can't read\n", __func__, __LINE__);
		close(fd);
		return -1;
	}

	sscanf(value, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
		&calib[0],  &calib[1],  &calib[2],  &calib[3],  &calib[4],  &calib[5],  &calib[6],  &calib[7],  &calib[8],  &calib[9],  
        &calib[10], &calib[11], &calib[12], &calib[13], &calib[14], &calib[15], &calib[16], &calib[17], &calib[18], &calib[19], 
        &calib[20], &calib[21], &calib[22], &calib[23], &calib[24], &calib[25], &calib[26], &calib[27], &calib[28], &calib[29]);

    ALOGE("cyw:%s:%d: read sysfs calib:Time:%lld\n", __func__, __LINE__, getTimestamp());
	for (int i = 0; i < 30; i++) {
		ALOGE("cyw:%s:%d: read sysfs calib[%d] = %d\n", __func__, __LINE__, i, calib[i]);
	}
	ALOGE("cyw:%s:%d: read sysfs calib:Time:%lld\n", __func__, __LINE__, getTimestamp());
	return 0;
}

int CwMcuSensor::cw_save_calibrator_sysfs(char *path, int *calib)
{
	int i = 0;
	uint32_t data[33]={0};
	int fd = 0;
	int error =0;
	char value[1024];//255
	
	ALOGE("cyw:%s:%d:\n", __func__, __LINE__);

	if ((fd = open(path, O_WRONLY)) < 0) {
		ALOGE("cyw:%s:%d:%s:%s\n", __func__, __LINE__, path, strerror(errno));
		return -1;
	}

	snprintf(value, sizeof(value), "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
		calib[0],  calib[1],  calib[2],  calib[3],  calib[4],  calib[5],  calib[6],  calib[7],  calib[8],  calib[9],  
        calib[10], calib[11], calib[12], calib[13], calib[14], calib[15], calib[16], calib[17], calib[18], calib[19], 
        calib[20], calib[21], calib[22], calib[23], calib[24], calib[25], calib[26], calib[27], calib[28], calib[29]);

	if (write(fd, value, sizeof(value)) < 0) {
		ALOGE("cyw:%s:%d: save calib to sysfs error: %d %s\n", __func__, __LINE__, fd, strerror(errno));
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

int CwMcuSensor::cw_set_command_sysfs(int cmd, int id, char *path)
{
	int i = 0;
	uint32_t data[33]={0};
	int fd = 0;
	int error =0;
	char value[128];//255
	int calib[10];
	calib[0] = cmd;
	calib[1] = id;
	calib[2] = 0;
	
	//ALOGE("cyw:%s:%d:value:%s\n",__func__,__LINE__, value);
	//ALOGE("cyw:%s:%d:value size:%d\n",__func__,__LINE__, sizeof(value));

	if ((fd = open(path, O_WRONLY)) < 0) {
		ALOGE("cyw:%s:%d:Can't open %s:%s", __func__, __LINE__, path, strerror(errno));
		return -1;
	}

	snprintf(value, sizeof(value), "%d %d %d", calib[0], calib[1], calib[2]);
	if (write(fd, value, sizeof(value)) < 0) {
		ALOGE("cyw:%s:%d: save calib to sysfs error: %d %s\n", __func__, __LINE__, fd, strerror(errno));
		close(fd);
		return -1;
	}
	close(fd);
	return 0;
}

int CwMcuSensor::cw_read_calibrator_file(char *path, int *calib)
{
	FILE *fp;
	int readBytes = 0;

	ALOGE("cyw:%s:%d\n", __func__, __LINE__);

	fp = fopen(path, "r");
	if (!fp) {
		ALOGE("cyw:%s:%d: open File failed: %s\n", __func__, __LINE__, strerror(errno));
		return -1;
	}
	rewind(fp);

	readBytes = fscanf(fp, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
		                &calib[0],  &calib[1],  &calib[2],  &calib[3],  &calib[4],  &calib[5],  &calib[6],  &calib[7],  &calib[8],
                        &calib[9],  &calib[10], &calib[11], &calib[12], &calib[13], &calib[14], &calib[15], &calib[16], &calib[17], 
                        &calib[18], &calib[19], &calib[20], &calib[21], &calib[22], &calib[23], &calib[24], &calib[25], &calib[26], 
                        &calib[27], &calib[28], &calib[29]);

	if (readBytes <= 0) {
		ALOGE("cyw:%s:%d: Read file error:%s\n", __func__, __LINE__, strerror(errno));
		return readBytes;
	} else {
		for (int i = 0; i < 30; i++) {
			ALOGE("cyw:%s:%d: calib[%d] = %d\n", __func__, __LINE__, i, calib[i]);
		}
	}
	fclose(fp);
	return 	0;
}

int CwMcuSensor::cw_save_calibrator_file(char *path, int *calib)
{
	FILE *fp = NULL;
	int readBytes = 0;
	char value[128];

	fp = fopen(path, "w+");
	if (!fp) {
        ALOGE("cyw:%s:%d:open calibrator file error:path:%s\n", __func__, __LINE__, path);
		return -1;
	}

	snprintf(value, sizeof(value), "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
			calib[0],  calib[1],  calib[2],  calib[3],  calib[4],  calib[5],  calib[6],  calib[7],  calib[8],  calib[9],  
            calib[10], calib[11], calib[12], calib[13], calib[14], calib[15], calib[16], calib[17], calib[18], calib[19],
            calib[20], calib[21], calib[22], calib[23], calib[24], calib[25], calib[26], calib[27], calib[28], calib[29]);

	readBytes = fprintf(fp, "%s", value);

	if (readBytes <= 0) {
		ALOGE("cyw:%s:%d: Write file error:%s\n", __func__, __LINE__, strerror(errno));
		fclose(fp);
		return -1;
    } else {
        ALOGE("cyw:%s:%d:readBytes:%d\n", __func__, __LINE__, readBytes);
	}

	fclose(fp);
	return 0;
}

int CwMcuSensor::sysfs_set_input_attr(char *devpath, const char *attr, char *value, int len)
{
    char fname[PATH_MAX];
    int fd;
    snprintf(fname, sizeof(fname), "%s/%s", devpath, attr);
    fname[sizeof(fname) - 1] = '\0';
    fd = open(fname, O_WRONLY);
    if (fd < 0) {
        ALOGE("cyw:%s:%d:open %s failed!\n", __func__, __LINE__, fname);
        return -errno;
    }

    if (write(fd, value, (size_t)len) < 0) {
        ALOGE("cyw:%s:%d:write %s failed!\n", __func__, __LINE__, fname);
        close(fd);
        return -errno;
    }
    close(fd);
    return 0;
}

int CwMcuSensor::sysfs_set_input_attr_by_int(char *devpath, const char *attr, int value)
{
    char buf[INT32_CHAR_LEN];
    int2a(buf, value, sizeof(buf));
    return sysfs_set_input_attr(devpath, attr, buf, sizeof(buf));
}
