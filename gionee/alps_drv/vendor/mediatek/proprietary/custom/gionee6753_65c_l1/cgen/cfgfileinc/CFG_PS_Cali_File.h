/*******************************************
  This file is added for psensor calibration
  *******************************************/

/* Gionee chengx 20140403 add for CR01167528 begin */
#ifndef _CFG_PS_CALI_FILE_H
#define _CFG_PS_CALI_FILE_H
//The record structure define of GN_PSENSOR_CALIBRATION nvram file
typedef struct _PS_CALI{
     unsigned  int Close;
	 unsigned  int far_away;
	 unsigned  int valid;
} PS_CALI;

//the record sieze and number of PS_CALI nvram file
#define CFG_FILE_PS_CALI_SIZE       sizeof(PS_CALI)
#define CFG_FILE_PS_CALI_TOTAL       1

#endif

/* Gionee chengx 20140403 add for CR01167528 end */
