#============================================================================
#  Name:
#    Android.mk
#
#  Description:
#    Main Makefile to build all TS Player binaries with the NDK
#
#  Copyright (c) 2010-2015  Immersion Corporation.  All rights reserved.
#                           Immersion Corporation Confidential and Proprietary
#
#  Merge:
#    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
#============================================================================

LOCAL_PATH := $(call my-dir)
 
include $(CLEAR_VARS)
LOCAL_MODULE := libImmVibeJ.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := libpparam.so
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := immvibed
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_CLASS := EXECUTABLES
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/bin
LOCAL_SRC_FILES := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)