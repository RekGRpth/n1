/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary
**
** File:
**     Device.java
**
** Description:
**     Java Device class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion.android;

/**
 * Encapsulates a device handle representing an open device on the Android platform.
 */
public class Device extends com.immersion.Device
{
    /**
     * Returns an instance of com.immersion.android.EffectHandle class
     */
    protected com.immersion.EffectHandle newEffectHandle(int deviceHandle, int effectHandle)
    {
        return new com.immersion.android.EffectHandle(deviceHandle, effectHandle);
    }
    
    /**
     * Calls {@link #close close} upon object finalization.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error closing the device.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    protected void finalize()
    {
        if (ImmVibe.VIBE_INVALID_DEVICE_HANDLE_VALUE != deviceHandle) close();
    }
}
