/*
** =============================================================================
** Copyright (c) 2009-2014 Immersion Corporation.  All rights reserved.
**                         Immersion Corporation Confidential and Proprietary
**
** File:
**     ImmVibe.java
**
** Description:
**     Java ImmVibe API wrapper class for Android.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion.android;

/**
 * Implements the {@link com.immersion.ImmVibeAPI} interface on Android.
 */
public class ImmVibe extends com.immersion.ImmVibe
{
    public void initialize()
    {
        Initialize2();
    }
    public void terminate()
    {
        Terminate2();

        super.terminate();
    }
    public int getDeviceCount()
    {
        return GetDeviceCount();
    }
    public int getDeviceState(int deviceIndex)
    {
        return GetDeviceState(deviceIndex);
    }
    public boolean getDeviceCapabilityBool(int deviceIndex, int devCapType)
    {
        return GetDeviceCapabilityBool(deviceIndex, devCapType);
    }
    public int getDeviceCapabilityInt32(int deviceIndex, int devCapType)
    {
        return GetDeviceCapabilityInt32(deviceIndex, devCapType);
    }
    public String getDeviceCapabilityString(int deviceIndex, int devCapType)
    {
        return GetDeviceCapabilityString(deviceIndex, devCapType);
    }
    public int openDevice(int deviceIndex)
    {
         return OpenDevice2(deviceIndex);
    }
    /**
     * @deprecated  As of release 3.4, a license key is no longer required for
     * third-party developers.  This method is replaced by
     * <code>{@link com.immersion.ImmVibeAPI#openDevice(int)}</code>,
     * however <code>openDevice</code> should not be called directly.  Instead, developers are
     * encouraged to use the <code>{@link com.immersion.Device Device}</code> class as shown here:
     * <br><br>
     * <code>
     * Device device = Device.newDevice(deviceIndex);
     * </code>
     * <br><br>
     * Device manufacturers may choose to set a license key to unlock higher priority levels. This
     * may be done as follows:
     * <br><br>
     * <code>
     * device.setPropertyString(ImmVibe.VIBE_DEVPROPTYPE_LICENSE_KEY, "license key from Immersion");
     * </code>
     * <br><br>
     * Developers who continue to use <code>openDevice</code> must close each device handle by
     * calling <code>{@link com.immersion.ImmVibeAPI#closeDevice(int) closeDevice}</code> when the
     * device is no longer needed.  Failure to close device handles will result in memory leaks.
     */
    public int openDevice(int deviceIndex, String licenseKey)
    {
        int ret = OpenDevice2(deviceIndex);
        if((licenseKey != null) && !(licenseKey.equals("")))
        {
            try
            {
                setDevicePropertyString(ret, ImmVibe.VIBE_DEVPROPTYPE_LICENSE_KEY, licenseKey);
            }
            catch (Exception e)
            {
                CloseDevice2(ret);
                throw new java.lang.RuntimeException(e.getMessage());
            }
        }
        return ret;
    }
    public int openCompositeDevice(int numDevice)
    {
         return OpenCompositeDevice2(null, numDevice);
    }
    /**
     * @deprecated  As of release 3.4, a license key is no longer required for
     * third-party developers.  This method is replaced by
     * <code>{@link com.immersion.ImmVibeAPI#openCompositeDevice(int)}</code>,
     * however <code>openCompositeDevice</code> should not be called directly.  Instead, developers
     * are encouraged to use the {@link com.immersion.Device Device} class as shown here:
     * <br><br>
     * <code>
     * Device device = Device.newDevice();
     * </code>
     * <br><br>
     * Device manufacturers may choose to set a license key to unlock higher priority levels. This
     * may be done as follows:
     * <br><br>
     * <code>
     * device.setPropertyString(ImmVibe.VIBE_DEVPROPTYPE_LICENSE_KEY, "license key from Immersion");
     * </code>
     * <br><br>
     * Developers who continue to use <code>openCompositeDevice</code> must close each device
     * handle by calling <code>{@link com.immersion.ImmVibeAPI#closeDevice(int) closeDevice}</code>
     * when the device is no longer needed.  Failure to close device handles will result in memory
     * leaks.
     */
    public int openCompositeDevice(int array[], int numDevice, String licenseKey)
    {
         int ret = OpenCompositeDevice2(null, numDevice);
         if((licenseKey != null) && !(licenseKey.equals("")))
         {
             try
             {
                 setDevicePropertyString(ret, ImmVibe.VIBE_DEVPROPTYPE_LICENSE_KEY, licenseKey);
             }
             catch (Exception e)
             {
                 CloseDevice2(ret);
                 throw new java.lang.RuntimeException(e.getMessage());
             }
         }
         return ret;
    }
    public void closeDevice(int deviceHandle)
    {
        if (deviceHandle != ImmVibe.VIBE_INVALID_DEVICE_HANDLE_VALUE)
        {
            CloseDevice2(deviceHandle);
        }
    }
    public boolean getDevicePropertyBool(int deviceHandle, int devPropType)
    {
         return GetDevicePropertyBool(deviceHandle, devPropType);
    }
    public void setDevicePropertyBool(int deviceHandle, int devPropType, boolean devPropValue)
    {
         SetDevicePropertyBool(deviceHandle, devPropType, devPropValue);
    }
    public int getDevicePropertyInt32(int deviceHandle, int devPropType)
    {
         return GetDevicePropertyInt32(deviceHandle, devPropType);
    }
    public void setDevicePropertyInt32(int deviceHandle, int devPropType, int devPropValue)
    {
         SetDevicePropertyInt32(deviceHandle, devPropType, devPropValue);
    }
    public String getDevicePropertyString(int deviceHandle, int devPropType)
    {
         return GetDevicePropertyString(deviceHandle, devPropType);
    }
    public void setDevicePropertyString(int deviceHandle, int devPropType, String devPropValue)
    {
         SetDevicePropertyString(deviceHandle, devPropType, devPropValue);
    }
    public int getDeviceKernelParameter(int deviceIndex, int kernelParamID)
    {
         return GetDeviceKernelParameter(deviceIndex, kernelParamID);
    }
    public int playIVTEffect(int deviceHandle, byte ivt[], int effectIndex)
    {
         return PlayIVTEffect(deviceHandle, ivt, effectIndex);
    }
    public int playIVTEffectRepeat(int deviceHandle, byte ivt[], int effectIndex, byte repeat)
    {
         return PlayIVTEffectRepeat(deviceHandle, ivt, effectIndex, repeat);
    }
    public int playMagSweepEffect(int deviceHandle, int duration, int magnitude, int style, int attackTime, int attackLevel, int fadeTime, int fadeLevel)
    {
         return PlayMagSweepEffect(deviceHandle, duration, magnitude, style, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public int playPeriodicEffect(int deviceHandle, int duration, int magnitude, int period, int styleAndWaveType, int attackTime, int attackLevel, int fadeTime, int fadeLevel)
    {
         return PlayPeriodicEffect(deviceHandle, duration, magnitude, period, styleAndWaveType, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public int playWaveformEffect(int deviceHandle, byte data[], int dataSize, int sampleRate, int bitDepth, int magnitude)
    {
        return PlayWaveformEffect(deviceHandle, data, dataSize, sampleRate, bitDepth, magnitude);
    }
    public int appendWaveformEffect(int deviceHandle, int effectHandle, byte data[], int dataSize, int sampleRate, int bitDepth, int magnitude)
    {
        return AppendWaveformEffect(deviceHandle, effectHandle, data, dataSize, sampleRate, bitDepth, magnitude);
    }
    public void modifyPlayingMagSweepEffect(int deviceHandle, int effectHandle, int duration, int magnitude, int style, int attackTime, int attackLevel, int fadeTime, int fadeLevel)
    {
         ModifyPlayingMagSweepEffect(deviceHandle, effectHandle, duration, magnitude, style, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public void modifyPlayingPeriodicEffect(int deviceHandle, int effectHandle, int duration, int magnitude, int period, int styleAndWaveType, int attackTime, int attackLevel, int fadeTime, int fadeLevel)
    {
         ModifyPlayingPeriodicEffect(deviceHandle, effectHandle, duration, magnitude, period, styleAndWaveType, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public int getEffectState(int deviceHandle, int effectHandle)
    {
         return GetEffectState(deviceHandle, effectHandle);
    }
    public void pausePlayingEffect(int deviceHandle, int effectHandle)
    {
         PausePlayingEffect(deviceHandle, effectHandle);
    }
    public void resumePausedEffect(int deviceHandle, int effectHandle)
    {
         ResumePausedEffect(deviceHandle, effectHandle);
    }
    public void stopPlayingEffect(int deviceHandle, int hEffect)
    {
         StopPlayingEffect(deviceHandle, hEffect);
    }
    public void stopAllPlayingEffects(int deviceHandle)
    {
         StopAllPlayingEffects(deviceHandle);
    }
    public int playIVTInterpolatedEffect(int deviceHandle, byte ivt[], int effectIndex, int interpolant)
    {
         return PlayIVTInterpolatedEffect(deviceHandle, ivt, effectIndex, interpolant);
    }
    public void modifyPlayingInterpolatedEffectInterpolant(int deviceHandle, int effectHandle, int interpolant)
    {
         ModifyPlayingInterpolatedEffectInterpolant(deviceHandle, effectHandle, interpolant);
    }
    public int createStreamingEffect(int deviceHandle)
    {
         return CreateStreamingEffect(deviceHandle);
    }
    public void destroyStreamingEffect(int deviceHandle, int effectHandle)
    {
         DestroyStreamingEffect(deviceHandle, effectHandle);
    }
    public void playStreamingSample(int deviceHandle, int effectHandle, byte streamingSample[], int size)
    {
         PlayStreamingSample(deviceHandle, effectHandle, streamingSample, size);
    }
    public void playStreamingSampleWithOffset(int deviceHandle, int effectHandle, byte streamingSample[], int size, int offsetTime)
    {
         PlayStreamingSampleWithOffset(deviceHandle, effectHandle, streamingSample, size, offsetTime);
    }
    public byte[] getBuiltInEffects()
    {
         return GetBuiltInEffects();
    }
    /**
     * @deprecated  As of release 3.4, developers are encouraged to use the
     * <code>{@link com.immersion.Device Device}</code>, <code>{@link com.immersion.EffectHandle EffectHandle}</code>
     * and <code>{@link com.immersion.IVTBuffer IVTBuffer}</code> classes as shown:
     * <br><br>
     * <code>
     * EffectHandle effHandle = device.playIVTEffect(IVTBuffer.getBuiltInEffects(), effectIndex);
     * </code>
     */
    public int playBuiltInEffect(int deviceHandle, int effectIndex)
    {
        return PlayIVTEffect(deviceHandle, GetBuiltInEffects(), effectIndex);
    }
    /**
     * @deprecated  As of release 3.4, developers are encouraged to use the
     * <code>{@link com.immersion.Device Device}</code>, <code>{@link com.immersion.EffectHandle EffectHandle}</code>
     * and <code>{@link com.immersion.IVTBuffer IVTBuffer}</code> classes as shown:
     * <br><br>
     * <code>
     * EffectHandle effHandle = device.playIVTEffectRepeat(IVTBuffer.getBuiltInEffects(), effectIndex, repeat);
     * </code>
     */
    public int playBuiltInEffectRepeat(int deviceHandle, int effectIndex, byte repeat)
    {
         return PlayIVTEffectRepeat(deviceHandle, GetBuiltInEffects(), effectIndex, repeat);
    }
    public int getIVTEffectCount(byte ivt[])
    {
         return GetIVTEffectCount(ivt);
    }
    public String getIVTEffectName(byte ivt[], int effectIndex)
    {
         return GetIVTEffectName(ivt, effectIndex);
    }
    public int getIVTEffectIndexFromName(byte ivt[], String effectName)
    {
         return GetIVTEffectIndexFromName(ivt, effectName);
    }
    public int getIVTEffectType(byte ivt[], int effectIndex)
    {
         return GetIVTEffectType(ivt, effectIndex);
    }
    public int getIVTEffectDuration(byte ivt[], int effectIndex)
    {
         return GetIVTEffectDuration(ivt, effectIndex);
    }
    public void getIVTMagSweepEffectDefinition(byte ivt[], int effectIndex, int duration[], int magnitude[], int style[], int attackTime[], int attackLevel[], int fadeTime[], int fadeLevel[])
    {
         GetIVTMagSweepEffectDefinition(ivt, effectIndex, duration, magnitude, style, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public void getIVTPeriodicEffectDefinition(byte ivt[], int effectIndex, int duration[], int magnitude[], int period[], int styleAndWaveType[], int attackTime[], int attackLevel[], int fadeTime[], int fadeLevel[])
    {
         GetIVTPeriodicEffectDefinition(ivt, effectIndex, duration, magnitude, period, styleAndWaveType, attackTime, attackLevel, fadeTime, fadeLevel);
    }
    public void saveIVTFile(byte ivt[], String pathName)
    {
         SaveIVTFile(ivt, pathName);
    }
    public void deleteIVTFile(String pathName)
    {
         DeleteIVTFile(pathName);
    }
    public byte[] initializeIVTBuffer(int bufferSize)
    {
         return InitializeIVTBuffer(bufferSize);
    }
    public int getIVTSize(byte ivt[])
    {
         return GetIVTSize2(ivt);
    }
    /**
     * @deprecated  As of release 3.4, it is no longer necessary to pass the size parameter
     * to this method.  This method is replaced by <code>{@link #getIVTSize(byte[])}</code>,
     * however <code>getIVTSize</code> should not be called directly.  Instead, developers are
     * encouraged to use the <code>{@link com.immersion.IVTBuffer IVTBuffer}<code> class as shown:
     * <br><br>
     * <code>
     * IVTBuffer ivt = new IVTBuffer(ivt);<br>
     * int ivtSize = ivt.getSize();
     * </code>
     */
    public int getIVTSize(byte ivt[], int bufferSize)
    {
         return GetIVTSize2(ivt);
    }
    public byte[] insertIVTElement(byte ivt[], int timelineIndex, int ivtElement[], byte[] ivtElementData)
    {
         return InsertIVTElement2(ivt, timelineIndex, ivtElement, ivtElementData);
    }
    /**
     * @deprecated  As of release 3.4, it is no longer necessary to pass the size parameter
     * to this method.  This method is replaced by
     * <code>{@link #insertIVTElement(byte[], int, int[], byte[])}</code>,
     * however <code>insertIVTElement</code> should not be called directly.  Instead, developers are
     * encouraged to use the <code>{@link com.immersion.IVTBuffer IVTBuffer}<code> class as shown:
     * <pre>
     * <code>
     * IVTBuffer ivt = new IVTBuffer(bufferSize);
     * PeriodicEffectDefinition def = new PeriodicEffectDefinition(
     *         1000,                       // Duration,
     *         ImmVibe.VIBE_MAX_MAGNITUDE, // Magnitude,
     *         10,                         // Period,
     *         ImmVibe.VIBE_DEFAULT_STYLE, // Style and WaveType,
     *         0,                          // Attack Time,
     *         0,                          // Attack Level,
     *         0,                          // Fade Time,
     *         0,                          // Fade Level,
     *         0                           // Actuator Index
     *         );
     * IVTPeriodicElement elem = new IVTPeriodicElement(0, def);
     * ivt.insertElement(timelineIndex, elem);
     * </code>
     * </pre>
     */
    public byte[] insertIVTElement(byte ivt[], int bufferSize, int timelineIndex, int ivtElement[])
    {
         return InsertIVTElement2(ivt, timelineIndex, ivtElement, null);
    }
    public byte[] removeIVTElement(byte ivt[], int timelineIndex, int elementIndex)
    {
         return RemoveIVTElement2(ivt, timelineIndex, elementIndex);
    }
    /**
     * @deprecated  As of release 3.4, it is no longer necessary to pass the size parameter
     * to this method.  This method is replaced by
     * <code>{@link #removeIVTElement(byte[], int, int)}</code>,
     * however <code>removeIVTElement</code> should not be called directly.  Instead, developers are
     * encouraged to use the <code>{@link com.immersion.IVTBuffer IVTBuffer}<code> class as shown:
     * <pre>
     * <code>
     * IVTBuffer ivt; // initialized elsewhere
     * ivt.removeElement(timelineIndex, elementIndex);
     * </code>
     * </pre>
     */
    public byte[] removeIVTElement(byte ivt[], int bufferSize, int timelineIndex, int elementIndex)
    {
         return RemoveIVTElement2(ivt, timelineIndex, elementIndex);
    }
    public int[] readIVTElement(byte ivt[], int timelineIndex, int elementIndex)
    {
         return ReadIVTElement2(ivt, timelineIndex, elementIndex);
    }
    /**
     * @deprecated  As of release 3.4, it is no longer necessary to pass the size parameter
     * to this method.  This method is replaced by
     * <code>{@link #readIVTElement(byte[], int, int)}</code>,
     * however <code>removeIVTElement</code> should not be called directly.  Instead, developers are
     * encouraged to use the <code>{@link com.immersion.IVTBuffer IVTBuffer}<code> class as shown:
     * <pre>
     * <code>
     * IVTBuffer ivt; // initialized elsewhere
     * IVTElement elem = ivt.readElement(timelineIndex, elementIndex);
     * </code>
     * </pre>
     */
    public int[] readIVTElement(byte ivt[], int bufferSize, int timelineIndex, int elementIndex)
    {
         return ReadIVTElement2(ivt, timelineIndex, elementIndex);
    }
    public byte[] readIVTElementData(byte[] ivt, int timelineIndex, int elementIndex)
    {
         return ReadIVTElementData(ivt, timelineIndex, timelineIndex);
    }
    public int playEnhancedWaveformEffect(int deviceHandle, byte data[], int sampleRate, int format, int magnitude, int secureMode)
    {
        return PlayEnhancedWaveformEffect(deviceHandle, data, sampleRate, format, magnitude, secureMode);
    }
    public int appendEnhancedWaveformEffect(int deviceHandle, int effectHandle, byte data[], int sampleRate, int format, int magnitude, int secureMode)
    {
        return AppendEnhancedWaveformEffect(deviceHandle, effectHandle, data, sampleRate, format, magnitude, secureMode);
    }
    public int replaceEnhancedWaveformEffect(int deviceHandle, int effectHandle, byte data[], int sampleRate, int format, int magnitude, int secureMode)
    {
        return ReplaceEnhancedWaveformEffect(deviceHandle, effectHandle, data, sampleRate, format, magnitude, secureMode);
    }
    public int getEffectRemainingDuration(int deviceHandle, int effectHandle)
    {
         return GetEffectRemainingDuration(deviceHandle, effectHandle);
    }
    public byte[] loadIVTFromXIVT(int deviceHandle, byte xivt[])
    {
         return LoadIVTFromXIVT(deviceHandle, xivt.length, xivt);
    }
    static
    {
         System.loadLibrary("ImmVibeJ");
    }

    // API
    private native void Initialize2();
    private native void Terminate2();

    // Device
    private native int GetDeviceCount();
    private native int GetDeviceState(int deviceIndex);
    private native boolean GetDeviceCapabilityBool(int deviceIndex, int devCapType);
    private native int GetDeviceCapabilityInt32(int deviceIndex, int devCapType);
    private native String GetDeviceCapabilityString(int deviceIndex, int devCapType);
    private native int OpenDevice2(int deviceIndex);
    private native int OpenCompositeDevice2(int pDeviceIndex[], int nDeviceNumber);
    private native void CloseDevice2(int deviceHandle);
    private native boolean GetDevicePropertyBool(int deviceHandle, int devPropType);
    private native void SetDevicePropertyBool(int deviceHandle, int devPropType, boolean devPropValue);
    private native int GetDevicePropertyInt32(int deviceHandle, int devPropType);
    private native void SetDevicePropertyInt32(int deviceHandle, int devPropType, int devPropValue);
    private native String GetDevicePropertyString(int deviceHandle, int devPropType);
    private native void SetDevicePropertyString(int deviceHandle, int devPropType, String devPropValue);
    private native int GetDeviceKernelParameter(int deviceIndex, int kernelParamID);

    // Player
    private native int PlayIVTEffect(int deviceHandle, byte ivt[], int effectIndex);
    private native int PlayIVTEffectRepeat(int deviceHandle, byte ivt[], int effectIndex, byte repeat);
    private native int PlayMagSweepEffect(int deviceHandle, int duration, int magnitude, int style, int attackTime, int attackLevel, int fadeTime, int fadeLevel);
    private native int PlayPeriodicEffect(int deviceHandle, int duration, int magnitude, int period, int styleAndWaveType, int attackTime, int attackLevel, int fadeTime, int fadeLevel);
    private native int PlayWaveformEffect(int deviceHandle, byte data[], int dataSize, int sampleRate, int bitDepth, int magnitude);
    private native int AppendWaveformEffect(int deviceHandle, int effectHandle, byte data[], int dataSize, int sampleRate, int bitDepth, int magnitude);
    private native void ModifyPlayingMagSweepEffect(int deviceHandle, int effectHandle, int duration, int magnitude, int style, int attackTime, int attackLevel, int fadeTime, int fadeLevel);
    private native void ModifyPlayingPeriodicEffect(int deviceHandle, int effectHandle, int duration, int magnitude, int period, int styleAndWaveType, int attackTime, int attackLevel, int fadeTime, int fadeLevel);
    private native int GetEffectState(int deviceHandle, int effectHandle);
    private native void PausePlayingEffect(int deviceHandle, int effectHandle);
    private native void ResumePausedEffect(int deviceHandle, int effectHandle);
    private native void StopPlayingEffect(int deviceHandle, int hEffect);
    private native void StopAllPlayingEffects(int deviceHandle);
    private native int PlayIVTInterpolatedEffect(int deviceHandle, byte ivt[], int effectIndex, int interpolant);
    private native void ModifyPlayingInterpolatedEffectInterpolant(int deviceHandle, int effectHandle, int interpolant);
    private native int PlayEnhancedWaveformEffect(int hDeviceHandle, byte pData[], int nSampleRate, int nFormat, int nMagnitude, int nSecureMode);
    private native int AppendEnhancedWaveformEffect(int hDeviceHandle, int hEffectHandle, byte pData[], int nSampleRate, int nFormat, int nMagnitude, int nSecureMode);
    private native int ReplaceEnhancedWaveformEffect(int hDeviceHandle, int hEffectHandle, byte pData[], int nSampleRate, int nFormat, int nMagnitude, int nSecureMode);
    private native int GetEffectRemainingDuration(int deviceHandle, int effectHandle);

    // Streaming Effect Player
    private native int CreateStreamingEffect(int deviceHandle);
    private native void DestroyStreamingEffect(int deviceHandle, int effectHandle);
    private native void PlayStreamingSample(int deviceHandle, int effectHandle, byte streamingSample[], int size);
    private native void PlayStreamingSampleWithOffset(int deviceHandle, int effectHandle, byte streamingSample[], int size, int offsetTime);

    // IVT
    private native byte[] GetBuiltInEffects();
    private native int GetIVTEffectCount(byte ivt[]);
    private native String GetIVTEffectName(byte ivt[], int effectIndex);
    private native int GetIVTEffectIndexFromName(byte ivt[], String effectName);
    private native int GetIVTEffectType(byte ivt[], int effectIndex);
    private native int GetIVTEffectDuration(byte ivt[], int effectIndex);
    private native void GetIVTMagSweepEffectDefinition(byte ivt[], int effectIndex, int duration[], int magnitude[], int style[], int attackTime[], int attackLevel[], int fadeTime[], int fadeLevel[]);
    private native void GetIVTPeriodicEffectDefinition(byte ivt[], int effectIndex, int duration[], int magnitude[], int period[], int styleAndWaveType[], int attackTime[], int attackLevel[], int fadeTime[], int fadeLevel[]);
    private native void SaveIVTFile(byte ivt[], String pathName);
    private native void DeleteIVTFile(String pathName);

    // IVT Writer
    private native byte[] InitializeIVTBuffer(int bufferSize);
    private native int GetIVTSize2(byte ivt[]);
    private native byte[] InsertIVTElement2(byte ivt[], int timelineIndex, int ivtElement[], byte[] ivtElementData);
    private native byte[] RemoveIVTElement2(byte ivt[], int timelineIndex, int elementIndex);
    private native int[] ReadIVTElement2(byte ivt[], int timelineIndex, int elementIndex);
    private native byte[] ReadIVTElementData(byte ivt[], int timelineIndex, int elementIndex);

    // XIVT
    private native byte[] LoadIVTFromXIVT(int deviceHandle, int bufferSize, byte xivt[]);
}
