/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     WaveformEffectDefinition.java
**
** Description:
**     Java ImmVibe class declaration for Android.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Encapsulates the parameters of a Waveform effect.
 */
public class WaveformEffectDefinition
{
    private byte[]  data;
    private int     dataSize;
    private int     sampleRate;
    private int     bitDepth;
    private int     magnitude;
    private int     actuatorIndex;

    /**
     * Initializes this Waveform effect with the given parameters.
     *
     * @param   data            PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   dataSize        Size of the PCM data in bytes. The size must be
     *                          greater than zero. The maximum supported size is
     *                          16 megabytes, or less if limited by platform
     *                          constraints.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     * @param   bitDepth        Bit depth of PCM data, or number of bits per
     *                          sample. The only supported values are 8 and 16;
     *                          that is, the Player supports only 8-bit and
     *                          16-bit PCM data.
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public WaveformEffectDefinition(byte[] data,
                                    int dataSize,
                                    int sampleRate,
                                    int bitDepth,
                                    int magnitude,
                                    int actuatorIndex)
    {
        setData(data);
        setDataSize(dataSize);
        setSampleRate(sampleRate);
        setBitDepth(bitDepth);
        setMagnitude(magnitude);
        setActuatorIndex(actuatorIndex);
    }

    /**
     * Sets the PCM data of this Waveform effect definition.
     *
     * @param   data            PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     */
    public void setData(byte[] data)
    {
        this.data = data;
    }

    /**
     * Gets the PCM data of this Waveform effect definition.
     *
     * @return                  PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * Sets the PCM data size of this Waveform effect definition.
     *
     * @param   dataSize        Size of the PCM data in bytes. The size must be
     *                          greater than zero. The maximum supported size is
     *                          16 megabytes, or less if limited by platform
     *                          constraints.
     */
    public void setDataSize(int dataSize)
    {
        this.dataSize = dataSize;
    }

    /**
     * Gets the PCM data size of this Waveform effect definition.
     *
     * @return                  Size of the PCM data in bytes. The size will be
     *                          greater than zero. The maximum supported size is
     *                          16 megabytes, or less if limited by platform
     *                          constraints.
     */
    public int getDataSize()
    {
        return dataSize;
    }

    /**
     * Sets the sampling rate of this Waveform effect definition.
     *
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     */
    public void setSampleRate(int sampleRate)
    {
        this.sampleRate = sampleRate;
    }

    /**
     * Gets the sampling rate of this Waveform effect definition.
     *
     * @return                  Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     */
    public int getSampleRate()
    {
        return sampleRate;
    }

    /**
     * Sets the bit depth of this Waveform effect definition.
     *
     * @param   bitDepth        Bit depth of PCM data, or number of bits per
     *                          sample. The only supported values are 8 and 16;
     *                          that is, the Player supports only 8-bit and
     *                          16-bit PCM data.
     */
    public void setBitDepth(int bitDepth)
    {
        this.bitDepth = bitDepth;
    }

    /**
     * Gets the bit depth of this Waveform effect definition.
     *
     * @return                  Bit depth of PCM data, or number of bits per
     *                          sample. The only supported values are 8 and 16;
     *                          that is, the Player supports only 8-bit and
     *                          16-bit PCM data.
     */
    public int getBitDepth()
    {
        return bitDepth;
    }

    /**
     * Sets the magnitude of this Waveform effect definition.
     *
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     */
    public void setMagnitude(int magnitude)
    {
        this.magnitude = magnitude;
    }

    /**
     * Gets the magnitude of this Waveform effect definition.
     *
     * @return                  Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     */
    public int getMagnitude()
    {
        return magnitude;
    }

    /**
     * Sets the actuator index of this Waveform effect definition.
     *
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public void setActuatorIndex(int actuatorIndex)
    {
        this.actuatorIndex = actuatorIndex;
    }

    /**
     * Gets the actuator index of this Waveform effect definition.
     *
     * @return                  Index of the actuator on which to play the
     *                          effect when the effect is in an
     *                          {@link IVTMagSweepElement}, and the
     *                          {@link IVTMagSweepElement} is in an
     *                          {@link IVTBuffer}, and the effect is played
     *                          from the {@link IVTBuffer} using
     *                          {@link Device#playIVTEffect Device.playIVTEffect}
     *                          on a composite device supporting effects
     *                          targeting multiple actuators simultaneously.
     *                          This value is not used when playing the effect
     *                          on its own, outside of an {@link IVTBuffer},
     *                          even when played on a composite device. The
     *                          actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public int getActuatorIndex()
    {
        return actuatorIndex;
    }
}
