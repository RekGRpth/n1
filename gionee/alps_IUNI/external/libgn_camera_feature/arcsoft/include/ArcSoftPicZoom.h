/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for night shot.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-09-07
 *
 *************************************************************************************/
 
#ifndef ANDROID_ARCSOFT_PIC_ZOOM_H
#define ANDROID_ARCSOFT_PIC_ZOOM_H

#include "arcsoft_piczoom.h"
#include "ammem.h"

#include "GNCameraFeatureDefs.h"
#include <pthread.h>

#define MAX_PICZOOM_INPUT_IMAGES 6//MAX_DZ_INPUT_IMAGES

enum {
	PICZOOM_CMD_TYPE_NONE	= 0,
	PICZOOM_CMD_TYPE_INIT	= 1,
	PICZOOM_CMD_TYPE_DEINIT = 2,
	PICZOOM_CMD_TYPE_EXIT	= 3,
};

namespace android { 
class ArcSoftPicZoom {
public:
    ArcSoftPicZoom();
	~ArcSoftPicZoom();
	
	int32 init();
    void  deinit();
	int32 getBurstCnt();
	int32 processPicZoom(LPASVLOFFSCREEN param);
	int32 enablePicZoom(PicZoomParam const param);
	int32 disablePicZoom();

private:
	int32 initEngine(MInt32 width, MInt32 height, GNImgFormat format);
	bool startEnhance(LPASVLOFFSCREEN param);
	int32 getMemSize(MUInt32 u32PixelArrayFormat, int stride, int scanline);
	
private:
	static void* defferedWorkRoutine(void* data);

private:

	MVoid* mMem;
	MHandle mEnhancer;

	ADZ_INPUTINFO mSrcInput;
	
	ASVLOFFSCREEN mSrcImages[MAX_DZ_INPUT_IMAGES];
	MLong mImageNum; 
	MInt32 mImgWidth;
	MInt32 mImgHeight;
	GNImgFormat mFormat;

	ASVLOFFSCREEN mDstImg;
	ADZ_PARAM mParam;
	MFloat mScale;
	bool mInitialized;

	pthread_mutex_t mMutex;
	pthread_cond_t mCond ;
	pthread_t mTid;
	int mCmdType;
};
};
#endif /* ANDROID_ARCSOFT_PIC_ZOOM_H */
