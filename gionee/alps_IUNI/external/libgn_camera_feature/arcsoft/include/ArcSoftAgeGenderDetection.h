/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for age gender detection.
 *
 * Author : zhuangxiaojian
 * Email  : zhuangxj@gionee.com
 * Date   : 2014-10-28
 *
 *************************************************************************************/

#ifndef ANDROID_ARCSOFT_AGEGENDER_DETECTION_H
#define ANDROID_ARCSOFT_AGEGENDER_DETECTION_H

#include "abstypes.h"
#include "beauty_shot.h"

#include "asvloffscreen.h"
#include "ammem.h"
#include "merror.h"

#include "GNCameraFeatureListener.h"
#include "GNCameraFeatureDefs.h"

#include "ArcSoftFaceProcess.h"


namespace android {
class ArcSoftAgeGenderDetection {
public:
	ArcSoftAgeGenderDetection();
	~ArcSoftAgeGenderDetection();
	
	int32 init();
	void  deinit();
	int32 setCameraListener(GNCameraFeatureListener* listener);
	int32 processAgeGenderDetection(LPASVLOFFSCREEN srcImg, GNDataType dataType);
	int32 enableAgeGenderDetection();
	int32 disableAgeGenderDetection();


private:	
	bool mInitialized;
	ArcSoftFaceProcess* mArcSoftFaceProcess;
	GNCameraFeatureListener* mListener;
};
};
#endif/*ANDROID_ARCSOFT_AGEGENDER_DETECTION_H*/

