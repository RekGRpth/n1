/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#ifndef GN_CAMERA_QUEUE_H
#define GN_CAMERA_QUEUE_H

#include <pthread.h>
#include "GNDlist.h"

namespace android {

typedef bool (*match_fn_data)(void *data, void *user_data, void *match_data);
typedef void (*release_data_fn)(void* data, void *user_data);
typedef bool (*match_fn)(void *data, void *user_data);

class GNCameraQueue {

public:
    GNCameraQueue();
    GNCameraQueue(release_data_fn data_rel_fn, void *user_data);
    virtual ~GNCameraQueue();

	/****************************************************************************
	 * FUNCTION   : enqueue
	 *
	 * DESCRIPTION: enqueue data into the queue
	 *
	 * PARAMETERS :
	 *   @data    : data to be enqueued
	 *
	 * RETURN     : true -- success; false -- failed
	 ***************************************************************************/
    bool enqueue(void *data);

	/****************************************************************************
	 * FUNCTION   : enqueueWithPriority
	 *
	 * DESCRIPTION: enqueue data into queue with priority, will insert into the
	 *              head of the queue
	 *
	 * PARAMETERS :
	 *   @data    : data to be enqueued
	 *
	 * RETURN     : true -- success; false -- failed
	 ***************************************************************************/
    bool enqueueWithPriority(void *data);

	/****************************************************************************
	 * FUNCTION   : flush
	 *
	 * DESCRIPTION: flush all nodes from the queue, queue will be empty after this
	 *              operation.
	 *
	 * PARAMETERS : None
	 *
	 * RETURN     : None
	 ***************************************************************************/
    void flush();

	/****************************************************************************
	 * FUNCTION   : flushNodes
	 *
	 * DESCRIPTION: flush only specific nodes, depending on
	 *              the given matching function.
	 *
	 * PARAMETERS :
	 *   @match   : matching function
	 *
	 * RETURN     : None
	 ***************************************************************************/
    void flushNodes(match_fn match);

	/****************************************************************************
	 * FUNCTION   : flushNodes
	 *
	 * DESCRIPTION: flush only specific nodes, depending on
	 *              the given matching function.
	 *
	 * PARAMETERS :
	 *   @match   : matching function
	 *
	 * RETURN     : None
	 ***************************************************************************/
    void flushNodes(match_fn_data match, void *spec_data);

	/****************************************************************************
	 * FUNCTION   : dequeue
	 *
	 * DESCRIPTION: dequeue data from the queue
	 *
	 * PARAMETERS :
	 *   @bFromHead : if true, dequeue from the head
	 *                if false, dequeue from the tail
	 *
	 * RETURN     : data ptr. NULL if not any data in the queue.
	 ***************************************************************************/
    void* dequeue(bool bFromHead = true);

	/****************************************************************************
	 * FUNCTION   : isEmpty
	 *
	 * DESCRIPTION: return if the queue is empty or not
	 *
	 * PARAMETERS : None
	 *
	 * RETURN     : true -- queue is empty; false -- not empty
	 ***************************************************************************/
    bool isEmpty();

private:
    typedef struct {
        struct dlist list;
        void* data;
    } DlistNode_t;

    DlistNode_t m_head; // dummy head
    int m_size;
    pthread_mutex_t m_lock;
    release_data_fn m_dataFn;
    void * m_userData;
};

}; // namespace android

#endif /* GN_CAMERA_QUEUE_H */
