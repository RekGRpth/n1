/*************************************************************************************
 * 
 * Description:
 * 	Defines Gionee APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-03-15
 *
 *************************************************************************************/

#ifndef FRAME_PROCESSOR_H_
#define FRAME_PROCESSOR_H_

#include <memory.h>
#include <time.h>
#include <pthread.h>
#include <android/log.h>
#include <string.h>

#include "AlgImgDefog.h"
#include "GNCameraFeatureDefs.h"

#define LOG_TAG "FrameProcessor"

typedef enum SceneMsgTypeTag {
	SCENE_MSG_TYPE_DESTROY,
	SCENE_MSG_TYPE_CLEAR,
	SCENE_MSG_TYPE_PROC,
} SceneMsgType_t;

typedef struct MsgSenceDetectTag {
	SceneMsgType_t type;
	IMGOFFSCREEN srcImg;
	uint32_t mode;
} MsgSceneDetect_t;

class SceneDetectionListener {
public:
	virtual ~SceneDetectionListener() {};
	virtual void onCallback(int result) = 0;
};

class FrameProcessor {
private: 
	FrameProcessor();
	~FrameProcessor();
	
public:
	int32_t init();
	void 	deinit();

	int32_t registerSceneDetectionListener(SceneDetectionListener* listener);
	void 	unregisterSceneDetectionListener();
	int32_t processFrame(PIMGOFFSCREEN srcImg, bool isPreview);
	int32_t getDefogDetectionResult();
	int32_t enableFrameProcess(int32_t maxWidth, int32_t maxHeight, GNImgFormat format, int32_t featureMask);
	void 	disableFrameProcess(int32_t featureMask);

public:
	static FrameProcessor* getInstance();
	
private:
	int32_t	initEngineInstance(int32_t width, int32_t height, uint32_t format);
	void	deinitEngineInstance();
    static void *defogDetectionRoutine(void *data);
	uint32_t getPixelFormat(GNImgFormat format);

private:
	DefogProcessor* mDefogProcessor;
	pthread_mutex_t mMutex;
	pthread_cond_t mCond ;
	pthread_mutex_t mThreadMutex ;

	pthread_t mTidDefogDetection;

    MsgSceneDetect_t mMsgScene;
	FogFlareDegree_t mFogFlareDegree;
	AlgDefogInitParm* mMemInitParm;	

	static FrameProcessor* mFrameProcessor;
    SceneDetectionListener* mDefogListener;
	bool mInitialized;
	int32_t mGNFrameProcessMask;

	int32_t mWidth;
	int32_t mHeight;
};
#endif /* FRAME_PROCESSOR_H_*/
