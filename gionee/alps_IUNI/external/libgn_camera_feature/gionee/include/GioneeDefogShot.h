/*************************************************************************************
 *
 * Description:
 *  Defines Gionee APIs for camera HAL
 *
 * Author : huangxiaofeng, carson.zang
 * Email  : huangxiaofeng@gionee.com, carson.zang@foxmail.com
 * Date   : 2015-01-17
 *
 *************************************************************************************/

#ifndef ANDROID_GIONEE_PRIVATE_DEFOG_H
#define ANDROID_GIONEE_PRIVATE_DEFOG_H

#include "GNCameraFeatureDefs.h"
#include "GNCameraFeatureListener.h"
#include "AlgImgDefog.h"
//#include "AlgCommon.h"
#include "FrameProcessor.h"

namespace android {

class GioneeDefogShot {
public:
    GioneeDefogShot();
    ~GioneeDefogShot();

    int32 init ();
    void  deinit();
    int32 enableDefogShot(GNImgFormat format);
    void disableDefogShot();
    int32 processDefogShot(PIMGOFFSCREEN imgSrc);
	
private:
	FrameProcessor* mFrameProcessor;
		
    bool mInitialized;
};

};
#endif /* */
