#ifndef V_SIGN_PRESENCE_GJKALMNXVNN
#define V_SIGN_PRESENCE_GJKALMNXVNN

#include "../object_presence.h"


namespace TouchlessA3D {

  /**
   * Emitted when V-Sign gestures are present in the input images to Engine#handleImage().
   *
   * Whenever a new V-Sign gesture is detected in an input image, a %VSignPresence
   * object with the Action START is emitted. After that, a %VSignPresence object
   * with the Action MOVEMENT is emitted for every input image in which the V-Sign
   * gesture is still seen. When the V-Sign gesture is no longer found in the input
   * images, a %VSignPresence object with the Action END is sent.  After that, no
   * more %VSignPresence objects are sent for that object id, but objects with a new
   * id will be sent if a V-Sign gesture is again seen in the input images.
   *
   * Copyright © 2014 Crunchfish AB. All rights reserved. All information herein is
   * or may be trade secrets of Crunchfish AB.
   */
  class TA3D_EXPORT VSignPresence : public ObjectPresence {
  public:
    static const GestureType TYPE = 4;

    VSignPresence(unsigned long long uTime, int nObjectId, Action eAction,
      int x, int y, int z, int prevX, int prevY, int prevZ, int nWidth, int nHeight);
    Gesture *clone() const;
    GestureType getType() const;
  };

}  // namespace TouchlessA3D


#endif
