#ifndef PARAMETERS_ONFOANFKJKSDNKSGRIE
#define PARAMETERS_ONFOANFKJKSDNKSGRIE

// Integer parameter. The default value 0 means that you are not using the
// region of interest functionality. If you set this paramater to a non-
// zero value, TouchlessA3D will use background movement areas and detected
// objects to crop the input image in order to support larger distances.
#define TA3D_PARAMETER_EXTENDED_RANGE                 1002

// Internal parameters below

// Integer parameter. Divisor for minimum cells a swipe has to go through
// per frame there are 32 cells so a value between 1 and 32 makes sense.
// This is referred to as a directional swipe - brightness changes that moves
// left/right in the input image over time.
// Default is 31
#define TA3D_PARAMETER_SWIPE_MIN_SIDE_STEP_DIVISOR    3000

// Integer parameter. Divisor for maximum cells a swipe can to go through
// per frame, there are 32 cells so a value between 1 and 32 makes sense.
// Default is 3
#define TA3D_PARAMETER_SWIPE_MAX_SIDE_STEP_DIVISOR    3001

// Integer parameter. How many cells a swipe can go through per frame
// without being detected as a swipe. A none-swipe can be explained as
// brightness changes that occur in about the same spatial space over time.
// Default is 1
#define TA3D_PARAMETER_SWIPE_NONE_SIDE_STEP           3002

// Integer parameter. How many neutral frames before an opposite swipe
// can be detected.
// Default is 0
#define TA3D_PARAMETER_SWIPE_BLOCK_COUNT              3003

// Integer parameter. Percentage of directional swipe compared to
// none-swipes that has to be ful-filled to generate a swipe event.
// Default is 50
#define TA3D_PARAMETER_SWIPE_NONE_DIFF                3004

// Integer parameter. Percentage of directional swipe compared to
// opppostie directional swipe that has to be ful-filled to
// generate a swipe event.
// Default is 52
#define TA3D_PARAMETER_SWIPE_DIRECTION_DIFF           3005

// Integer parameter. Threshold value to decide a swipe or no-swipe
// Default is 1500
#define TA3D_PARAMETER_SWIPE_THRESHOLD                3006

// Integer parameter 0 or 1. 1 to enable dynamic threshold 0 to disable
// Default is 0
#define TA3D_PARAMETER_SWIPE_DYNAMIC_THRESHOLD        3007

// Integer parameter. Threshold value to filter out the small peaks in the
// dynamic threshold.
// Only valid if dynamic threshold is enabled.
// Default is 300
#define TA3D_PARAMETER_SWIPE_TRIP_THRESHOLD           3008

// Integer parameter. Percentage of threshold to pass for swipes
// Only valid if dynamic threshold is enabled.
// Default is 70
#define TA3D_PARAMETER_SWIPE_THRESHOLD_FACTOR         3009

#endif
