LOCAL_PATH:= $(call my-dir)

#64bit 
include $(CLEAR_VARS)
LOCAL_MODULE := libgn_camera_feature
LOCAL_MODULE_TAGS:= optional
LOCAL_MULTILIB := 64

LOCAL_SRC_FILES:= GNCameraFeature64.cpp \
                  GNCameraFeatureBase.cpp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include
include $(BUILD_SHARED_LIBRARY)

#32bit
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= GNCameraFeature.cpp \
                  GNCameraFeatureBase.cpp 

LOCAL_MODULE:= libgn_camera_feature
LOCAL_MULTILIB := 32

LOCAL_C_INCLUDES += $(LOCAL_PATH)/include \
					$(LOCAL_PATH)/util \
					$(LOCAL_PATH)/util/common \
					$(TOP)/bionic \
					$(TOP)/bionic/libstdc++/include \
					$(TOP)/external/stlport/stlport/ \

#Gionee <zhangwu><2013-08-14> modify for CR00854504 begin
LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog
LOCAL_CFLAGS = -Wall -Wno-error
#Gionee <zhangwu><2013-08-14> modify for CR00854504 end

LOCAL_SHARED_LIBRARIES:= \
    libutils \
    libcutils \
	libcamera_client

LOCAL_SCALADO_LIB_ROOT_PATH:= $(LOCAL_PATH)/scalado/bin
LOCAL_LDFLAGS += \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmhdr2.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmlowlight.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmlowligt.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmphotoart.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmexif.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmautorama.a  \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/baseapi.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/excodecs.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmspeedview.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/codecs.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/capseng.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/facedetector.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmface.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/libFace.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmlocaltimewarp.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/osl_gcc.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmclearshot.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmextencoder.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmfacebeautification.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmimageenhance.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmimagefusion.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmjpegsqueeze.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/fmjpegtools.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/libFB.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/oslansi.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/oslbase.a \
	$(LOCAL_SCALADO_LIB_ROOT_PATH)/render2d_opengl.a

LOCAL_ARCSOFT_LIB_ROOT_PATH:= $(LOCAL_PATH)/arcsoft/bin
LOCAL_LDFLAGS += \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libarcsoft_beautyshot.so \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libamipengine.so \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libarcsoft_night_shot.so \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libarcsoft_nighthawk.so \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libarcsoft_piczoom.so \
	$(LOCAL_ARCSOFT_LIB_ROOT_PATH)/libarcsoft_picauto.so \


LOCAL_CRUNCHFISH_LIB_ROOT_PATH:= $(LOCAL_PATH)/crunchfish/libs/armeabi-v7a
LOCAL_LDFLAGS += \
	$(LOCAL_CRUNCHFISH_LIB_ROOT_PATH)/libtouchless_a3d.so

LOCAL_GIONEE_LIB_ROOT_PATH:= $(LOCAL_PATH)/gionee/bin
LOCAL_LDFLAGS += \
    $(LOCAL_GIONEE_LIB_ROOT_PATH)/libimage_defog.so

LOCAL_VISIDON_LIB_ROOT_PATH:= $(LOCAL_PATH)/visidon/bin/armeabi-v7a
LOCAL_LDFLAGS += \
	$(LOCAL_VISIDON_LIB_ROOT_PATH)/libVDSuperPhotoRT.so \
	$(LOCAL_VISIDON_LIB_ROOT_PATH)/libVDSuperPhotoRT-GLESv3.so \
	$(LOCAL_VISIDON_LIB_ROOT_PATH)/libVDSuperPhotoAPI.so \
	$(LOCAL_VISIDON_LIB_ROOT_PATH)/libVDSingleHDRAPI.so \

ifeq "$(strip $(GN_SCALADO_FEATURE_SUPPORT))" "yes"
LOCAL_C_INCLUDES += $(LOCAL_PATH)/scalado/include
LOCAL_STATIC_LIBRARIES += libscalado
endif

ifeq "$(strip $(GN_ARCSOFT_FEATURE_SUPPORT))" "yes" 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/arcsoft/include
LOCAL_STATIC_LIBRARIES += libarcsoft
endif

ifeq "$(strip $(GN_CRUNCHFISH_FEATURE_SUPPORT))" "yes" 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/crunchfish/include
LOCAL_STATIC_LIBRARIES += libcrunchfish
endif

ifeq "$(strip $(GN_GIONEE_FEATURE_SUPPORT))" "yes" 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/gionee/include
LOCAL_STATIC_LIBRARIES += libgionee
endif

ifeq "$(strip $(GN_VISIDON_FEATURE_SUPPORT))" "yes" 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/visidon/include
LOCAL_STATIC_LIBRARIES += libvisidon
endif

ifeq "$(strip $(GN_CAMERA_FEATURE_SUPPORT))" "yes"
include $(BUILD_SHARED_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))
endif

