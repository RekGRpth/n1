/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee camera adapter.
 *<CR01402918>
 * Author : litao
 * Email  : litao@gionee.com
 * Date   : 2014/9/28
 *
 *************************************************************************************/
#ifndef _MTK_HAL_CAMADAPTER_GIONEE_INC_GNDEFAULTCAMADAPTER_H_
#define _MTK_HAL_CAMADAPTER_GIONEE_INC_GNDEFAULTCAMADAPTER_H_
#include <inc/GnBaseCamAdapter.h>

//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
#include "inc/GnPerfService.h"
//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end

class GnDefaultCamAdapter:public GnBaseCamAdapter
{
	public:
		GnDefaultCamAdapter(BaseCamAdapter* owner);
		virtual ~GnDefaultCamAdapter();
		static  GnDefaultCamAdapter*    createInstance(BaseCamAdapter*const owner);
		virtual void   					destroyInstance(); 
		virtual void                    handlerReturnYuvData(
																	int64_t    i8Timestamp, 
																    uint32_t   u4BufSize, 
																    uint8_t *  puBuf);
		virtual bool   					setParameters();
		virtual bool					setExParameters(int type, void* param);
	private:
		void	                        updateFaceBeauty();
		void                            updateSceneDetection();		
		void                        	updateGestureMode();
    	void                            updateLiveEffect();
		void	                        updateNightShot();
		void							updateNightVideo();
		void                            updateMirror();
		void                            updateAgeGenderDetection();
		void							updateGestureDetection();
		void							updateSingleHDRMode();
		void							updateSuperPhotoMode();
		void							updateSuperZoomMode();
		void							updateDefogShotMode();
		void							updateOrientation();
		bool							IsNightShotEnabled();
		bool							IsSingleHDREnabled();
		bool							IsDefogShotEnabled();
		void							configSceneParameter();
		void							processYuvInScenePass(uint8_t* puBuf, 
																		uint32_t u4BufSize,
																		int width,
																		int height);
		void							processYuvInNormalPass(uint8_t* puBuf, 
																		 uint32_t u4BufSize,
																		 int width,
																		 int height,
																		 int featureMask);
		void                            enableShotYuvMsg(ShotParam * shotParam,uint32_t mShotMode);		
		GNLiveEffect_t                  getEffectIndex(String8 const);	
	private:
		bool mSceneDetectionEnabled;

		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 begin
		GnPerfService* mGnPerfService;
		//Gionee <wutangzhi><2015-06-02> modify for CR01492399 end
};
#endif