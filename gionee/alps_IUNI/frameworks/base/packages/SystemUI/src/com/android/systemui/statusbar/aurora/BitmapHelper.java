package com.android.systemui.statusbar.aurora;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.util.Arrays;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Bitmap.Config;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.android.systemui.R;
import com.android.systemui.screenshot.GlobalScreenshot;

public class BitmapHelper {

	private static boolean DEBUG = false;

	private static SoftReference<Bitmap> mSoftRef = null;

	public static Bitmap getWallpaper(Context c,File file) {
		return getCurrentWallpaper(c,file,false);
	}

	public static Bitmap drawable2bitmap(Drawable dw) {
		// set up new bitmap
		Bitmap bg = Bitmap.createBitmap(dw.getIntrinsicWidth(),
				dw.getIntrinsicHeight(), Config.ARGB_8888);
		// set up new canvas
		Canvas canvas = new Canvas(bg);
		// draw bounds
		dw.draw(canvas);
		// release resource
		canvas.setBitmap(null);
		return bg;
	}

	/** A: Hazel merge code from old key guard code */
	private static Bitmap getCurrentWallpaper(Context context, File file,boolean debug) {
		try {
			if (file != null) {
				try {
					Bitmap bm = decodeSampledBitmapFromFile(file
							.getAbsolutePath());
					if (mSoftRef != null) {
						mSoftRef.clear();
						mSoftRef = null;
					}
					if(null ==bm){
						bm = getWallpaperDefaults(context);
						Log.e("linp", "--------------------------------------------getKeyguardBackgroundDrawable will read from drawable!\n");
					}
					if (bm != null) {
						mSoftRef = new SoftReference<Bitmap>(bm);
						bm = null;
						return mSoftRef.get();
					}
				} catch (OutOfMemoryError err) {
					Log.e("linp","--------------> bitmapHelper caught oom exception from systemui..");
				} catch (Exception e) {
					Log.e("linp","--------------> bitmapHelper caught exception from systemui and log is .."+ e);
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
			Log.e("linp","--------------> bitmapHelper caught exception from systemui and log1 is .."+ e);
		}
		return null;
	}
	
	public static Bitmap getWallpaperDefaults(Context context){
          return decodeSampledBitmapFromRes(context.getResources(), R.drawable.defaults);
	}
	public static Bitmap getNotificationPanelViewDefaults(Context context){
        return decodeSampledBitmapFromRes(context.getResources(), R.drawable.notification_panel_bg);
	}
	
	public static Bitmap decodeSampledBitmapFromFile(String path) {
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		return BitmapFactory.decodeFile(path, options);
	} 

	public static Bitmap decodeSampledBitmapFromRes(Resources res,int id){
		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		return BitmapFactory.decodeResource(res, id, options);
	}
	
	public static Bitmap magnifyBitmap(Bitmap bitmap, float ws, float hs) {
		Matrix matrix = new Matrix();
		matrix.postScale(ws, hs); 
		Bitmap resizeBmp = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
				bitmap.getHeight(), matrix, true);
		return resizeBmp;
	}
	
	
	/**M:Hazel add for get sceenshot bitmap*/
    public static Bitmap takeScreenshot(Context cx,boolean statusBarVisible, boolean navBarVisible)throws NullPointerException{    	
    		GlobalScreenshot gls = new GlobalScreenshot(cx,0);
    		Bitmap mScreenBitmap = gls.takeScreenshot(statusBarVisible, navBarVisible);
			if (mSoftRef != null) {
				mSoftRef.clear();
				mSoftRef = null;
			}
			if (mScreenBitmap != null) {
				mSoftRef = new SoftReference<Bitmap>(mScreenBitmap);
				mScreenBitmap = null;
				return mSoftRef.get();
			}
		return null;
    }
    
	public static Bitmap overlayBitmap(Context cx,Bitmap bm,int radius) {
		// set up new bitmap
		Bitmap bg = Bitmap.createBitmap(bm.getWidth(),
				bm.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bg);
		Paint paint = new Paint();
		
		canvas.save();
		canvas.drawBitmap(bm, 0.0f, 0.0f, paint);
		
		canvas.drawColor(radius, PorterDuff.Mode.SRC_OVER);
		canvas.restore();
		canvas.setBitmap(null);
		return bg;
	}
	
    public static byte[] Bitmap2Bytes(Bitmap bm) {
        ByteArrayOutputStream baos =new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
         
        return baos.toByteArray();
    }
    
    public static boolean compare(Bitmap bitmap1, Bitmap bitmap2) {
        ByteBuffer buffer1 = ByteBuffer.allocate(bitmap1.getHeight() * bitmap1.getRowBytes());
        bitmap1.copyPixelsToBuffer(buffer1);

        ByteBuffer buffer2 = ByteBuffer.allocate(bitmap2.getHeight() * bitmap2.getRowBytes());
        bitmap2.copyPixelsToBuffer(buffer2);

        return Arrays.equals(buffer1.array(), buffer2.array());
    }
}
