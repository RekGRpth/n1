package com.android.systemui.statusbar.policy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.os.Looper;

/**
 * 
 * @author xiejun
 *
 */
public class AuroraNetworkSpeedMonitoring extends TextView {
	private final static String TAG = "AuroraNetworkSpeedMonitoring"; 
	private HandlerThread mHandlerThread;
	private Handler mMyHandler;
	Handler mUIHandler = new Handler();
	private Runnable mCalculateSpeedRunnable = new Runnable() {
		@Override
		public void run() {
			mActuralSpeed = calculateNetSpeed();
			mMyHandler.obtainMessage().sendToTarget();
			mMyHandler.postDelayed(mCalculateSpeedRunnable, 3000);
		}
	};
	
	private Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			setVisibility(View.GONE);
			mMyHandler.removeCallbacks(mCalculateSpeedRunnable);	
		}
	};
	
	private Runnable mUpdateUI = new Runnable() {
		@Override
		public void run() {
			if(mActuralSpeed == null||"".equals(mActuralSpeed)){
				mActuralSpeed = "0.00K/s";
			}
			if(!canSetText){
				mActuralSpeed = "0.00K/s";
        		canSetText = true;
        	}
			setText(mActuralSpeed);
			if (!(isMobileNetworkConnected() || isWifiNetworkConnected())) {
				Log.v(TAG, "handler.postDelayed(mHandlerThreadShow, 2000);");
				mUIHandler.removeCallbacks(mHideRunnable);
				mUIHandler.postDelayed(mHideRunnable, 2000);

				canRemoveShow = true;
			} else {
                if (canRemoveShow) {
                	mUIHandler.removeCallbacks(mHideRunnable);
					canRemoveShow = false;
					Log.v("Baisha", "handler.removeCallbacks(mHandlerThreadShow);");
                }
			}
			
		}
	};
	
	private Context mContext;
	private boolean canSetText = true;
	private boolean canRemoveShow = false;
	private static final String ACTION_NETWORKS_SPEED = "action_isdisplay_network_speed";
    private static final String DISPLAY = "isdisplay";
    private static final String TABLE_NETWORK_DISPLAY = "isdisplay_network_speed";
    private boolean mIsCountNetSpeed = false;
    private String mActuralSpeed;
    private long mPrevNetSpeed;
	private double mNetSpeed;
	
	public AuroraNetworkSpeedMonitoring(Context context) {
		this(context,null);
	}

	public AuroraNetworkSpeedMonitoring(Context context, AttributeSet attrs) {
		this(context, attrs,0);
	}

	public AuroraNetworkSpeedMonitoring(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		mContext = context;
		mHandlerThread = new HandlerThread("Aurora network speed monitor");
		mHandlerThread.start();
		mMyHandler = new MyHandler(mHandlerThread.getLooper());
		canSetText = true;
		mIsCountNetSpeed = shouldOpenSpeedMonitor();//M:tymy bug16152
		onOrOffNetworkSpeedMonitoring();//M:tymy bug16152
	}
	
	private class MyHandler extends Handler{
		public MyHandler(Looper looper){
			super(looper);
		}
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			mUIHandler.post(mUpdateUI);
		}
	}
	
	private BroadcastReceiver mSpeedMonitorReceiver = new BroadcastReceiver(){
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if(ACTION_NETWORKS_SPEED.equals(action)){
				mIsCountNetSpeed = intent.getBooleanExtra(DISPLAY, false);
				onOrOffNetworkSpeedMonitoring();
			}else if(Intent.ACTION_SCREEN_OFF.equals(action)){
				if(getVisibility() == View.VISIBLE){
            		if(mIsCountNetSpeed){
            		    mMyHandler.removeCallbacks(mCalculateSpeedRunnable);
            		}
            	}
			}else if(Intent.ACTION_SCREEN_ON.equals(action)){
				if(getVisibility() == View.VISIBLE){
            		if(mIsCountNetSpeed && (isMobileNetworkConnected() || isWifiNetworkConnected())){
            			canSetText = false;
            		    mMyHandler.post(mCalculateSpeedRunnable);
            		}
            	}
			}else if(ConnectivityManager.CONNECTIVITY_ACTION.equals(action)){
				onOrOffNetworkSpeedMonitoring();
			}
		}
	};
	
	private boolean isMobileNetworkConnected(){
		boolean connected = false;
		ConnectivityManager manager = null;
        try {
			manager = (ConnectivityManager)mContext.getSystemService(
			        Context.CONNECTIVITY_SERVICE);
		} catch (Exception e) {
			return false;
		}
        NetworkInfo netInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);        
        State mobile = null;
        if(netInfo!=null){
        	mobile = netInfo.getState();
        }
    //tymy add State.CONNECTING for bug15174 bug15249 bug15391 begin
        //if(mobile == State.CONNECTED){
        if(mobile == State.CONNECTED||mobile == State.CONNECTING){
    //tymy add State.CONNECTING for bug15174 bug15249 bug15391 end
        	Log.d(TAG, "getMobileConnectState---true");
        	connected = true;
        }else{
        	Log.d(TAG, "getMobileConnectState---false");
        	connected = false;
        }
		return connected;
	}
	
	private boolean isWifiNetworkConnected() {
		boolean connected = false;
		ConnectivityManager manager = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = (NetworkInfo) manager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    //tymy add Connecting for bug15174 bug15249 bug15391 begin
		//connected = info.isConnected();
		connected = info.isConnectedOrConnecting();
    //tymy add Connecting for bug15174 bug15249 bug15391 end
		return connected;
	}
	
	private String getNetworkSpeed(String speed) {
		if (!speed.contains(".")) {
			if (speed.length() == 1) {
				return speed + ".00";
			} else if (speed.length() == 2) {
				return speed + ".0";
			}
			return speed;
		} else {
			String[] separationSpeed = speed.split("\\.");
			if (separationSpeed[0].length() == 1) {
				if (separationSpeed[1].length() < 2) {
					return speed + "0";
				}
				return speed;
			} else if (separationSpeed[0].length() == 2) {
				return (new BigDecimal(speed).setScale(1,
						BigDecimal.ROUND_HALF_UP)).toString();
			} else {
				return (new BigDecimal(speed).setScale(0,
						BigDecimal.ROUND_HALF_UP)).toString();
			}
		}

	}
	
	private void onOrOffNetworkSpeedMonitoring(){
    	Log.v(TAG, "onOrOffNetworkSpeedMonitoring mIsCountNetSpeed = " + mIsCountNetSpeed);
		Log.v(TAG, "onOrOffNetworkSpeedMonitoring isMobileNetworkConnected() = " + 
				isMobileNetworkConnected() + " isWifiNetworkConnected() = " + isWifiNetworkConnected());
        synchronized(this) { //aurora linchunhui add for fix BUG #17198
    	if(mIsCountNetSpeed){
    		if(true/*getMobileConnectState() || getWifiConnectState()*/){
    			if(true/*getVisibility() == View.GONE*/){
					Log.v(TAG, "onOrOffNetworkSpeedMonitoring getVisibility() == View.GONE");
    			    setVisibility(View.VISIBLE);
    			    canSetText = true;
					mMyHandler.removeCallbacks(mCalculateSpeedRunnable);
    			    mMyHandler.post(mCalculateSpeedRunnable);
    			}
    		}
    	}else{
    		if(getVisibility() == View.VISIBLE){
				Log.v(TAG, "onOrOffNetworkSpeedMonitoring getVisibility() == View.VISIBLE");
    		    setVisibility(View.GONE);
    		    mMyHandler.removeCallbacks(mCalculateSpeedRunnable);
    		}
    	}
        }
    }
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		Log.i(TAG,"Aurora network speed monitor onAttachedToWindow");
		IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NETWORKS_SPEED);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        mContext.registerReceiver(mSpeedMonitorReceiver, filter);
        mIsCountNetSpeed = shouldOpenSpeedMonitor();
        onOrOffNetworkSpeedMonitoring();
	}
	
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		Log.i(TAG,"Aurora network speed monitor onDetachedFromWindow");
		mMyHandler.removeCallbacks(mCalculateSpeedRunnable);
		mContext.unregisterReceiver(mSpeedMonitorReceiver);
	}
	
	private boolean shouldOpenSpeedMonitor(){
		try{
	    	Context settingsAppsContext = mContext.createPackageContext("com.android.settings", Context
	    			.CONTEXT_IGNORE_SECURITY);
	    	SharedPreferences sharedPreferences = settingsAppsContext
	        		.getSharedPreferences(TABLE_NETWORK_DISPLAY, Context
	        				.MODE_WORLD_READABLE);
	        return sharedPreferences.getBoolean(DISPLAY, false);
        } catch (NameNotFoundException e) {
        	return false;
        }
	}
	
	private String calculateNetSpeed() {
		if (mPrevNetSpeed == 0) {
            mPrevNetSpeed = getTotalReceivedBytes();
        } else {
            mNetSpeed = (getTotalReceivedBytes() - mPrevNetSpeed)/2.00d;
            if(mNetSpeed < 0){
            	mNetSpeed = 0.00d;
            }
            mPrevNetSpeed = getTotalReceivedBytes();
        }
    	Log.i(TAG,"calculateNetSpeed : mPrevNetSpeed = "+mPrevNetSpeed+"  ,  mNetSpeed= "+mNetSpeed);
    	DecimalFormat df = new DecimalFormat("###.##");
    	String mNetworkSpeed = null;
    	if (mNetSpeed < 1024*999) {
    		mNetworkSpeed = getNetworkSpeed(df.format(mNetSpeed/1024d)) + "K/s";
        } else {
        	mNetworkSpeed = getNetworkSpeed(df.format(mNetSpeed/(1024d*1024d))) + "M/s";
        }
    		return mNetworkSpeed;
	}
	
	private long getTotalReceivedBytes() {
        String line;
        String[] segs;
        long received = 0;
        int i;
        long tmp = 0;
        boolean isNum;
        try {
            FileReader fr = new FileReader("/proc/net/dev");
            BufferedReader in = new BufferedReader(fr, 500);
            while ((line = in.readLine()) != null) {
                line = line.trim();
                if (line.startsWith("rmnet") || line.startsWith("eth") || line.startsWith("wlan") || line.startsWith("netts") 
					|| line.startsWith("ccmni")) {
                    segs = line.split(":")[1].split(" ");
                    for (i = 0; i < segs.length; i++) {
                        isNum = true;
                        try {
                            //tmp = Integer.parseInt(segs[i]);
                            tmp = Long.parseLong(segs[i]);
                        } catch (Exception e) {
                            isNum = false;
                        }
                        if (isNum == true) {
                            received = received + tmp;
                            break;
                        }
                    }
                }
            }
        } catch (IOException e) {
            return -1;
        }
        return received;
    }

}
