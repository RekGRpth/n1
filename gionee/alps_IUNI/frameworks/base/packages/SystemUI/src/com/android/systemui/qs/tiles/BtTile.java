/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.util.Log;

import com.android.systemui.R;
import com.android.systemui.qs.GlobalSetting;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTile.ResourceIcon;

/*
 * Author:tymy
 * Date:20150911
 * Function:Quick settings tile: BtTile
 */

public class BtTile extends QSTile<QSTile.BooleanState> {

    private static final String TAG = "BtTile";
    private static final boolean DEBUG = true;    
    
    private final BluetoothAdapter mBluetoothAdapter; 
    private boolean mBtOn;
    private boolean mEnable=true;
    
    public BtTile(Host host) {
        super(host);
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();          
    }

    @Override
    protected BooleanState newTileState() {
        return new BooleanState();
    }

    @Override
    public void handleClick() {
    	mBtOn = !mBtOn;    
        if(mBtOn){
    		mBluetoothAdapter.enable();
    	}else{
    		mBluetoothAdapter.disable();
    	}
    }

    @Override
    public void handleLongClick() {
    	 Intent intent =  new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
    	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	 mHost.startSettingsActivity(intent);
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {
        state.visible = true;
        state.label = mContext.getString(R.string.quick_settings_bluetooth_label);
        state.enable = mEnable;
        if (mBtOn) {
        	//state.color=state.color=mContext.getResources().getColor(R.color.qs_title_color_select);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_enable);
        	state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_bluetooth_enable_svg);
        } else {
        	//state.color=state.color=mContext.getResources().getColor(R.color.qs_title_color_normal);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_disable);
        	state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_bluetooth_disable_svg);
        }
    }

    @Override
    protected String composeChangeAnnouncement() {
        if (mBtOn) {
            return mContext.getString(R.string.accessibility_quick_settings_bluetooth_changed_on);
        } else {
            return mContext.getString(R.string.accessibility_quick_settings_bluetooth_changed_off);
        }
    }

    public void setListening(boolean listening) {       
        final IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        mContext.registerReceiver(mReceiver, filter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(intent.getAction())) {
            	handleBtStateChanged(intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR));                          
                refreshState();
            }
        }
    };
    
    private void handleBtStateChanged(int state) {
        switch (state) {
            case BluetoothAdapter.STATE_TURNING_ON:
            	mEnable=false;
                break;
            case BluetoothAdapter.STATE_ON:
            	mBtOn=true;
            	mEnable=true;            	
                break;
            case BluetoothAdapter.STATE_TURNING_OFF:
            	mEnable=false;
                break;
            case BluetoothAdapter.STATE_OFF:
            	mBtOn=false;
            	mEnable=true;           	
                break;
            default:
            	mEnable=false;
        }
    }     
    
}
