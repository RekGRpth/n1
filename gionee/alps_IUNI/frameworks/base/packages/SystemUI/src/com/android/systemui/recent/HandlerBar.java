package com.android.systemui.recent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.os.Environment;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.android.systemui.R;
import com.android.systemui.recent.RecentsActivity.TouchOutsideListener;
import com.android.systemui.recent.RecentsPanelView.RecentsScrollView;
import com.android.systemui.statusbar.BaseStatusBar;
import com.android.systemui.statusbar.aurora.BitmapHelper;
import com.android.systemui.statusbar.phone.Blur;
import com.android.systemui.statusbar.phone.PhoneStatusBarPolicy;
import com.android.systemui.statusbar.phone.NavigationBarView;

import android.content.Intent;
import android.os.UserHandle;
import android.content.res.Resources;
import android.provider.Settings;
import android.database.ContentObserver;
import android.content.ContentResolver;
import android.os.SystemProperties;

import android.content.res.Configuration;
import android.graphics.BitmapFactory;

public class HandlerBar extends LinearLayout {

	Context						mContext;
	WindowManager				mWM;		
	WindowManager.LayoutParams	mHandlerBarMParams;	
	WindowManager.LayoutParams  mQuickRecentPanelParams;
	View						mHandlerBarView;
	private int mTouchSlop;
	int handlerHeight = 0;
	private int phoneHeight;
	private int phoneWidth;
	boolean isHandlerBarViewVisiabe = true; 
	boolean isHandlerBarMoveBottom = false;
	public RecentsPanelView mRecentsPanelView;
	int tag = 0;
	int oldOffsetX;
	int oldOffsetY;
    private float mDownMotionX;
    protected float mLastMotionX;
    protected float mLastMotionXRemainder;
    protected float mLastMotionY;
    protected float mTotalMotionX;
    private float mDownMotionZ;
    private float mDownTime;
    private static int sHeight = 0;
    private static final int QUIT_THRESHOLD = -10; // deltaZ
    private static final float CLEAR_LOCK_OFFSET_Y = 190f;
	private VelocityTracker mVelocityTracker;
	private static final int SNAP_VELOCITY = 200;
	protected static int mRecentsScrimHeight = 0;
	protected static int mQuickSettingHeight = 0;

	TelephonyManager telManager;
    private boolean mLandscape = false;

	DisplayMetrics mDisplayMetrics = new DisplayMetrics();
	Display mDisplay;
	private Bitmap mPanelBg = null;
	private int totalMoveDistant = 0;
	private boolean flg = false;
	private boolean isAutoUp = false;
	private int mLastZ;
	private View mRecentsLayout;
	AuroraPagedView mAuroraPagedView;
    private Handler mHandler;

    public static final int ANIMATION_CONTINUE_ENTER_DURATION = 450; // 600ms
	
    public static final int AUTO_ENTER_ANIMATION_DURATION = 500;

    private static Boolean mEnable = true;

    public static final String DISABLE_HANDLER = "com.android.systemui.recent.AURORA_DISABLE_HANDLER";
    public static final String ENABLE_HANDLER = "com.android.systemui.recent.AURORA_ENABLE_HANDLER";
    public static final String AURORA_BIND_INVOKER_SERVICE = "com.android.systemui.recent.AURORA_BIND_INVOKER_SERVICE";

    public static final String AURORA_BIND_INVOKER_VIEW = "com.android.systemui.recent.AURORA_BIND_INVOKER_VIEW";

    public boolean mTouching = false;

    public boolean mHasNaviBar;

	public HandlerBar(Context context) {
		super(context);
		mContext = context;
		mHandlerBarMParams = new WindowManager.LayoutParams();
        mHandlerBarMParams.setTitle("HandlerBar");
		mQuickRecentPanelParams = new WindowManager.LayoutParams();
    	mQuickRecentPanelParams.setTitle("Recents");
		final ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = configuration.getScaledTouchSlop();//
        Resources r=context.getResources();
        mRecentsScrimHeight = (int)(r.getDimension(R.dimen.recents_view_width_or_height));
    	mQuickSettingHeight = (int)(r.getDimension(R.dimen.quick_setting_view_height));
    	mDisplay = ((WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE))
                .getDefaultDisplay();
        mLandscape = isOrientationLand();
	}
	
	public interface HandlerBarCallback{
    	public void removeRecentPanelView();
    	public void removeRecentPanelView(boolean isShowHandlerBar);
    	public void setRecentsPanelViewBackgroundAlpha(int alpha);
    	public void showRecentwsPanelView();
    	public void showHandlerBarView(boolean isShow);

    	public RecentsPanelView getRecentsPanelView();
    	public void removeBar();
    	public void addBar();
    	public void updateOrientation(int newOri);
    	
    	//iht 2015-03-23 更新图标问题
    	public void refreshViewIcons();
    }
    
    private HandlerBarCallback hcb = new HandlerBarCallback(){
    	@Override
    	public void refreshViewIcons(){
    		Log.v("iht-ssui","++++++++++++++++++++++++++++++++++++++++++++++-----------U3--------update");
    		mRecentsPanelView.refreshViewIcons();
    	}
    	
		@Override
		public void removeRecentPanelView() {
			removeRecentPanelViewAchieve();
		}
		
		@Override
		public void removeRecentPanelView(boolean isShowHandlerBar) {
			removeRecentPanelViewAchieve(isShowHandlerBar);
		}

		@Override
		public void setRecentsPanelViewBackgroundAlpha(int alpha) {
			// TODO Auto-generated method stub
			setBackgroundAlpha(alpha);
		}
		@Override
		public void showRecentwsPanelView() {
			// TODO Auto-generated method stub
			showRecentsPanelViewAchieve();
		}
		@Override
		public void showHandlerBarView(boolean isShow) {
			// TODO Auto-generated method stub
			showView(isShow);
		}
		
		public RecentsPanelView getRecentsPanelView(){
			return mRecentsPanelView;
		}
		
		public void removeBar(){
			removeHandlerBar();
		}
		
    	public void addBar(){
    		addHandlerBar();
    	}
    	
    	public void updateOrientation(int newOri) {
            mLandscape = (newOri == Configuration.ORIENTATION_LANDSCAPE);
            Log.d("felix","HandlerBar.DEBUG updateOrientation() mLandscape = " + mLandscape);
        }
    };
		
	public void fun() {
		mWM = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		phoneWidth = mWM.getDefaultDisplay().getWidth();
		phoneHeight = mWM.getDefaultDisplay().getHeight();
		mHandlerBarView = LayoutInflater.from(mContext).inflate(R.layout.handlerbar, null);
		mRecentsPanelView = (RecentsPanelView)LayoutInflater.from(mContext).inflate(R.layout.quick_recent_panel, null);
		
		mRecentsPanelView.init(hcb,phoneWidth, phoneHeight);
		setBackgroundAlpha(0);
		mRecentsPanelView.setRecentsMaskViewAlpha();
		mRecentsPanelView.setRecentsQuickSettingViewAlpha();
		mRecentsLayout = mRecentsPanelView.getMoveView();

		mAuroraPagedView = (AuroraPagedView)mRecentsLayout.findViewById(R.id.recents_container);
        mHandler = new Handler();

		final RecentTasksLoader recentTasksLoader = RecentTasksLoader.getInstance(mContext);
		recentTasksLoader.preloadFirstTask();
		// Aurora <zhanggp> <2013-10-18> added for quicksetting end
	    recentTasksLoader.setRecentsPanel(mRecentsPanelView, mRecentsPanelView);
		
		mHandlerBarView.setOnTouchListener(new OnTouchListener() {
            /*
             * New MOTION_UP logic when finger moving slow, A.K.A. flg = false
             * if 1. panel && finger is up to less than 1/3
             * or 2. finger moving down
             * then close panels
            */
		    int lastMoveDeltaZ = 0;
            private boolean shouldClosePanel(int z, int deltaZ) {
				return ((mLandscape ? mRecentsLayout.getScrollX() : mRecentsLayout.getScrollY()) < mRecentsScrimHeight / 3)
                            && (-z < mRecentsScrimHeight / 3)
                            || (deltaZ < QUIT_THRESHOLD || lastMoveDeltaZ < QUIT_THRESHOLD); 
            }

			public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_OUTSIDE == event.getAction())
                    return false;
                if (mEnable == false) {
                    return false;
                }
                if (mRecentsPanelView.mAnimating == true)
                    return false;

				if (mVelocityTracker == null) {
		            mVelocityTracker = VelocityTracker.obtain();
		        }
				
	             
				final int action = event.getAction();
				int z = (int)(mLandscape ? event.getX() : event.getY());

                mTouching = true;

				float distantZ = (mLandscape ? event.getRawX() : event.getRawY());
				int deltaZ = 0;
				int velocityZ = 0;
				if (action == MotionEvent.ACTION_DOWN) {
                    mDownTime = event.getDownTime();
					mDownMotionZ = (mLandscape ? event.getX() : event.getY());
					isHandlerBarViewVisiabe = true;
					isHandlerBarMoveBottom = false;
					switchingHandlerPanel();
					((RecentsLayout)mRecentsLayout).closeScroll();
					mLastZ = 0;
				}else if (action == MotionEvent.ACTION_MOVE) {
                    if (mDownTime != event.getDownTime()) {
                        // If down time dosen`t match, this event comes with no down event.
                        // Better not handle it
                        return false;
                    }
					float dz = Math.abs(distantZ);
                    // Only update coordinates until pointer went out of navi bar.
                    if (z <= 0){
					    isHandlerBarViewVisiabe = false;
					    mVelocityTracker.addMovement(event);
					    final VelocityTracker velocityTracker = mVelocityTracker;
		                velocityTracker.computeCurrentVelocity(50);
		                velocityZ = (int) (mLandscape
                            ? velocityTracker.getXVelocity()
                            : velocityTracker.getYVelocity());
		                if(-velocityZ > 50){
		                	isAutoUp = true;
		                }
	            	    final float directionZ = z - mDownMotionZ;
		                lastMoveDeltaZ = deltaZ = mLastZ - z;
		                totalMoveDistant += deltaZ;
	            	    if((int)dz != 0){
					    	isHandlerBarMoveBottom = true;
					    } 
	            	     if(!mRecentsPanelView.isShown() && totalMoveDistant > 10){

	            	         showRecentsPanelViewAchieve();
	            	         
	            	         setPanelBg();
					     }
	            	     
	            	     if (-velocityZ < SNAP_VELOCITY) {
	     			    	flg = false;
	     			    	if (totalMoveDistant >= mRecentsScrimHeight) {
	     			    		if(deltaZ > mRecentsScrimHeight/2){
	     			    			mRecentsPanelView.autoQuickSettingEnterAnimationForZ((mLandscape)
                                        ? mRecentsLayout.getScrollX()
                                        : mRecentsLayout.getScrollY());
                                    if (mLandscape)
                                        ((RecentsLayout)mRecentsLayout).startScroll(mRecentsLayout.getScrollX(), 0,
                                            mRecentsScrimHeight - mRecentsLayout.getScrollX(), 0, 500);
                                    else
                                        ((RecentsLayout)mRecentsLayout).startScroll(0, mRecentsLayout.getScrollY(),
                                            0, mRecentsScrimHeight - mRecentsLayout.getScrollY(), 500);
	     			    			pauseAuroraPagedViewScroll();
	     			    		}else{
                                    ((RecentsLayout)mRecentsLayout).scroll((mLandscape
                                            ? mRecentsLayout.getScrollX()
                                            : mRecentsLayout.getScrollY()),
                                            -mRecentsScrimHeight, 200);

	     			    		}
	     			    	} else {
	     			    		
	     			    		if((mLandscape ? mRecentsLayout.getScrollX() : mRecentsLayout.getScrollY()) + deltaZ 
                                        > mRecentsScrimHeight){
	     			    			
	     			    		}else{
	     			    			if(deltaZ > mRecentsScrimHeight/2){
	     			    				
                                        if (mLandscape)
                                            ((RecentsLayout)mRecentsLayout).startScroll(mRecentsLayout.getScrollX(), 0, deltaZ, 0, 500);
                                        else
                                            ((RecentsLayout)mRecentsLayout).startScroll(0, mRecentsLayout.getScrollY(), 0, deltaZ, 500);
										pauseAuroraPagedViewScroll();
	     			    			}else{
	     			    				
	     							    ((RecentsLayout)mRecentsLayout).scroll((mLandscape?mRecentsLayout.getScrollX():mRecentsLayout.getScrollY()), z, 200);

	     			    			}
	     			    		}
	     			    		
	     			    	}
	     			     }else{
	     			    	
	     			    	flg = true;
	     			     }
	            	     notifyQuickRecentPanelZMove((mLandscape
                                ? mRecentsLayout.getScrollX()
                                : mRecentsLayout.getScrollY()));
                     }
	     			 mLastZ = z;
				}else if (action ==  MotionEvent.ACTION_UP

                    || action == MotionEvent.ACTION_CANCEL){
                    if (mDownTime != event.getDownTime()) {
                        // If down time dosen`t match, this event comes with no down event.
                        // Better not handle it
                        return false;
                    }
                    mTouching = false;
		            deltaZ = mLastZ - z;
                    // skip if RecentsPanelView not showed up
                    if (!isHandlerBarViewVisiabe) {
					if(!flg){
						flg = false;
                        if (!shouldClosePanel(z, deltaZ)) { 
							isAutoUp = false;
							setBackgroundAlpha(255);
							mRecentsPanelView.continueAutoQuickSettingEnterAnimation();
							// Aurora <Steve.Tang> support orientation land recent panel. start
							if(!mLandscape){
								((RecentsLayout)mRecentsLayout).startScroll(0, mRecentsLayout.getScrollY(),
                                        0, mRecentsScrimHeight - mRecentsLayout.getScrollY(),
                                        ANIMATION_CONTINUE_ENTER_DURATION);

							} else {
								((RecentsLayout)mRecentsLayout).startScroll(mRecentsLayout.getScrollX(), 0,
                                        mRecentsScrimHeight - mRecentsLayout.getScrollX(),0,
                                        ANIMATION_CONTINUE_ENTER_DURATION);
							}
							pauseAuroraPagedViewScroll();
						} else {
							if(mLandscape)
                                ((RecentsLayout)mRecentsLayout).startScroll(mRecentsLayout.getScrollX(), 0,
                                        -mRecentsLayout.getScrollX(), 0, 500);
                            else
                                ((RecentsLayout)mRecentsLayout).startScroll(0, mRecentsLayout.getScrollY(),
                                        0, -mRecentsLayout.getScrollY(), 500);
							pauseAuroraPagedViewScroll();
							mRecentsPanelView.removeRecentsPanelView();
						}
						
					}else{
						setBackgroundAlpha(255);
						mRecentsPanelView.continueAutoQuickSettingEnterAnimation();
                        if(mLandscape)
                            ((RecentsLayout)mRecentsLayout).startScroll(0, 0,
                                    mRecentsScrimHeight, 0,
                                    ANIMATION_CONTINUE_ENTER_DURATION);
                        else
                            ((RecentsLayout)mRecentsLayout).startScroll(0, 0,
                                    0, mRecentsScrimHeight,
                                    ANIMATION_CONTINUE_ENTER_DURATION);
						pauseAuroraPagedViewScroll();
					}
                    }
					isHandlerBarViewVisiabe = true;
					totalMoveDistant = 0;
									
				    lastMoveDeltaZ = 0;
				}
				
				velocityZ = 0;
				releaseVelocityTracker();
				return true;
			}
		});
    	
    	
    	WindowManager wm = mWM;
		mHandlerBarMParams.type = 2003; 
		mHandlerBarMParams.flags = 40;
		mHandlerBarMParams.gravity = Gravity.BOTTOM; //

		mHandlerBarMParams.width = LayoutParams.MATCH_PARENT;
		mHandlerBarMParams.height = sHeight;//45;//0;
		mHandlerBarMParams.format = -3; 

		wm.addView(mHandlerBarView, mHandlerBarMParams);
        mRecentsPanelView.setVisibility(View.GONE);

    	mQuickRecentPanelParams.type = WindowManager.LayoutParams.TYPE_STATUS_BAR_PANEL;
    	mQuickRecentPanelParams.flags |= WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
    	mQuickRecentPanelParams.width = LayoutParams.MATCH_PARENT;
    	mQuickRecentPanelParams.height = LayoutParams.MATCH_PARENT;
    	mQuickRecentPanelParams.format = -3;
        wm.addView(mRecentsPanelView, mQuickRecentPanelParams);

		handlerHeight = mHandlerBarView.getHeight();
		if(telManager == null){
			telManager = (TelephonyManager)mContext.getSystemService(Context.TELEPHONY_SERVICE);
			telManager.listen(new HandlerBarPhoneStateListener(), PhoneStateListener.LISTEN_CALL_STATE);
		}

        mHasNaviBar = false;

        IntentFilter filter = new IntentFilter();
        filter.addAction(DISABLE_HANDLER);
        filter.addAction(ENABLE_HANDLER);
        filter.addAction("felix.duan.setHeight");
        filter.addAction(AURORA_BIND_INVOKER_VIEW);
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (DISABLE_HANDLER.equals(intent.getAction())) {
                    if (!mTouching) {
                    	Log.e("666666", "----DISABLE_HANDLER broadcast---");
                        mEnable = false;
                        showView(false);
                        if (!mHasNaviBar)
                        	updatePanelStatus(mEnable);
                    }
                } else if (ENABLE_HANDLER.equals(intent.getAction())){
                	Log.e("666666", "----ENABLE_HANDLER broadcast---");
                    mEnable = true;
                    showView(true);
         //M:liuzuo fix bug 21356 begin             
                    mTouching=false;
         //M:liuzuo fix bug 21356 end                  
                    if (!mHasNaviBar){
                    	updatePanelStatus(mEnable);
                    	}
                } else if ("felix.duan.setHeight".equals(intent.getAction())){
                    int height = intent.getIntExtra("height",45);
                    Log.d("felix", "onReceive() height = " + height);
                    sHeight = height;
                } else if(intent.getAction().equals(AURORA_BIND_INVOKER_VIEW)) {
                    bindServiceView();
                }
            }
        }, filter);
        bindServiceView();

        mContext.sendBroadcast(new Intent(HandlerBar.AURORA_BIND_INVOKER_SERVICE));
	}

    private static InvokerService sInvokerService = null;
    // TODO choose proper time to bind
    private void bindServiceView(){
        if (mHasNaviBar) return; 
        if (sInvokerService == null)
            sInvokerService = InvokerService.getInstance();
        if (sInvokerService != null) {
            sInvokerService.setView(mHandlerBarView);
        } else {
            Log.d("felix", "bindServiceView() sInvokerService = null");
        }
    }

	 /**
     * Pause @AuroraPagedView scroll on X axis.
     *
     * @see @AuroraPagedView#setScrollable
     *
     * @author Felix.Duan.
     * @date 2014-4-10
     * Modified @date 2014-4-28
     */
    private void pauseAuroraPagedViewScroll() {
        mAuroraPagedView.setScrollable(false);
    }
	
    public void show(){
    	WindowManager wm = mWM;
    	mQuickRecentPanelParams.type = WindowManager.LayoutParams.TYPE_STATUS_BAR_PANEL;
    	mQuickRecentPanelParams.flags |= WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED;
    	mQuickRecentPanelParams.width = LayoutParams.MATCH_PARENT;
    	mQuickRecentPanelParams.height = LayoutParams.MATCH_PARENT;
    	mQuickRecentPanelParams.format = -3;

        mRecentsPanelView.setVisibility(View.VISIBLE);
    }
    
    public void notifyQuickRecentPanelZMove(float scrollZ){
    	mRecentsPanelView.quickRecentPanelZMove((int)scrollZ);
    }
    
    public void hideBar()
    {
    	//Log.e("666666", "---hideBar()-------");
    	mHandlerBarView.setVisibility(View.GONE);
    }
    
    public void resumeBar()
    {
    	//Log.e("666666", "---resumeBar()-------");
    	mHandlerBarView.setVisibility(View.VISIBLE);
    }
    
    public void removeHandlerBar(){
    	try{
    	((WindowManager)mContext.getSystemService("window")).removeViewImmediate(mHandlerBarView);
    	}catch (Exception localException){
    		
    	}
    }
    
    public void addHandlerBar(){
    	try{
    		WindowManager wm = mWM;
    		mHandlerBarMParams.type = 2003; 
    		mHandlerBarMParams.flags = 40;
    		mHandlerBarMParams.gravity = Gravity.BOTTOM; //

    		mHandlerBarMParams.width = LayoutParams.MATCH_PARENT;
    		mHandlerBarMParams.height = sHeight;//45;//0;
    		mHandlerBarMParams.format = -3; 

    		wm.addView(mHandlerBarView, mHandlerBarMParams);
        	}catch (Exception localException){
        		
        	}
    }
    
    public void switchingHandlerPanel(){
    	  //用一个定时器定时
    	if(!isBarHide()){
    		showView(false);
    	}
    	new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if(isBarHide()) {
                	if(isBarHide()){
                		showView(true);//hide
                	}
                }
				
			}
		}, 3000);
    }
    
    //控制悬浮按钮的显示或消失
    public void showView(Boolean boo) {
        if(boo && mEnable) {
        	resumeBar();
        }else {
        	hideBar();
        }
    }
    
    public boolean isBarHide(){
    	if(mHandlerBarView.getVisibility() == View.GONE){
    		return true;
    	}else{
    		return false;
    	}
    	
    }
    
    public void callRecentsPanelShow(){

    	//setNavigationBarViewLowProfile(true);

    	final RecentTasksLoader recentTasksLoader = RecentTasksLoader.getInstance(mContext);
        boolean waitingForWindowAnimation = false;
        mRecentsPanelView.show(true, recentTasksLoader.getLoadedTasks(), recentTasksLoader.isFirstScreenful(), waitingForWindowAnimation);
    }
    
    
    public void setBackgroundAlpha(int alpha){//0-229
    	mRecentsPanelView.getBackground().setAlpha(Math.min(alpha, 255));
    }
    
    private void removeRecentPanelViewAchieve(){

        mEnable = true;
    	if(mRecentsPanelView != null){
			try{
				if(mRecentsPanelView.isShown()){
					setBackgroundAlpha(0);
                    mRecentsPanelView.setVisibility(View.GONE);
					mAuroraPagedView.snapToPage(1);
					removeHandlerBar();
					addHandlerBar();
					Intent intent= new Intent(HandlerBar.ENABLE_HANDLER);
					mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));
					showView(true);
				}
			}catch (Exception localException)
		      {
		        localException.printStackTrace();
		      }
		}
    }
    
    
    private void removeRecentPanelViewAchieve(boolean isShowHandlerBar){

        mEnable = true;
    	if(mRecentsPanelView != null){
			try{
				if(mRecentsPanelView.isShown()){
					setBackgroundAlpha(0);
                    mRecentsPanelView.setVisibility(View.GONE);
					mAuroraPagedView.snapToPage(1);
					removeHandlerBar();
					addHandlerBar();

					Intent intent= new Intent(HandlerBar.ENABLE_HANDLER);
					mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));

					if(!isShowHandlerBar){
						showView(false);
					}

				}
			}catch (Exception localException)
		      {
		        localException.printStackTrace();
		      }
		}
    }
    
    public void showRecentsPanelViewAchieve(){
    	
 	      callRecentsPanelShow();
 	      show();
    }

    public boolean isRecentsPanelViewShow(){
    	return mRecentsPanelView.isShown();
    }
    
	private boolean isOrientationLand(){
		int orientation = mContext.getResources().getConfiguration().orientation;
	    if (orientation == Configuration.ORIENTATION_PORTRAIT) return false;
		return true;
	}

    public void showRecentsPanelViewForHomeKey(){
    	Log.e("666666", "----Handlerbar showRecentsPanelViewForHomeKey mEnable = ---" + mEnable);
    	if (!mEnable) return;
    	setBackgroundAlpha(255);
    	callRecentsPanelShow();
    	setPanelBg();
    	show();

    	if(!mLandscape){
    		((RecentsLayout)mRecentsLayout).startScroll(0, 0, 0, HandlerBar.mRecentsScrimHeight, AUTO_ENTER_ANIMATION_DURATION);
    	} else {
    		((RecentsLayout)mRecentsLayout).startScroll(0, 0, HandlerBar.mRecentsScrimHeight, 0, AUTO_ENTER_ANIMATION_DURATION);
    	}

    	mRecentsPanelView.autoQuickSettingEnterAnimation();
    	pauseAuroraPagedViewScroll();

    	RecentsPanelView.isResponseHomeKey = true;
    }
    
    private class HandlerBarPhoneStateListener extends PhoneStateListener{
    	public void onCallStateChanged(int state, String incomingNumber) {
    		 switch (state) {
    		 case TelephonyManager.CALL_STATE_IDLE:
    			 BaseStatusBar.isPhoneRinging = false;
    			 if(isBarHide()){
    			     showView(true);
    			 }
    			 break;
    		 case TelephonyManager.CALL_STATE_OFFHOOK: 
    			 BaseStatusBar.isPhoneRinging = false;
    			 if(mRecentsPanelView.isShown()){
    				 mRecentsPanelView.removeRecentsPanelView(false);
    			 }else{
    			     if(!isBarHide()){
    				     showView(false);
    			     }
    			 }
    			 break;
    		 case TelephonyManager.CALL_STATE_RINGING:
    			 BaseStatusBar.isPhoneRinging = true;
    			 if(mRecentsPanelView.isShown()){
    				 mRecentsPanelView.removeRecentsPanelView(false);
    			 }else{
    			     if(!isBarHide()){
    				     showView(false);
    			     }
    			 }
    			 break;
    		 }
    		 super.onCallStateChanged(state, incomingNumber);
    	}
    }
    
	public void setPanelBg(){
		try {
//			mRecentsPanelView.setBackgroundDrawable(null);
//    		mPanelBg = BitmapHelper.takeScreenshot(getContext(), false, false);
//    		mPanelBg = (Blur.fastblur(getContext(), BitmapHelper.magnifyBitmap(mPanelBg, 0.1f, 0.1f),10));
//    		//mPanelBg = BitmapHelper.overlayBitmap(getContext(),mPanelBg,0xe5000000);
//    		mRecentsPanelView.setBackgroundDrawable(new BitmapDrawable( mPanelBg) );
			
    		mRecentsPanelView.setBackgroundColor(Color.argb(252, 1, 9, 14));
    	} catch (NullPointerException e) {
    		// TODO: handle exception
//    		mPanelBg = BitmapFactory.decodeResource(getContext().getResources(),R.drawable.status_bar_bg_tile);
//    		mRecentsPanelView.setBackgroundDrawable(new BitmapDrawable(mPanelBg));
    		mRecentsPanelView.setBackgroundColor(Color.argb(252, 1, 9, 14));
    		Log.e("666666", "schedulePeek catch NullPointerException when take a screenshot");
    	}
	}
    
	private void releaseVelocityTracker() {  
	    if(null != mVelocityTracker){
		    
    	    mVelocityTracker.clear(); 
    	    mVelocityTracker.recycle();
    	    mVelocityTracker = null;  
        }
	 }

    // For navigationBar set as it`s delegate view
    public View getHandlerBarView() {
        if (mHandlerBarView == null)
            return null;
        else
            return mHandlerBarView;
    }
    
    public RecentsPanelView getRecentPanelView( ) {
    	return mRecentsPanelView;
    }
    //M: liuzuo fix nullpointException begin 
    private void updatePanelStatus(boolean enable){
    	InvokerService instance = InvokerService.getInstance();
    	if (instance!=null) {
    		instance.updatePanelStatus(enable);
		}
    }
    //M: liuzuo fix nullpointException end
}
