package com.android.systemui.statusbar.aurora;

import java.io.File;
import java.lang.ref.SoftReference;

import com.android.systemui.statusbar.phone.Blur;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.util.Log;
import android.view.View;

public class KeyguradBackgroundManager {

	public static final int NORMAL = 0;

	public static final int BLUR = 1;

	private static final int DEFAULT_TRANSITION_COUNT = 2;
	
	private static final int DEFAULT_BLUR_RADIUS = 15;
	
	private static final String DEFAULT_WALLPAPER_DIR = "/data/aurora/change/lockscreen/wallpaper.png";
	
	private static SoftReference<Bitmap> mSoftRef = null;
	
	private int state = NORMAL;
    
	private TransitionDrawable td;
	
	private Context cx;
	
	public static final int DEFAULT_TRANSITION_DURATION = 200;
	
	public static KeyguradBackgroundManager kbm;
	
	public Bitmap mCurrentBitmap;
	
	// for bounce layout background
	public Drawable mBounceDrawable;
	
	public KeyguradBackgroundManager(Context c) {
		this.cx = c;
	}
	
	public static KeyguradBackgroundManager getKeyguardManager(Context c) {
		if (kbm == null)
			kbm = new KeyguradBackgroundManager(c);
		return kbm;
	}
	
	public void setKeyguardBackgroundDrawable(View v,Bitmap b){
		try {
			Drawable[] layers = new Drawable[DEFAULT_TRANSITION_COUNT]; 			
			layers[0] = new BitmapDrawable(b); //normal image
			layers[1] = new BitmapDrawable(BitmapHelper.overlayBitmap(cx, getKeyguardBackgroundDrawable(true),0x80000000)); //blur image
			setBounceBackgroundDrawable(layers[1]);
			td = new TransitionDrawable(layers);
//			setTransitionDrawable(v,td);
			setCurrentBackgroundDrawableState(NORMAL);
		} catch (NullPointerException e) {
			// TODO: handle exception
			Log.e("linp", "------------------------------->KeyguradBackgroundManager setKeyguardBackgroundDrawable catch NullPointer Exception and log is.."+e);
		}
	}
	
	/**getting img from given url in order to checking whether it's content is the same or not*/
	public Bitmap getRenderKeyguardBitmap(boolean blur){
		try {
			setCurrentKeyguardBitmap(getKeyguardBackgroundDrawable(blur));
			return getCurrentKeyguardBitmap();
		} catch (NullPointerException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public void setCurrentKeyguardBitmap(Bitmap b){
		mCurrentBitmap  = b;
	}
	
	public Bitmap getCurrentKeyguardBitmap(){
		return this.mCurrentBitmap;
	}
	
	public void setBounceBackgroundDrawable(View v){
		v.setBackground(null);
		v.setBackground(getBounceBackgroundDrawable());
	}
	
	public void setCurrentBackgroundDrawableState(int s) {
		this.state = s;
	}

	public int getCurrentBackgroundDrawableState() {
		return state;
	}
	
	public void startTransition(int durationMillis){
		td.startTransition(durationMillis);
	}
	
	public void reverseTransition(int duration){
		td.reverseTransition(duration);
	}
	
	public void resetTransition(){
		td.resetTransition();
	}
	
	public void restoreTransition(){
		Log.e("lockscreen", "td.getid(0)="+td.getId(0)+";"+"td.getid(1)="+td.getId(1)+";"+"td.getNumberOfLayers()="+td.getNumberOfLayers());
	}
	
	public void setDrawableByLayerId(int id ,Drawable drawable){
		td.setDrawableByLayerId(id, drawable);
	}
	
	private void setBounceBackgroundDrawable(Drawable d){
		this.mBounceDrawable  = d;
	}
	
	public Drawable getBounceBackgroundDrawable(){
		return this.mBounceDrawable;
	}
	
	public TransitionDrawable getTransitionDrawable(){
		return this.td;
	}
	
	public void setTransitionDrawable(View v,TransitionDrawable td){
		v.setBackground(null);
		v.setBackground(td);
	}
	
	public boolean  isTransitionDrawable(View v){
		Drawable d = v.getBackground();
		if(d!=null){
			if(d == td ||d == td.getDrawable(0) || d == td.getDrawable(1))
				return true;
		}
		return false;
	}
	
	public Bitmap getKeyguardBackgroundDrawable(boolean needblur)throws NullPointerException{
		Bitmap bitmap = BitmapHelper.getWallpaper(this.cx, new File(DEFAULT_WALLPAPER_DIR));
		if (mSoftRef != null) {
			mSoftRef.clear();
			mSoftRef = null;
		}
		if (null != bitmap) {
			if (needblur){
				bitmap = Blur.fastblur(cx, BitmapHelper.magnifyBitmap(bitmap, 0.1f, 0.1f), DEFAULT_BLUR_RADIUS);
			}
			mSoftRef = new SoftReference<Bitmap>(bitmap);
			bitmap = null;
			return mSoftRef.get();
		}
		return null;
	}
	
	public Bitmap getWallpaperDefaults(){
		Bitmap bitmap = BitmapHelper.getWallpaperDefaults(cx);
		if (mSoftRef != null) {
			mSoftRef.clear();
			mSoftRef = null;
		}
		mSoftRef = new SoftReference<Bitmap>(bitmap);
		bitmap = null;
		return mSoftRef.get();
	}
}
