//Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
package com.android.server.am;

import android.content.Context;
import android.content.Intent;
import android.os.Process;
import android.os.SystemProperties;
import android.util.Log;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.UserHandle;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.RemoteException;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ProviderInfo;

import android.app.AppGlobals;
import android.view.View;
import android.view.WindowManager;
import android.content.pm.PermissionInfo;
import android.app.KeyguardManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ContentValues;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.text.TextUtils;
import android.os.AsyncTask;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.FrameLayout;
import android.view.View.OnClickListener;
import com.android.server.pm.AmigoPermission;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import android.util.Slog;
import android.util.AmigoCheckPermission;
import com.android.server.ServiceThread;
import com.android.server.Watchdog;
import android.app.IAmigoActivityManager;
import android.app.AlertDialog;
import android.widget.Toast;	
/**
 *  {@hide} 
 */
public class AmigoActivityManagerService extends IAmigoActivityManager.Stub{

    private static final boolean DEBUG_PERM = true;
       
    private static final int GN_SHOW_PERM_ERROR = 200;
    private static final int GN_PERM_COUNT_DOWN = 201;
    private static final int GN_PERM_UPDATEDB = 202;
    private static final Uri GN_PERM_URI = Uri.parse("content://com.amigo.settings.PermissionProvider/permissions");
    public static final String GN_PERM_TAG = "perm_ctrl";
    private static final int GN_MAX_WAIT_TIME = 10;
	private static final long WATCHDOG_TIMEOUT = 1000*60*12;
    
    private Map<String, Integer> mPermCtrlResultMap = new HashMap<String, Integer>();
    private AmigoPermDialogEntry mPermDialogEntry;
    private List<Message> mPermMsgList = new ArrayList<Message>();
    private boolean mIsPermDialogShowing = false;
    private Handler mPermCtrlHandler;
    private KeyguardManager mKeyguardManager;
    //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 begin
    //private AmigoPermissionAsyncTask mPerAsyncTask = null;
    //private int mExecuteCount = 0 ;
    //private static final int MAX_THREAD = 120;
    //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 end
    static final int PEMISSION_CONTROL_DENIDE = -1 ;
    static final int PEMISSION_CONTROL_ASK = 0 ;
    static final int PEMISSION_CONTROL_GRANTED = 1 ;  
    static final int PEMISSION_CONTROL_SLEEP_TIME = 250 ; 
    static final int PEMISSION_CONTROL_SLEEP_COUNTS = 41 ;    
    private boolean mRememberMe = true ; 
    private int mPermissionsStatus = 0 ;
    private boolean mStartSetting = false ;
    private Context mContext;
    private ActivityManagerService mService = null;
	final ServiceThread mHandlerThread;
    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
	private List<String> mPackageSetting = new ArrayList<String>();
    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end  
	
    public AmigoActivityManagerService(Context context, ActivityManagerService service){
		if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "New AmigoActivityManagerService");
		
		mHandlerThread = new ServiceThread(GN_PERM_TAG, Process.THREAD_PRIORITY_FOREGROUND, true);
		mHandlerThread.start();
        mPermCtrlHandler = new AmigoPermCtrlHandler(mHandlerThread.getLooper());

        mContext = context;
        mService = service;
		AmigoPermission.getInstance().initPermissionProp();
		//Watchdog.getInstance().addThread(mPermCtrlHandler, WATCHDOG_TIMEOUT);
    }
	 

    void amigoReadSystemAppInfo() {
        AmigoPermission.getInstance().readSystemAppInfo();	
    }

    
    void amigoRegistCloseSysDlg() {
        IntentFilter closeSysDlg = new IntentFilter();
        closeSysDlg.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
               amigoCleanAllPermMessages();
            }
        }, closeSysDlg);
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 begin
    public int amigoCheckPermmison(String permissionName, int callingUid){
       return amigoCheckComponentPermission(permissionName, callingUid, -1, true, mService.mBooted);
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-20> add for CR01456418 end       
    int amigoCheckComponentPermission(String permission, int uid,
            int owningUid, boolean exported, boolean booted) {
        // Root, system server get to do everything.
        if (uid == 0 || uid == Process.SYSTEM_UID) {
            return PackageManager.PERMISSION_GRANTED;
        }
        // Isolated processes don't get any permissions.
        if (UserHandle.isIsolated(uid)) {
            return PackageManager.PERMISSION_DENIED;
        }
        // If there is a uid that owns whatever is being accessed, it has
        // blanket access to it regardless of the permissions it requires.
        if (owningUid >= 0 && UserHandle.isSameApp(uid, owningUid)) {
            return PackageManager.PERMISSION_GRANTED;
        }
        // If the target is not exported, then nobody else can get to it.
        if (!exported) {
            if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "Permission denied: checkComponentPermission() owningUid=" + owningUid);
            return PackageManager.PERMISSION_DENIED;
        }
        if (permission == null) {
            return PackageManager.PERMISSION_GRANTED;
        }
        permission = amigoTransPermission(permission);

        if (!AmigoCheckPermission.mCheckPermissions.contains(permission)
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> add for CR01452396 begin
                || AmigoCheckPermission.mSepcialPackage.contains(amigoGetNameForUid(uid))
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-03-10> add for CR01452396 end
                || android.Manifest.permission.CHANGE_WIFI_STATE.equals(permission)) {		
        try {          
                //if (DEBUG_PERM) Slog.w(GN_PERM_TAG, "Check perm in pms " + permission);
                return AppGlobals.getPackageManager().checkUidPermission(amigoRealPermission(permission), uid);
            } catch (RemoteException e) {
                Slog.e(GN_PERM_TAG, "PackageManager is dead?!?", e);
            }
        }

        permission = permission.replaceAll("/amigo", "");
        int checkPermission = PackageManager.PERMISSION_DENIED;
        try {
            checkPermission = AppGlobals.getPackageManager().checkUidPermission(amigoRealPermission(permission), uid);   
        } catch (RemoteException e) {
            // Should never happen, but if it does... deny!
            Slog.e(GN_PERM_TAG, "PackageManager is dead?!?", e);
            return PackageManager.PERMISSION_DENIED;
        }

		if (uid > Process.FIRST_APPLICATION_UID) {
			String packageName = amigoGetNameForUid(uid);
			if (AmigoPermission.getInstance().isSystemApp(packageName)) {
			    return checkPermission;
			}
		}

        if (PackageManager.PERMISSION_DENIED == checkPermission) {
            return checkPermission;
        }

        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
        if (bPackagePermDisableInSetting(uid) && amigoPermCtrlDenied(permission, uid)) {
            return PackageManager.PERMISSION_DENIED;
        }
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin

        //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 begin
        // Note: It's same by calling amigoInterceptPermission(...) as calling mPerAsyncTask.doInBackground(...),
        // mPerAsyncTask is meanless here.
        amigoInterceptPermission(permission, uid, PackageManager.PERMISSION_GRANTED, booted);
        /* 
        mExecuteCount++;
        if (mExecuteCount > MAX_THREAD) {
            mExecuteCount = 0;
            if (null != mPerAsyncTask && !mPerAsyncTask.isCancelled()) {
                mPerAsyncTask.cancel(true);
                mPerAsyncTask = null;
            }
            mPerAsyncTask = new AmigoPermissionAsyncTask();
            mPerAsyncTask.doInBackground(permission, String.valueOf(uid), String.valueOf(booted));
        } else {
            if (null == mPerAsyncTask) {
                mPerAsyncTask = new AmigoPermissionAsyncTask();
            }
            mPerAsyncTask.doInBackground(permission, String.valueOf(uid), String.valueOf(booted));
        }
        */
        //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 end
        
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> modify for CR01437187 begin
        String pkgName = amigoGetNameForUid(uid);
        if (booted && !amigoIsScreenLocked() && amigoPermCtrlDenied(permission, uid)) {
        // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> modify for CR01437187 end
            if (DEBUG_PERM) Log.i(GN_PERM_TAG, "pkgName:" + pkgName + ", permission:" + permission);
            if (amigoGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_ASK) {
                boolean foundFlag = false;
                int checkResult = PEMISSION_CONTROL_ASK;
                for (int i = 0; i < PEMISSION_CONTROL_SLEEP_COUNTS; i++) {
                    if(mStartSetting){
                        mStartSetting = false ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "mStartSetting true");
                        break ;
                    }
            
                    if(mPermissionsStatus == PEMISSION_CONTROL_DENIDE ){
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "PEMISSION_CONTROL_DENIDE");
                        break ;	
                    }
                    if(mPermissionsStatus == PEMISSION_CONTROL_GRANTED ){
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "PEMISSION_CONTROL_GRANTED");
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        foundFlag = true;
                        break;	
                    }
                    if (amigoIsScreenLocked()){
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "ScreenLocked");
                        break;
                    }

                    if (amigoGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
                        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "amigoGetPermStatus denied");
                        break ;
                    }

                    SystemClock.sleep(PEMISSION_CONTROL_SLEEP_TIME);
                }

                if (DEBUG_PERM)  Log.i(GN_PERM_TAG, "---->checkResult " + checkResult + ", foundFlag=" + foundFlag);

                if (!foundFlag){
                    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> add for CR01437187 begin
                    handleDialogTimeout(pkgName, permission); 
                    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> add for CR01437187 end
                    return PackageManager.PERMISSION_DENIED;
                }
           } else if (amigoGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                if (DEBUG_PERM) Log.i(GN_PERM_TAG, "---->getPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE ");
                try {
                    int times = AmigoPermission.getInstance().amigoGetPermDeniedTime(permission, pkgName);
                    if (times < 1) {
                        amigoSendNotificationBroacast(pkgName, permission,false);
                    }
                    AmigoPermission.getInstance().amigoSetPermDeniedTime(permission, pkgName, times + 1);

                } catch (Exception e) {
                    Log.i(GN_PERM_TAG, "exception:", e);
                }
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> add for CR01437187 begin            
                handleDialogTimeout(pkgName, permission);    
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> add for CR01437187 end
                return PackageManager.PERMISSION_DENIED;
            }
        }

        if (AmigoPermission.getInstance().amigoIsContainPermDenied(permission, pkgName)){
            AmigoPermission.getInstance().amigoSetPermDeniedTime(permission, pkgName, 0);
        }
        if (DEBUG_PERM) Log.i(GN_PERM_TAG, "Crant pkgName:" + pkgName + ", permission:" + permission);
        return PackageManager.PERMISSION_GRANTED;

    }

    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> modify for CR01437187 begin        
    private void handleDialogTimeout(String pkgName, String permission) {        
        String key = pkgName + permission;        
        int count = 0;        
        if(null != mPermCtrlResultMap && mPermCtrlResultMap.containsKey(key)){           
            count  = mPermCtrlResultMap.get(key);        
        }       
        Slog.i(GN_PERM_TAG, "isDialogTimeout key = " + key + ", count = " + count);       
        if (count >  0 && count < GN_MAX_WAIT_TIME) {          
            if (mPermDialogEntry != null && mPermDialogEntry.dialog != null && mPermDialogEntry.dialog.isShowing()) { 
                amigoClearPermMessages(pkgName);               
                mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);               
                mPermDialogEntry.dialog.dismiss();               
                mIsPermDialogShowing = false;            
            }        
        }   
	
        mPermissionsStatus = PEMISSION_CONTROL_ASK;    
    }    
	// Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-17> modify for CR01437187 end
    
    int amigoInterceptPermission(String permission, int uid, int returnValueWhenDenied, boolean booted) {
        if(amigoPermCtrlDenied(permission, uid)) {
            // If the system is not booted or the screen is locked,
            // we do not show the permisison dialog.
            if (!booted || amigoIsScreenLocked()) {
            } else {
                String pkgName = amigoGetNameForUid(uid);
                String permGrp = amigoGetPkgPermGrp(pkgName, permission);
                String key = pkgName + permission;
                
                if (amigoGetPermStatus(pkgName, permission) == PEMISSION_CONTROL_DENIDE) {
                    return returnValueWhenDenied;
                }
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-29> modify for CR01441255 begin
                synchronized (mPermMsgList) {
                    if (!TextUtils.isEmpty(pkgName) && !amigoIsPermMsgExisted(key)) {
                        // While there is a permission group check of a valid app, which is neither existed
                        // in the array nor dealing with, we new a check message and countdown.
                        Log.d(GN_PERM_TAG, "Waiting new control result of permission " + key);
                        mPermCtrlResultMap.put(key, GN_MAX_WAIT_TIME);
                    
                        Message msg = mPermCtrlHandler.obtainMessage(GN_SHOW_PERM_ERROR);
                        msg.arg1 = uid;
                        String[] arg = new String[] {key, pkgName, permGrp, permission};
                        msg.obj =  arg;
                        mPermMsgList.add(msg);
						
                        if (!mIsPermDialogShowing) {
                            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "No permission dialog is showing!");
                            try {
                                amigoPopPermMessage();
                            } catch (IllegalStateException e) {
                                if (DEBUG_PERM) Log.e(GN_PERM_TAG,"amigoPopPermMessage error ", e);          
                            }
                        }
                    }
                }
                // Gionee <Amigo_AppCheckPermission> <cheny> <2015-01-29> modify for CR01441255 end
            }
            return returnValueWhenDenied;
        }
        return PERMISSION_GRANTED;
    }
    
   private boolean amigoPermCtrlDenied(String permName, int uid) {
        if (uid <= 1000 || TextUtils.isEmpty(permName)) {
            return false;
        }
        boolean isDenied = false;
        if (uid > Process.FIRST_APPLICATION_UID) {
            String packageName = amigoGetNameForUid(uid);
            isDenied = AmigoPermission.getInstance().CheckPermDenied(permName,packageName);
            if (DEBUG_PERM) Log.d(GN_PERM_TAG, " isDenied = "+isDenied);
        }
        
        return isDenied;
    }
    
    private void amigoUpdatePermStatus(Context context, String pkgName, String permission, int status) {
        Message updateDb = mPermCtrlHandler.obtainMessage(GN_PERM_UPDATEDB);
        updateDb.arg1 = status;
        String[] arg = new String[] {pkgName, permission};
        updateDb.obj =  arg;
        mPermCtrlHandler.sendMessage(updateDb);
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-28> modify for CR01465771 begin	
    private void amigoUpdatePermDB(final String pkgName, final String permission, final int status) {

        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "update and start Activity!");
        AmigoPermission.getInstance().amigoUpdatePermStatus(permission, pkgName, status);

        mPermCtrlHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    ContentValues cv = new ContentValues();
                    cv.put("status", status);
                    mContext.getContentResolver().update(GN_PERM_URI, cv, " packagename = ? and permission =?",
                        new String[] {pkgName, permission});
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "updateData : pkgName = " + pkgName + 
					    " permission = " + permission + " status = " + status);
                } catch (Exception e){
                }
            }
        });
    }
    // Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-28> modify for CR01465771 end
	
    private void amigoStartSettingActivity(Context context,String packageName) {

        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "start Setting Activity!");

        Intent intent = new Intent();
        
        intent.setAction("com.gionee.permission");
        intent.addCategory("com.gionee.permission.category");
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try{
            PackageInfo pi = context.getPackageManager().getPackageInfo(packageName, 0); 
            String appName = pi.applicationInfo.loadLabel(context.getPackageManager()).toString();
            intent.putExtra("packagename", packageName);
            intent.putExtra("title", appName);
            context.startActivity(intent);
		    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
			mPackageSetting.add(packageName);
		    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end
        }catch (Exception e) {
            Log.d(GN_PERM_TAG, "getPackageInfo Error");
        }
    }
    
    private String amigoGetAppNameLabel(Context context,String pkgName) {
        String label = "";
        PackageManager pm = context.getPackageManager();
        try {
            ApplicationInfo  appInfo = pm.getApplicationInfo(pkgName, 0);
            String temp = (String)pm.getApplicationLabel(appInfo);
            label = ( TextUtils.isEmpty(temp) ) ? appInfo.processName : temp;
        } catch (NameNotFoundException e) {
            Log.d(GN_PERM_TAG, "Invalid group name:" + pkgName);
        }
        return label;
    }
    
    public String amigoGetPermKey(int uid, String permission) {
        String pkgName = amigoGetNameForUid(uid);
        String permGrp = amigoGetPkgPermGrp(pkgName, permission);
        return pkgName + permGrp;
    }

    public String amigoGetPkgPermGrp(String pkgName, String permName) {
        return AmigoPermission.getInstance().amigoGetPkgPermGrp(pkgName, permName);
    }
    

    private class AmigoPermDialogEntry {
        String key;
        int uid;
        String pkgName;
        AlertDialog dialog;
        String perm;
        
        public AmigoPermDialogEntry(String key, int uid, String pkgName, String perm, AlertDialog dialog) {
            this.key = key;
            this.uid = uid;
            this.pkgName = pkgName;
            this.dialog = dialog;
            this.perm = perm;
        }		
    }
    
    private boolean amigoIsScreenLocked() {
        if (mKeyguardManager == null) {
            mKeyguardManager = (KeyguardManager)
            mContext.getSystemService(Context.KEYGUARD_SERVICE);
        }
        boolean isScreenLocked = mKeyguardManager == null || mKeyguardManager.isKeyguardLocked();
        return isScreenLocked;
    }
    
    private boolean amigoIsPermMsgExisted(String key) {
        boolean existed = false;
        if (mPermDialogEntry != null &&
                mIsPermDialogShowing && mPermDialogEntry.key.equals(key)) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The permission dialog of " + key + " is already showing");
            existed = true;
        }
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-07> modify for CR01461211 begin
        synchronized (mPermMsgList) {
            for (Message msg : mPermMsgList) {
                if (((String[]) msg.obj)[0].equals(key)) {
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The permission message of " + key + " is already existed");
                    existed = true;
                }
            }
        }
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-07> modify for CR01461211 begin
        return existed;
    }
    
    private void amigoPopPermMessage() {
        mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);
        if (mPermMsgList.size() == 0) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Permission messages array is empty!");
            mIsPermDialogShowing = false;
            return;
        }
        Message msg = mPermMsgList.remove(0);
        if (!mPermCtrlResultMap.containsKey(((String[]) msg.obj)[0])) {
            if (DEBUG_PERM) Log.d(GN_PERM_TAG,  ((String[]) msg.obj)[0] + "'s permission message is already overdue!");
            mIsPermDialogShowing = false;
            amigoPopPermMessage();
            return;
        }
        mIsPermDialogShowing = true;
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-08-24> modify for CR01541957 begin
        mPermissionsStatus = PEMISSION_CONTROL_ASK ;
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-08-24> modify for CR01541957 end
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Pop message " + ((String[]) msg.obj)[0]);
        mPermCtrlHandler.sendMessage(msg);
    }
    
    private void amigoClearPermMessages(String pkgName) {        
        Set<String> trashSet = new HashSet<String>();
        for (String key : mPermCtrlResultMap.keySet()) {
            if (key.contains(pkgName)) {
                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Find trash " + key);
                trashSet.add(key);
            }
        }
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-07> modify for CR01461211 begin
        synchronized (mPermMsgList) {
            for (int i = mPermMsgList.size() - 1; i >= 0; i--) {
                String key = ((String[])mPermMsgList.get(i).obj)[0];
                if (trashSet.contains(key)) {
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Remove trash " + key + " from mPermMsgList!");
                    mPermMsgList.remove(i);
                }
            }

            for (String key : trashSet) {
                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Remove trash " + key + " from maps!");
                mPermCtrlResultMap.remove(key);    
            }
        }
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-04-07> modify for CR01461211 end        
        trashSet.clear();
    }
    
    private void amigoCleanAllPermMessages() {
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Clean all permission messages!");

        if (mPermDialogEntry != null && mPermDialogEntry.dialog.isShowing()) {
            mPermCtrlHandler.removeMessages(GN_PERM_COUNT_DOWN);
            if (mPermCtrlResultMap.get(mPermDialogEntry.key) != null 
                    && mPermCtrlResultMap.get(mPermDialogEntry.key) >= 0 
                    && mPermDialogEntry.pkgName != null 
                    && mPermDialogEntry.perm != null) {
                if(!mRememberMe){
            	    amigoUpdatePermStatus(mContext, mPermDialogEntry.pkgName, mPermDialogEntry.perm, PEMISSION_CONTROL_ASK);
                } else {
            	    amigoUpdatePermStatus(mContext, mPermDialogEntry.pkgName, mPermDialogEntry.perm, PEMISSION_CONTROL_DENIDE);
                }//if(!mRememberMe
            }//if (mPermCtrlResultMap.get(mPermDialogEntry.key) != null 

            mPermCtrlResultMap.put(mPermDialogEntry.key, -1);
            mPermDialogEntry.dialog.dismiss();
        }//if (mPermDialogEntry != null 
   
        mIsPermDialogShowing = false;
        mPermMsgList.clear();
        mPermCtrlResultMap.clear();

        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
        mPackageSetting.clear();
        //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end
    }
    
    private class AmigoPermCtrlHandler extends Handler {

        AmigoPermCtrlHandler(Looper looper) {
            super(looper);
        }
        
        public void handleMessage(Message msg) {

            switch (msg.what) {

                case GN_SHOW_PERM_ERROR: {
                    final String key = ((String[]) msg.obj)[0];
                    final String pkgName = ((String[]) msg.obj)[1];
                    final String permGrp = ((String[]) msg.obj)[2];
                    final String permission = ((String[]) msg.obj)[3];
                    final int uid = msg.arg1;

                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "permGrp = "+permGrp + " uid = "+uid + " pkgName = "+pkgName);
		//Toast.makeText(mContext, "zhangwei permGrp = "+permGrp + " uid = "+uid + " pkgName = "+pkgName, Toast.LENGTH_LONG).show();
		 Resources res= mContext.getResources();
 		String message = res.getString(com.aurora.R.string.aurora_perm_meg, amigoGetAppNameLabel(mContext, pkgName));
                    message += amigoGetPkgPermLabel(permission);
                    //String messageMore = res.getString(com.aurora.R.string.aurora_perm_more);
					// Gionee <Amigo_AppCheckPermission> <wujj> <2015-09-08> add for CR01549798 begin
					//message += messageMore;
					// Gionee <Amigo_AppCheckPermission> <wujj> <2015-09-08> add for CR01549798 end
		AlertDialog permDialog = new AlertDialog.Builder(
														  mContext, AlertDialog.THEME_HOLO_LIGHT)
		   .setTitle(res.getString(com.aurora.R.string.aurora_perm_title))
		   //.setMessage(message)
														 /* .setNegativeButton(res.getString(com.aurora.R.string.aurora_perm_setting), new DialogInterface.OnClickListener() {

					   @Override
					   public void onClick(
							   DialogInterface dialog,
							   int which) {
						 mStartSetting = true ;
                                    amigoClearPermMessages(pkgName);
                                    amigoStartSettingActivity(mContext,pkgName);
                                    //mService.forceStopPackage(pkgName, -1);
                                    amigoPopPermMessage();
						   
					   }

				   })*/ .setNeutralButton(res.getString(com.aurora.R.string.aurora_perm_cancel), new DialogInterface.OnClickListener() {

					   @Override
					   public void onClick(
							   DialogInterface dialog,
							   int which) {
						 mPermCtrlResultMap.remove(key);
                            	    mPermissionsStatus = PEMISSION_CONTROL_DENIDE ;
                                    if(!mRememberMe){
                	                    mRememberMe = true ;
                	                    amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                                        } else { 	    
                	                    amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_DENIDE);
                	                }
                                    amigoPopPermMessage();
						   
					   }

				   }).setPositiveButton(res.getString(com.aurora.R.string.aurora_perm_ok),
																   new DialogInterface.OnClickListener() {
							   
																	   @Override
																	   public void onClick(
																			   DialogInterface dialog,
																			   int which) {
																		mPermCtrlResultMap.remove(key);
                                    mPermissionsStatus = PEMISSION_CONTROL_GRANTED ;
                        	        if(!mRememberMe){
                        	            amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                        	        } else {
                	        	        amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_GRANTED);
                	                }
                                    amigoPopPermMessage();
																		   
																	   }
							   
																   }).create();
				
			permDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "The showing dialog dimiss!");

                    if (mPermCtrlResultMap != null && key != null && mPermCtrlResultMap.get(key) != null && mPermCtrlResultMap.get(key) >= 0) {
                        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "We remove the dialog from the map!");

                        if(!mRememberMe){
    	                    mRememberMe = true ;
    	                    amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
    	                } else {
    	                    amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_DENIDE);
    	                }
                        mPermissionsStatus = PEMISSION_CONTROL_DENIDE ;
                        amigoClearPermMessages(pkgName);
                        amigoPopPermMessage();
                    } // (mPermCtrlResultMap != null
                }// onDismiss
            });
				
		   permDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
		   //mWifiConDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		   View layout = amigoBuildPermissionView(message, permission);
		   permDialog.setCanceledOnTouchOutside(false);
		   permDialog.setView(layout);
		   
		 
												   
			

                    mPermDialogEntry = new AmigoPermDialogEntry(key, uid, pkgName, permission, permDialog);
                    mPermDialogEntry.dialog.show();
                   /* if (mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEGATIVE) != null) {
                        mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(0xffff9000);
                    }
                    Message countDownMsg = mPermCtrlHandler.obtainMessage(GN_PERM_COUNT_DOWN);
                    countDownMsg.obj = key;
                    mPermCtrlHandler.sendMessageDelayed(countDownMsg, 1000);*/
                    break;
                } // case GN_SHOW_PERM_ERROR
            
                case GN_PERM_COUNT_DOWN:{
                    String key = (String) msg.obj;
                    if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down " + key);

                    if (amigoIsScreenLocked()) {
                        amigoCleanAllPermMessages();
                    } else {
                        // we need to check if the perm_map has the key ,if not, get key will return null,conn't convert to int type.
                        if(null != mPermCtrlResultMap && mPermCtrlResultMap.containsKey(key)){
                            int count = mPermCtrlResultMap.get(key);
                            count --;
                            mPermCtrlResultMap.put(key, count);
                            mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setText(
                                    String.format(mContext.getResources().getString(com.aurora.R.string.aurora_perm_cancel), count));
                        
                            if (count > 0) {
                                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down!");
                                msg = mPermCtrlHandler.obtainMessage(GN_PERM_COUNT_DOWN);
                                msg.obj = key;
                                mPermCtrlHandler.sendMessageDelayed(msg, 1000);
                            } else if (count == 0) {
                                if (DEBUG_PERM) Log.d(GN_PERM_TAG, "Count down end!");
                                mPermDialogEntry.dialog.getButton(AlertDialog.BUTTON_NEUTRAL).performClick();
                            }
                        }//if(null != mPermCtrlResultMap		
                    }
                    break;
                } // case GN_PERM_COUNT_DOWN

                case GN_PERM_UPDATEDB:{				
                    final String pkgName = ((String[]) msg.obj)[0];
                    final String permission = ((String[]) msg.obj)[1];
                    final int status = msg.arg1;
                    amigoUpdatePermDB(pkgName, permission, status);
	            break;
                } //  case GN_PERM_UPDATEDB
            }//switch
        }//handleMessage    
    }; //AmigoPermCtrlHandler   

    //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 begin
    /*
    private class AmigoPermissionAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String permission = params[0];
            int uid = Integer.parseInt(params[1]);
            boolean booted = Boolean.parseBoolean(params[2]);
            amigoInterceptPermission(permission, uid, PackageManager.PERMISSION_GRANTED, booted);
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }
    */
    //GIONEE <Amigo_AppCheckPermission> <wujj> <2015-10-19> modify for CR01566312 end

    private int amigoGetPermStatus(String pkgName, String permission) {
        return AmigoPermission.getInstance().amigoGetPermStatus(permission, pkgName);
    }

    private void amigoSendNotificationBroacast(String pkgName, String permission,boolean important) {
        Intent intent = new Intent();
        intent.setAction("Permissio.Action.Denied");
        intent.putExtra("pkgName", pkgName);
        intent.putExtra("permission", permission);
        intent.putExtra("important", important);
        mContext.sendBroadcast(intent);
    }

    private String amigoGetPkgPermLabel(final String permission) {
        String tempGrpName = permission;
        if (permission.contains("WRITE_SMS_MMS")) {
            return mContext.getResources().getString(com.aurora.R.string.aurora_permlab_writeMms);
        }
        if (permission.contains("READ_SMS_MMS")) {
            return mContext.getResources().getString(com.aurora.R.string.aurora_permlab_readMms);
        }
        if (permission.contains("SEND_SMS_MMS")) {
            return mContext.getResources().getString(com.aurora.R.string.aurora_perm_send_mms);
        }
        PermissionInfo pgi = null;
        PackageManager mPm = mContext.getPackageManager();
        if (null != mPm && null != tempGrpName) {
            try {
                pgi = mPm.getPermissionInfo(tempGrpName, 0);
            } catch (NameNotFoundException e) {
                Log.d(GN_PERM_TAG, "Invalid group name:" + tempGrpName);
                if (tempGrpName.equals("DefaultGrp")) {
                    tempGrpName = mContext.getResources().getString(
                            com.aurora.R.string.aurora_perm_default);
                }
            }
            if (null != pgi) {
                tempGrpName = pgi.loadLabel(mPm).toString();
            }
        }
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "getPkgPermGrupLabel return :" + tempGrpName);
        return tempGrpName;
    }

    private void amigoResetNoRememberPermission(String pkgName,String permission,int status) {
    	switch(status) {
            case PEMISSION_CONTROL_GRANTED :
	        if(!mRememberMe){
                    mRememberMe = true ;
                    amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
                }
                break;
            case PEMISSION_CONTROL_DENIDE :
               if(!mRememberMe){
                   mRememberMe = true ;
                   amigoUpdatePermStatus(mContext, pkgName, permission, PEMISSION_CONTROL_ASK);
               }
               break;
            default :
                break ;
    	}
    }

    private String amigoTransPermission(String permission) {
        if(permission == null){
            return null;
        }
        String result = permission;
        if (result.contains("CONTACTS_CALLS")) {
            result = result.replace("_CONTACTS_CALLS", "_CALL_LOG");
        }
        return result;
    }

    private String amigoRealPermission(String permission) {
        if(permission == null){
            return null;
        }
        String result = permission;
        if (result.contains("_SMS_MMS")) {
            result = result.replace("_SMS_MMS", "_SMS");
        }
        return result;
    }

    private View amigoBuildPermissionView(String message,String permission) {  
    	LinearLayout layout = new LinearLayout(mContext);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        TextView textView = new TextView(mContext);
        textView.setText(message);
        textView.setTextColor(0xcc000000);
        textView.setTextSize(18);
        int margin = amigoDip2px(mContext, 1);
        int dividerMargin = amigoDip2px(mContext, 1);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(margin, dividerMargin, margin, 0);
        layout.addView(textView,layoutParams);
		// Gionee <Amigo_AppCheckPermission> <wujj> <2015-09-08> add for CR01549798 begin
		/*
        TextView textViewMore = new TextView(mContext);
        if (textViewMore != null && messageMore != null) {
            textViewMore.setText(messageMore);	
            textViewMore.setTextSize(18);
            textViewMore.setTextColor(0xcc000000);
            LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
            layoutParams1.setMargins(margin, 0, margin, 0);
            layout.addView(textViewMore, layoutParams1);
        }
		*/
		// android <Amigo_AppCheckPermission> <wujj> <2015-09-08> add for CR01549798 end

        final CheckBox checkBox = new CheckBox(mContext);
        checkBox.setTextSize(12);
        if (AmigoCheckPermission.mSpecialPerHs.contains(permission)
                && AmigoPermission.NONE_TRUST.equalsIgnoreCase(SystemProperties.get("persist.sys.permission.level","0"))) {
            mRememberMe = false;
            checkBox.setChecked(false);
        } else {
            mRememberMe = true;
            checkBox.setChecked(true);
        }
        checkBox.setText(mContext.getResources().getString(com.aurora.R.string.aurora_perm_remember_my_choice)); 
        checkBox.setTextColor(0x66000000);
        checkBox.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    mRememberMe = true ;
                } else {
                    mRememberMe = false ;
                }
            }
        });		

        View dividerView = new View(mContext);
        
        LinearLayout.LayoutParams dividerParams = new LinearLayout.LayoutParams(
        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        dividerParams.setMargins(dividerMargin, dividerMargin, dividerMargin, dividerMargin);			
        dividerView.setBackgroundColor(0XFFBBBBBB);	
        //layout.addView(dividerView,dividerParams);		
        layout.addView(checkBox,dividerParams);	
        return layout;
    }

    private int amigoDip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    boolean amigoCheckWritePermission(ProviderInfo cpi) {
        return cpi.writePermission != null
                        && (cpi.writePermission.contains("WRITE_SMS") 
                                    || cpi.writePermission.contains("WRITE_CONTACTS") 
                                    || cpi.writePermission.contains("WRITE_CALL_LOG"));
    }

    boolean amigoCheckReadPermission(ProviderInfo cpi) {
        return cpi.readPermission != null  
                      && (cpi.readPermission.contains("READ_SMS") 
                                  || cpi.readPermission.contains("READ_CONTACTS") 
                                  || cpi.readPermission.contains("READ_CALL_LOG"));
    }   
	
    public String amigoGetNameForUid(int uid) {
        String pkgList[] = mContext.getPackageManager().getPackagesForUid(uid);
        if(null == pkgList || 0 == pkgList.length) {
            return null;
        }
        return pkgList[0];
    }

    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 begin
    public void setPackagePermEnable(String pk){
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "setPackagePermEnable pk: " + pk);
        mPackageSetting.remove(pk);
    }

    private boolean bPackagePermDisableInSetting(int uid){
        String pk = amigoGetNameForUid(uid);		
        boolean bFound = false;
        for (int i = 0; i < mPackageSetting.size(); i++) {
            if (pk.equals(mPackageSetting.get(i))) {
                bFound = true;
                break;
            }
        }
        if (DEBUG_PERM) Log.d(GN_PERM_TAG, "bPackagePermEnable pk: " + pk + " bFound " + bFound);
        return bFound;
    }
    //Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end
}
//Gionee <Amigo_AppCheckPermission> <cheny> <2015-02-04> add for CR01444758 end
