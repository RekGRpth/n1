/*
 * SmartStayMonitor 
 * Desciption:
 * 	This class is to implement the function that stay awake intelligently via camera face detection.
 *
 * Author : wutangzhi
 * Date	  : 2014/10/24
 * Email  : wutz@gionee.com
 **/

package android.hardware;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.content.Context;
import android.view.Surface;
import android.view.OrientationEventListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class SmartStayMonitor implements FaceDetectionListener
{

	private static final String TAG = "SmartStayMonitor";

	private static final int ORIENTATION_HYSTERESIS = 35;
	private static final int FACE_DETECTION_TIME	= 1000;

	private static final int MSG_FACE_DETECTION_TIMEOUT = 0;

	public static final int SAMRT_STAY_EVENT_KEEP_AKAKE = 0;
	public static final int SAMRT_STAY_EVENT_STANDBY	= 1;

	private Camera mCamera;
	private CameraInfo mCameraInfo;
	private Parameters mParameters;
	private StartupCameraThread mStartupCameraThread;
	private MyOrientationListener mOrientationListener;
	private SmartStayListener mSmartStayListener;
	private MainHandler mHandler;

	private int mOrientation;
	private int mOrientationCompensation;

	private Object mLock;
	private boolean mTimeout;

	public interface SmartStayListener {
		void onSmartStayEvent(int event);
	}
	
	private class StartupCameraThread extends Thread {		
		@Override
		public void run() {
			if (mCamera == null) {
				
				
				try {
					// Gionee <litao> <2015-04-26> modify for CR01467788 begin
					if(!Camera.isCameraRunning())
					{
					// Gionee <litao> <2015-04-26> modify for CR01467788 end
						mCamera = Camera.open(CameraInfo.CAMERA_FACING_FRONT);
						if (mCamera == null) {
							Log.e(TAG, "Failed to open front camera.");
						} else {
							mCameraInfo = new CameraInfo();
							Camera.getCameraInfo(CameraInfo.CAMERA_FACING_FRONT, mCameraInfo);
								
							//Set MTK preview Mode in MTK platform
							mParameters = mCamera.getParameters();
							mParameters.set("mtk-cam-mode", 1);
							mCamera.setParameters(mParameters);	
								
							//Start preview
							mCamera.startPreview();

							//Enable face detection and register the face detection listener;
							if (mCamera != null) {
								mCamera.setFaceDetectionListener(SmartStayMonitor.this);
								mCamera.startFaceDetection();
							}			

							//Monitor the orientation change
							if (mOrientationListener != null) {
								mOrientationListener.enable();
							}
							// Gionee <litao> <2015-03-06> modify for CR01450814 begin
								mCamera.setSmartStayMonitor(true);
								   Log.d(TAG,"running:"+mCamera.isSmartStayMonitorRunning());
							// Gionee <litao> <2015-03-06> modify for CR01450814 end
						}
					// Gionee <litao> <2015-04-26> modify for CR01467788 begin
					}
					else
					{
						Log.d(TAG,"camera is used by another ap cameraRunning=" + Camera.isCameraRunning());
					}
					// Gionee <litao> <2015-04-26> modify for CR01467788 end
					
				} catch (Exception e) {
					Log.e(TAG, "Failed to open camera. Please check the camera device.");
				}

				if (mHandler != null) {
					mHandler.sendEmptyMessageDelayed(MSG_FACE_DETECTION_TIMEOUT, FACE_DETECTION_TIME);
				}
			}
		}
	}
	
	private class MyOrientationListener extends OrientationEventListener {
		public MyOrientationListener(Context context) {
			super(context);
		}

		@Override
		public void onOrientationChanged(int orientation) {
			int orientationCompensation = 0;
			int rotation = 0;
			
			if (orientation == ORIENTATION_UNKNOWN) {
				return;
			}
			
			mOrientation = roundOrientation(orientation, mOrientation);
			orientationCompensation = mOrientation % 360;
			
			if (mOrientationCompensation != orientationCompensation) {
				mOrientationCompensation = orientationCompensation;
				
				if (mCamera != null && mParameters != null) {
					rotation = (mCameraInfo.orientation - orientationCompensation + 360) % 360;
					mParameters.setRotation(rotation);
					mCamera.setParameters(mParameters);
				}
			}
		}
	}

	private class MainHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			synchronized(mLock) {
				if (msg.what == MSG_FACE_DETECTION_TIMEOUT && mSmartStayListener != null) {
					stop();

					mTimeout = true;
					mSmartStayListener.onSmartStayEvent(SAMRT_STAY_EVENT_STANDBY);
				}
			}
		}
	}


	public SmartStayMonitor(Context context) {
		mOrientation = -1;
		mOrientationCompensation = -1;

		mOrientationListener = new MyOrientationListener(context);
		mStartupCameraThread = new StartupCameraThread();
		mHandler = new MainHandler();

		mLock = new Object();
		mTimeout = false;

		mSmartStayListener = null;
	}

	public void setSmartStayMonitorListener(SmartStayListener listener) {
		synchronized(mLock) {
			mSmartStayListener = listener;
		}
	}

	public boolean start() {
		boolean ret = true;

		if (mStartupCameraThread != null) {
			mStartupCameraThread.start();
		}

		return ret;
	}

	public synchronized void stop() {
		try {
			if (mStartupCameraThread != null) {
				mStartupCameraThread.join();
				mStartupCameraThread = null;
			}
		} catch (InterruptedException e) {
			Log.e(TAG, e.toString());
		}

		if (mOrientationListener != null) {
			mOrientationListener.disable();
		}

		if (mHandler != null) {
			mHandler.removeMessages(MSG_FACE_DETECTION_TIMEOUT);
		}

		if (mCamera != null) {
			mCamera.setFaceDetectionListener(null);
			mCamera.stopFaceDetection();

			mCamera.stopPreview();
			mCamera.release();

			// Gionee <litao> <2015-03-06> modify for CR01450814 begin
			mCamera.setSmartStayMonitor(false);
			Log.d(TAG,"running:"+mCamera.isSmartStayMonitorRunning());
			// Gionee <litao> <2015-03-06> modify for CR01450814 end
			mParameters = null;
			mCamera = null;
		}
	}

	@Override	
	public void onFaceDetection(Face[] faces, Camera camera) {
		if (mHandler != null) {
			mHandler.removeMessages(MSG_FACE_DETECTION_TIMEOUT);
		}

		synchronized(mLock) {
			if (!mTimeout && mSmartStayListener != null) {
				mSmartStayListener.onSmartStayEvent(SAMRT_STAY_EVENT_KEEP_AKAKE);
			}
		}
	}
	
	private int roundOrientation(int orientation, int orientationHistory) {
        boolean changeOrientation = false;
        
        if (orientationHistory == OrientationEventListener.ORIENTATION_UNKNOWN) {
            changeOrientation = true;
        } else {
            int dist = Math.abs(orientation - orientationHistory);
            dist = Math.min(dist, 360 - dist);
            changeOrientation = (dist >= 45 + ORIENTATION_HYSTERESIS);
        }    
        
        if (changeOrientation) {
            return ((orientation + 45) / 90 * 90) % 360;
        }          
        
        return orientationHistory;
    } 
}
