package android.content.res;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;

import android.app.ActivityThread;
import android.content.res.AuroraThemeZip.ThemeFileInfo;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.graphics.drawable.Drawable.ConstantState;
import android.os.IBinder;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.SparseArray;
import android.util.TypedValue;

/**
 * 继承系统Resources类，并重写对应的资源获取文件来重定向
 * 资源路径
 * @author alexluo
 *
 */
public class AuroraThemeResources extends Resources {

	private static final String TAG = "AuroraTheme";

	private static final String NINE_PATCH_SUFFIX = ".9.png";

	private HashMap<Integer, WeakReference<ThemeFileInfo>> mCachedThemeFile = new HashMap<Integer, WeakReference<ThemeFileInfo>>();

	private final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> mDrawableCache = new ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>>();
	private final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> mColorDrawableCache = new ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>>();

	private final LongSparseArray<WeakReference<ColorStateList>> mColorStateListCache = new LongSparseArray<WeakReference<ColorStateList>>();

	// These are protected by mAccessLock.
	private final Object mAccessLock = new Object();

	private SparseArray mThemeValues = new SparseArray();

	private boolean mThemeValuesLoaded = false;

	private AuroraApplicationTheme mTheme;

	private boolean clearFromConfigChanged = false;

	 String mPackageName;

	public AuroraThemeResources() {
		super();
		// TODO Auto-generated constructor stub
		mPackageName = ActivityThread.currentPackageName();
		init();
	}

	public AuroraThemeResources(AssetManager assets, DisplayMetrics metrics,
			Configuration config, CompatibilityInfo compatInfo, IBinder token) {
		super(assets, metrics, config, compatInfo, token);
		// TODO Auto-generated constructor stub
		mPackageName = ActivityThread.currentPackageName();
		init();
	}

	public AuroraThemeResources(AssetManager assets, DisplayMetrics metrics,
			Configuration config) {
		super(assets, metrics, config);
		// TODO Auto-generated constructor stub

	}

	public InputStream hasAssetsFile(String assetPath){
		return mTheme.getAssetsFile(assetPath);
	}
	
	/**
	 * 初始化主题资源
	 */
	private void init() {
		mThemeValues.clear();
		mTheme = new AuroraApplicationTheme(this, mPackageName);
		mTheme.update();
		mThemeValuesLoaded = mTheme.hasThemeValue();
		mTheme.addSystemResources(AuroraApplicationTheme.SYSTEM_RES_ANDROID);
		mTheme.addSystemResources(AuroraApplicationTheme.SYSTEM_RES_IUNI);
		clear(false);

	}

	public AuroraApplicationTheme getTheme() {

		return mTheme;
	}

	@Override
	public void intTheme(String pkgName) {
		// TODO Auto-generated method stub
		super.intTheme(pkgName);
		mPackageName = pkgName;
		init();
	}
	
	@Override
	public float getDimension(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			if(mThemeValuesLoaded){
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return dimen;
				}
			}
		}
		
		return super.getDimension(id);
	}
	
	@Override
	public int getDimensionPixelOffset(int id) throws NotFoundException {
		
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			if(mThemeValuesLoaded){
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return (int)(dimen+0.5f);
				}
			}
		}
		
		return super.getDimensionPixelOffset(id);
	}
	
	@Override
	public int getDimensionPixelSize(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			if(mThemeValuesLoaded){
				Float dimen = mTheme.getFloatThemeValue(id);
				if(dimen != null){
					return (int)(dimen+0.5f);
				}
			}
		}
		
		return super.getDimensionPixelSize(id);
	}
	

	
	@Override
	 public boolean getBoolean(int id) throws NotFoundException {
		synchronized (mAccessLock) {
			if(mThemeValuesLoaded){
				Integer bool = mTheme.getThemeValue(id);
				if(bool != null){
					return bool != 0;
				}
			}
		}
		
		return super.getBoolean(id);
	}
	
	@Override
	public int getInteger(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			
			if(mThemeValuesLoaded){
				Integer integer = mTheme.getThemeValue(id);
				if(integer != null){
					return integer;
				}
			}
			
		}
		
		return super.getInteger(id);
	}
	
	@Override
	public int getColor(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		synchronized (mAccessLock) {
			if(mThemeValuesLoaded){
				Integer color = mTheme.getThemeValue(id);
				if(color != null){
					return color;
				}
			}
		}
		
		return super.getColor(id);
	}
	
	
	
	
	@Override
	ColorStateList loadColorStateList(TypedValue value, int id)
			throws NotFoundException {
		// TODO Auto-generated method stub
		TypedValue themeValue = new TypedValue();
		themeValue.setTo(value);
		final long key = (((long) themeValue.assetCookie) << 32)
				| themeValue.data;
		ColorStateList csl = getCachedColorStateList(key);
		if (csl != null) {
			return csl;
		}
		/*
		 * 读取主题中的颜色、尺寸等（不包括图片）
		 */
		if (mThemeValuesLoaded) {
			Integer color = mTheme.getThemeValue(id);
			if (color != null) {
				csl = ColorStateList.valueOf(color);
				if (csl != null) {
					mColorStateListCache.put(key,
							new WeakReference<ColorStateList>(csl));
					return csl;
				}
			}

		}

		return super.loadColorStateList(value, id);
	}

	@Override
	Drawable loadDrawable(TypedValue value, int id, Theme theme)
			throws NotFoundException {
		// TODO Auto-generated method stub
		/*
		 * add for theme
		 */
		final Drawable themeDrawable = loadDrawableFromTheme(value, id, theme);
		if (themeDrawable != null) {
			return themeDrawable;
		}

		return super.loadDrawable(value, id, theme);
	}

	private ColorStateList getCachedColorStateList(long key) {
		synchronized (mAccessLock) {
			WeakReference<ColorStateList> wr = mColorStateListCache.get(key);
			if (wr != null) { // we have the key
				ColorStateList entry = wr.get();
				if (entry != null) {
					// Log.i(TAG, "Returning cached color state list @ #" +
					// Integer.toHexString(((Integer)key).intValue())
					// + " in " + this + ": " + entry);
					return entry;
				} else { // our entry has been purged
					mColorStateListCache.delete(key);
				}
			}
		}
		return null;
	}

	/**
	 * 读取主题包中的图片
	 * 
	 * @param value
	 * @param id
	 * @param theme
	 * @return
	 */
	protected Drawable loadDrawableFromTheme(TypedValue value, int id,Theme theme) {
		Drawable drawable = null;
		ThemeFileInfo themeFile = null;
		String srcName = value.string != null?value.string.toString():null;
		   final boolean isColorDrawable;
	        final ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches;
	        final long key;
	        TypedValue themeValue = new TypedValue();
			themeValue.setTo(value);
	        if (themeValue.type >= TypedValue.TYPE_FIRST_COLOR_INT
	                && themeValue.type <= TypedValue.TYPE_LAST_COLOR_INT) {
	            isColorDrawable = true;
	            caches = mColorDrawableCache;
	            key = themeValue.data;
	        } else {
	            isColorDrawable = false;
	            caches = mDrawableCache;
	            key = (((long) themeValue.assetCookie) << 32) | themeValue.data;
	        }
	        final Drawable cachedDrawable = getCachedDrawable(caches, key, theme);
            if (cachedDrawable != null) {
                return cachedDrawable;
            }
		  if(isColorDrawable){
			  Integer color = mTheme.getThemeValue(id);
			  if(color != null){
				  drawable = new ColorDrawable(color);
			  }
		  }else{
				String path = srcName;
				themeFile  = mTheme.getThemeFileInfo(path,id);
				if(themeFile != null){
					mCachedThemeFile.put(id, new WeakReference<ThemeFileInfo>(themeFile));
					if(path.endsWith(NINE_PATCH_SUFFIX)){
						try {
							drawable = AuroraDrawableUtils.decodeDrawableFromStream(themeFile.inputStream,this);
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}finally{
							try{
							themeFile.inputStream.close();
							}catch(Exception ex){
								
							}
						}
					}else{
						if(value.density != themeFile.density){
							value.density = themeFile.density;
						}
						
						drawable = Drawable.createFromResourceStream(this, value, themeFile.inputStream, themeFile.srcName, null);
						try {
							themeFile.inputStream.close();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}finally{
							try{
								themeFile.inputStream.close();
							}catch(Exception ex){
								
							}
						}
					}
					
					
				}
		  }
		if(drawable != null){
			drawable.setChangingConfigurations(value.changingConfigurations);
			cacheDrawable(value, null, false, caches, key, drawable);
		}
		return drawable;
	}

	@Override
	public void updateConfiguration(Configuration config,
			DisplayMetrics metrics, CompatibilityInfo compat) {
		// TODO Auto-generated method stub
		super.updateConfiguration(config, metrics, compat);
		/*
		 * 如果主题改变了就清理主题缓存
		 */
		try{
			clear(true);
		}catch(Exception e){
			Log.w(TAG, "ThemeConfiguration doesn't init->"+e);
		}
	}

	Integer getThemeValue(int index) {
		Integer integer;
		if (!mThemeValuesLoaded) {
			integer = null;
		} else {
			int j = mThemeValues.indexOfKey(index);
			if (j >= 0) {
				integer = (Integer) mThemeValues.valueAt(j);
			} else {
				integer = mTheme.getThemeValue(index);
				mThemeValues.put(index, integer);
			}
		}
		return integer;
	}

	private TypedArray getThemeTypedArray(TypedArray typedarray) {
		if (mTheme != null || mThemeValuesLoaded) {
			int ai[] = typedarray.mData;
			int i = 0;
			while (i < ai.length) {
				int j = ai[i + 0];
				int k = ai[i + 3];
				if (j >= 16 && j <= 31 || j == 5) {
					Integer integer = getThemeValue(k);
					if (integer != null)
						ai[i + 1] = integer.intValue();
				}
				i += 6;
			}
		}
		return typedarray;

	}

	private void clear(boolean fromConfig) {
		if (mColorDrawableCache != null) {
			mColorDrawableCache.clear();
		}
		if (mDrawableCache != null) {
			mDrawableCache.clear();
		}

		if (mTheme != null && fromConfig) {
			mTheme.freeCaches();
		}
		if (mColorStateListCache != null) {
			mColorStateListCache.clear();
		}
	}

	protected void clearDrawableCachesLocked(
			ArrayMap<String, LongSparseArray<WeakReference<ConstantState>>> caches) {
		final int N = caches.size();
		for (int i = 0; i < N; i++) {
			clearDrawableCacheLocked(caches.valueAt(i));
		}
	}

	private void clearDrawableCacheLocked(
			LongSparseArray<WeakReference<ConstantState>> cache) {
		final int N = cache.size();
		for (int i = 0; i < N; i++) {
			final WeakReference<ConstantState> ref = cache.valueAt(i);
			if (ref != null) {
				final ConstantState cs = ref.get();
				if (cs != null) {
					cache.setValueAt(i, null);
				}
			}
		}
	}

}
