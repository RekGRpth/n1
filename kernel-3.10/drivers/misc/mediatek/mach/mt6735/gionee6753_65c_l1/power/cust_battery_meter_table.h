#ifndef _CUST_BATTERY_METER_TABLE_H
#define _CUST_BATTERY_METER_TABLE_H

#include <mach/mt_typedefs.h>

// ============================================================
// define
// ============================================================
#define BAT_NTC_10 1
#define BAT_NTC_47 0

#if (BAT_NTC_10 == 1)
#define RBAT_PULL_UP_R             16900	
#endif

#if (BAT_NTC_47 == 1)
#define RBAT_PULL_UP_R             61900	
#endif

#define RBAT_PULL_UP_VOLT          1800



// ============================================================
// ENUM
// ============================================================

// ============================================================
// structure
// ============================================================

// ============================================================
// typedef
// ============================================================
typedef struct _BATTERY_PROFILE_STRUC
{
    kal_int32 percentage;
    kal_int32 voltage;
} BATTERY_PROFILE_STRUC, *BATTERY_PROFILE_STRUC_P;

typedef struct _R_PROFILE_STRUC
{
    kal_int32 resistance; // Ohm
    kal_int32 voltage;
} R_PROFILE_STRUC, *R_PROFILE_STRUC_P;

typedef enum
{
    T1_0C,
    T2_25C,
    T3_50C
} PROFILE_TEMPERATURE;

// ============================================================
// External Variables
// ============================================================

// ============================================================
// External function
// ============================================================

// ============================================================
// <DOD, Battery_Voltage> Table
// ============================================================
#if (BAT_NTC_10 == 1)
    BATT_TEMPERATURE Batt_Temperature_Table[] = {
        {-20,68237},
        {-15,53650},
        {-10,42506},
        { -5,33892},
        {  0,27219},
        {  5,22021},
        { 10,17926},
        { 15,14674},
        { 20,12081},
        { 25,10000},
        { 30,8315},
        { 35,6948},
        { 40,5834},
        { 45,4917},
        { 50,4161},
        { 55,3535},
        { 60,3014},
        //Gionee: zhouzq 2013-11-06 start modify for CR00957700
        #if defined(CONFIG_GN_MTK_BSP_THERMAL_LIMIT_65_SUPPORT)
        { 65,2586}
        #endif
        //Gionee: zhouzq 2013-11-06 end modify for CR00957700
    };
#endif

#if (BAT_NTC_47 == 1)
    BATT_TEMPERATURE Batt_Temperature_Table[] = {
        {-20,483954},
        {-15,360850},
        {-10,271697},
        { -5,206463},
        {  0,158214},
        {  5,122259},
        { 10,95227},
        { 15,74730},
        { 20,59065},
        { 25,47000},
        { 30,37643},
        { 35,30334},
        { 40,24591},
        { 45,20048},
        { 50,16433},
        { 55,13539},
        { 60,11210},
//Gionee: zhouzq 2013-11-06 start modify for CR00957700
#if defined(CONFIG_GN_MTK_BSP_THERMAL_LIMIT_65_SUPPORT)
        { 65,9328}
#endif
//Gionee: zhouzq 2013-11-06 end modify for CR00957700        
    };
#endif

// T0 -10C
BATTERY_PROFILE_STRUC battery_profile_t0[] =
{
	{0	 , 4362},
	{2	 , 4331},
	{4	 , 4301},
	{5	 , 4265},
	{7	 , 4224},
	{9	 , 4191},
	{11  , 4168},
	{13  , 4148},
	{15  , 4129},
	{16  , 4111},
	{18  , 4095},
	{20  , 4079},
	{22  , 4059},
	{24  , 4036},
	{26  , 4013},
	{27  , 3993},
	{29  , 3976},
	{31  , 3965},
	{33  , 3946},
	{35  , 3928},
	{37  , 3911},
	{38  , 3896},
	{40  , 3882},
	{42  , 3870},
	{44  , 3860},
	{46  , 3850},
	{47  , 3841},
	{49  , 3833},
	{51  , 3825},
	{53  , 3818},
	{55  , 3812},
	{57  , 3806},
	{58  , 3802},
	{60  , 3798},
	{62  , 3795},
	{64  , 3792},
	{66  , 3789},
	{68  , 3786},
	{69  , 3783},
	{71  , 3778},
	{73  , 3773},
	{75  , 3767},
	{77  , 3760},
	{78  , 3753},
	{80  , 3744},
	{82  , 3735},
	{84  , 3725},
	{86  , 3715},
	{88  , 3705},
	{89  , 3697},
	{91  , 3691},
	{93  , 3684},
	{95  , 3666},
	{97  , 3615},
	{99  , 3518},
	{100 , 3440},
	{100 , 3400},
	{100 , 3383},
	{100 , 3332},
	{100 , 3285},
	{100 , 3244},
	{100 , 3210},
	{100 , 3183} 
};      
        
// T1 0C 
BATTERY_PROFILE_STRUC battery_profile_t1[] =
{
	{0	 , 4372},
	{2	 , 4345},
	{3	 , 4323},
	{5	 , 4302},
	{7	 , 4282},
	{9	 , 4263},
	{10  , 4244},
	{12  , 4225},
	{14  , 4207},
	{15  , 4188},
	{17  , 4170},
	{19  , 4153},
	{21  , 4135},
	{22  , 4118},
	{24  , 4100},
	{26  , 4085},
	{27  , 4072},
	{29  , 4056},
	{31  , 4032},
	{33  , 4007},
	{34  , 3986},
	{36  , 3970},
	{38  , 3954},
	{40  , 3938},
	{41  , 3923},
	{43  , 3908},
	{45  , 3895},
	{46  , 3883},
	{48  , 3872},
	{50  , 3862},
	{52  , 3853},
	{53  , 3845},
	{55  , 3837},
	{57  , 3830},
	{58  , 3823},
	{60  , 3816},
	{62  , 3810},
	{64  , 3804},
	{65  , 3799},
	{67  , 3793},
	{69  , 3789},
	{70  , 3784},
	{72  , 3780},
	{74  , 3775},
	{76  , 3770},
	{77  , 3764},
	{79  , 3758},
	{81  , 3751},
	{82  , 3741},
	{84  , 3732},
	{86  , 3722},
	{88  , 3710},
	{89  , 3701},
	{91  , 3698},
	{93  , 3694},
	{95  , 3688},
	{96  , 3655},
	{98  , 3575},
	{99  , 3451},
	{100 , 3400},
	{100 , 3199},
	{100 , 3092},
	{100 , 3051} 
};           

// T2 25C
BATTERY_PROFILE_STRUC battery_profile_t2[] =
{
	{0	 , 4389},
	{2	 , 4368},
	{3	 , 4349},
	{5	 , 4330},
	{7	 , 4311},
	{8	 , 4292},
	{10  , 4273},
	{11  , 4254},
	{13  , 4235},
	{15  , 4217},
	{16  , 4198},
	{18  , 4180},
	{20  , 4162},
	{21  , 4144},
	{23  , 4126},
	{24  , 4109},
	{26  , 4092},
	{28  , 4075},
	{29  , 4060},
	{31  , 4044},
	{33  , 4026},
	{34  , 4008},
	{36  , 3995},
	{38  , 3981},
	{39  , 3968},
	{41  , 3953},
	{42  , 3936},
	{44  , 3916},
	{46  , 3899},
	{47  , 3885},
	{49  , 3873},
	{51  , 3863},
	{52  , 3854},
	{54  , 3845},
	{55  , 3838},
	{57  , 3830},
	{59  , 3823},
	{60  , 3817},
	{62  , 3811},
	{64  , 3805},
	{65  , 3800},
	{67  , 3794},
	{69  , 3790},
	{70  , 3785},
	{72  , 3779},
	{73  , 3771},
	{75  , 3764},
	{77  , 3757},
	{78  , 3750},
	{80  , 3744},
	{82  , 3738},
	{83  , 3730},
	{85  , 3720},
	{86  , 3712},
	{88  , 3699},
	{90  , 3692},
	{91  , 3690},
	{93  , 3687},
	{95  , 3682},
	{96  , 3648},
	{98  , 3574},
	{99  , 3462},
	{100 , 3400} 
};     

// T3 50C
BATTERY_PROFILE_STRUC battery_profile_t3[] =
{
	{0	 , 4393},
	{2	 , 4370},
	{3	 , 4350},
	{5	 , 4331},
	{7	 , 4312},
	{8	 , 4293},
	{10  , 4274},
	{11  , 4255},
	{13  , 4236},
	{15  , 4218},
	{16  , 4199},
	{18  , 4181},
	{20  , 4163},
	{21  , 4145},
	{23  , 4127},
	{25  , 4110},
	{26  , 4092},
	{28  , 4076},
	{29  , 4059},
	{31  , 4044},
	{33  , 4028},
	{34  , 4013},
	{36  , 3998},
	{38  , 3984},
	{39  , 3970},
	{41  , 3956},
	{42  , 3940},
	{44  , 3920},
	{46  , 3900},
	{47  , 3886},
	{49  , 3875},
	{51  , 3865},
	{52  , 3855},
	{54  , 3847},
	{56  , 3838},
	{57  , 3831},
	{59  , 3823},
	{60  , 3816},
	{62  , 3810},
	{64  , 3804},
	{65  , 3798},
	{67  , 3793},
	{69  , 3788},
	{70  , 3782},
	{72  , 3771},
	{74  , 3759},
	{75  , 3751},
	{77  , 3743},
	{78  , 3735},
	{80  , 3729},
	{82  , 3723},
	{83  , 3715},
	{85  , 3705},
	{87  , 3696},
	{88  , 3684},
	{90  , 3677},
	{92  , 3676},
	{93  , 3673},
	{95  , 3667},
	{96  , 3629},
	{98  , 3556},
	{99  , 3444},
	{100 , 3400} 	       
};           

// battery profile for actual temperature. The size should be the same as T1, T2 and T3
BATTERY_PROFILE_STRUC battery_profile_temperature[] =
{
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 }
};    

// ============================================================
// <Rbat, Battery_Voltage> Table
// ============================================================
// T0 -10C
R_PROFILE_STRUC r_profile_t0[] =
{
	{830  , 4362},
	{830  , 4331},
	{856  , 4301},
	{872  , 4265},
	{923  , 4224},
	{1010 , 4191},
	{1038 , 4168},
	{1041 , 4148},
	{1039 , 4129},
	{1037 , 4111},
	{1040 , 4095},
	{1046 , 4079},
	{1043 , 4059},
	{1031 , 4036},
	{1022 , 4013},
	{1016 , 3993},
	{1020 , 3976},
	{1030 , 3965},
	{869  , 3946},
	{1016 , 3928},
	{1011 , 3911},
	{1006 , 3896},
	{1003 , 3882},
	{1002 , 3870},
	{1003 , 3860},
	{1006 , 3850},
	{1008 , 3841},
	{1013 , 3833},
	{1017 , 3825},
	{1021 , 3818},
	{1027 , 3812},
	{1034 , 3806},
	{1039 , 3802},
	{1047 , 3798},
	{1057 , 3795},
	{1072 , 3792},
	{1088 , 3789},
	{1107 , 3786},
	{1132 , 3783},
	{1159 , 3778},
	{1190 , 3773},
	{1225 , 3767},
	{1264 , 3760},
	{1305 , 3753},
	{1347 , 3744},
	{1388 , 3735},
	{1428 , 3725},
	{1471 , 3715},
	{1511 , 3705},
	{1552 , 3697},
	{1596 , 3691},
	{1644 , 3684},
	{1692 , 3666},
	{1730 , 3615},
	{1791 , 3518},
	{1727 , 3440},
	{1584 , 3383},
	{1458 , 3332},
	{1342 , 3285},
	{1238 , 3244},
	{1155 , 3210},
	{1089 , 3183},
	{1041 , 3164} 	       
};      

// T1 0C
R_PROFILE_STRUC r_profile_t1[] =
{
	{394 , 4372},
	{394 , 4345},
	{404 , 4323},
	{408 , 4302},
	{410 , 4282},
	{410 , 4263},
	{411 , 4244},
	{412 , 4225},
	{412 , 4207},
	{413 , 4188},
	{413 , 4170},
	{414 , 4153},
	{415 , 4135},
	{417 , 4118},
	{419 , 4100},
	{421 , 4085},
	{432 , 4072},
	{434 , 4056},
	{425 , 4032},
	{418 , 4007},
	{417 , 3986},
	{416 , 3970},
	{412 , 3954},
	{404 , 3938},
	{394 , 3923},
	{388 , 3908},
	{383 , 3895},
	{382 , 3883},
	{382 , 3872},
	{385 , 3862},
	{386 , 3853},
	{389 , 3845},
	{392 , 3837},
	{395 , 3830},
	{398 , 3823},
	{404 , 3816},
	{406 , 3810},
	{411 , 3804},
	{414 , 3799},
	{419 , 3793},
	{422 , 3789},
	{425 , 3784},
	{428 , 3780},
	{432 , 3775},
	{435 , 3770},
	{438 , 3764},
	{444 , 3758},
	{454 , 3751},
	{464 , 3741},
	{476 , 3732},
	{492 , 3722},
	{505 , 3710},
	{524 , 3701},
	{545 , 3698},
	{575 , 3694},
	{614 , 3688},
	{644 , 3655},
	{683 , 3575},
	{765 , 3451},
	{992 , 3199},
	{860 , 3092},
	{761 , 3051},
	{697 , 3027} 
};     

// T2 25C
R_PROFILE_STRUC r_profile_t2[] =
{
	{119 , 4389},
	{119 , 4368},
	{118 , 4349},
	{118 , 4330},
	{119 , 4311},
	{119 , 4292},
	{119 , 4273},
	{118 , 4254},
	{119 , 4235},
	{120 , 4217},
	{120 , 4198},
	{122 , 4180},
	{123 , 4162},
	{123 , 4144},
	{124 , 4126},
	{126 , 4109},
	{127 , 4092},
	{131 , 4075},
	{136 , 4060},
	{138 , 4044},
	{136 , 4026},
	{141 , 4008},
	{143 , 3995},
	{149 , 3981},
	{151 , 3968},
	{150 , 3953},
	{143 , 3936},
	{131 , 3916},
	{121 , 3899},
	{115 , 3885},
	{113 , 3873},
	{113 , 3863},
	{114 , 3854},
	{114 , 3845},
	{115 , 3838},
	{115 , 3830},
	{116 , 3823},
	{117 , 3817},
	{118 , 3811},
	{120 , 3805},
	{122 , 3800},
	{123 , 3794},
	{125 , 3790},
	{125 , 3785},
	{123 , 3779},
	{119 , 3771},
	{115 , 3764},
	{113 , 3757},
	{112 , 3750},
	{113 , 3744},
	{114 , 3738},
	{114 , 3730},
	{114 , 3720},
	{116 , 3712},
	{115 , 3699},
	{113 , 3692},
	{117 , 3690},
	{123 , 3687},
	{130 , 3682},
	{126 , 3648},
	{133 , 3574},
	{151 , 3462},
	{227 , 3244} 
}; 

// T3 50C
R_PROFILE_STRUC r_profile_t3[] =
{
	{72  , 4393},
	{72  , 4370},
	{72  , 4350},
	{72  , 4331},
	{72  , 4312},
	{72  , 4293},
	{73  , 4274},
	{73  , 4255},
	{73  , 4236},
	{74  , 4218},
	{75  , 4199},
	{75  , 4181},
	{75  , 4163},
	{76  , 4145},
	{77  , 4127},
	{78  , 4110},
	{78  , 4092},
	{79  , 4076},
	{81  , 4059},
	{82  , 4044},
	{83  , 4028},
	{85  , 4013},
	{87  , 3998},
	{90  , 3984},
	{93  , 3970},
	{95  , 3956},
	{96  , 3940},
	{87  , 3920},
	{77  , 3900},
	{75  , 3886},
	{74  , 3875},
	{74  , 3865},
	{74  , 3855},
	{74  , 3847},
	{75  , 3838},
	{76  , 3831},
	{76  , 3823},
	{77  , 3816},
	{78  , 3810},
	{80  , 3804},
	{81  , 3798},
	{83  , 3793},
	{85  , 3788},
	{86  , 3782},
	{79  , 3771},
	{74  , 3759},
	{76  , 3751},
	{76  , 3743},
	{75  , 3735},
	{75  , 3729},
	{76  , 3723},
	{76  , 3715},
	{75  , 3705},
	{76  , 3696},
	{75  , 3684},
	{73  , 3677},
	{76  , 3676},
	{80  , 3673},
	{84  , 3667},
	{80  , 3629},
	{88  , 3556},
	{95  , 3444},
	{147 , 3223} 
}; 

// r-table profile for actual temperature. The size should be the same as T1, T2 and T3
R_PROFILE_STRUC r_profile_temperature[] =
{
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 },
	{0  , 0 }
};    

// ============================================================
// function prototype
// ============================================================
int fgauge_get_saddles(void);
BATTERY_PROFILE_STRUC_P fgauge_get_profile(kal_uint32 temperature);

int fgauge_get_saddles_r_table(void);
R_PROFILE_STRUC_P fgauge_get_profile_r_table(kal_uint32 temperature);

#endif	//#ifndef _CUST_BATTERY_METER_TABLE_H

