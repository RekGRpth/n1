#################GLOBAL_DEFINE BEGIN####################
TARGET_PRODUCT=full_gionee6753_65c_l1

GN_KERNEL_CFG_FILE_ENG=kernel-3.10/arch/arm64/configs/gionee6753_65c_l1_debug_defconfig
GN_KERNEL_CFG_FILE=kernel-3.10/arch/arm64/configs/gionee6753_65c_l1_defconfig

AUTO_ADD_GLOBAL_DEFINE_BY_NAME+= CONFIG_GN_BSP_PS_STATIC_CALIBRATION
AUTO_ADD_GLOBAL_DEFINE_BY_NAME_VALUE+= CONFIG_GN_BSP_MTK CONFIG_GN_BSP_AUDIO_SUPPORT CONFIG_GN_BSP_GNTORCH_SUPPORT MTK_LCM_PHYSICAL_ROTATION_HW GN_CYWEE_SENSORHUB_SUPPORT CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT MTK_ULTRA_DIMMING_SUPPORT CONFIG_GN_BSP_MTK_NXP_RESET_SUPPORT CONFIG_GN_BSP_AMIGO_CHARGING_SUPPORT CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT
AUTO_ADD_GLOBAL_DEFINE_BY_VALUE+= 
#################GLOBAL_DEFINE END######################

#################BSP BEGIN##############################
CONFIG_GN_BSP_MTK=yes

#################PL BEGIN###############################
# Gionee BSP1 add BQ24192 bedin
MTK_BQ24296_SUPPORT=yes
CONFIG_GN_BSP_MTK_BQ24296_SUPPORT=yes
CONFIG_MTK_PUMP_EXPRESS_PLUS_SUPPORT=no
# Gionee BSP1 add BQ24192 end
#################PL END#################################

#################LK BEGIN###############################
# Gionee BSP1 yangqb modify for CBL7503 basecode start
CUSTOM_LK_LCM="gn_smd_s6e8aa5"
BOOT_LOGO := hd720
# Gionee BSP1 yangqb modify for CBL7503 basecode end

#################LK END#################################

#################KERNEL BEGIN###########################
# Gionee BSP1 yangqb modify for CBL7503 basecode start
CONFIG_CUSTOM_KERNEL_LCM="gn_smd_s6e8aa5"
CONFIG_LCM_HEIGHT="1280"
CONFIG_LCM_WIDTH="720"
CONFIG_MTK_AAL_SUPPORT=yes
CONFIG_MTK_LCM_PHYSICAL_ROTATION_HW=yes
CONFIG_GN_BSP_MTK_AMOLED_FEATURE_SUPPORT=yes

CONFIG_GN_BSP_MTK_DEVICE_CHECK=yes

CONFIG_MTK_BQ24296_SUPPORT=yes

CONFIG_MTK_NFC=no
CONFIG_NFC_MT6605=no
CONFIG_HID_GYRATION=no
# Gionee BSP1 yangqb modify for CBL7503 basecode end

# Gionee BSP1 liudj modify for battery begin
CONFIG_GN_BSP_MTK_THERMAL_LIMIT_65_SUPPORT=yes
CONFIG_GN_BSP_MTK_DC_TO_DC_CHARGE=yes
CONFIG_GN_BSP_MTK_DISABLE_CHARGING_CONTROL=yes
CONFIG_GN_BSP_MTK_HV_RECHARGE_SUPPORT=yes
CONFIG_GN_BSP_MTK_LOWTEMP_WARNING_SUPPORT=yes
CONFIG_GN_BSP_AMIGO_CHARGING_SUPPORT=yes
CONFIG_GN_BSP_IUNI_CHARGING_SUPPORT=yes
CONFIG_GN_BSP_MTK_TEMPERATURE_RANGE_WIDE=yes
CONFIG_GN_BSP_MTK_UI_SOC_NO_DECLINE=yes
# Gionee BSP1 liudj modify for battery end

# Gionee BSP1 liu_hao modify for gn_torch begin
CONFIG_GN_BSP_GNTORCH_SUPPORT=yes
# Gionee BSP1 liu_hao modify for gn_torch end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_GN_BSP_MTK_LED_AWINIC_AW2013=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_GN_BSP_MTK_HALL_KEY=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

# Gionee BSP1 yangqb 20150324 modify for CBL7503 basecode start
CONFIG_MTK_VIBRATOR=no
CONFIG_GN_MTK_BSP_IMMERSION=yes
# Gionee BSP1 yangqb 20150116 modify for CBL7503 basecode end

#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA begin
CONFIG_MTK_SOUND=yes
CUSTOM_KERNEL_SOUND=gn_nxp_tfa9887
#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA end

#Gionee litao 20150324 modify for camera begin
CONFIG_CUSTOM_KERNEL_IMGSENSOR="s5k3m2_mipi_raw s5k3m2_ofilm_ojlbp01 ov8858R2A_mipi_raw"
#Gionee litao 20150324 modify for camera end

#Gionee yaoyc 20150407 add for synaptics tp begin
CONFIG_TOUCHSCREEN_SYNAPTICS_S3508=no
CONFIG_TOUCHSCREEN_MTK_GT1151=yes
#Gionee yaoyc 20150407 add for synaptics tp end

#Gionee BSP1 chuqf for unique usb id begin
CONFIG_MTK_USB_UNIQUE_SERIAL=yes
#Gionee BSP1 chuqf for unique usb id end
#Gionee BSP1 chuqf 20141216 add for CR01426061 mtk_shared_sdcard begin
CONFIG_MTK_SHARED_SDCARD=yes
#Gionee BSP1 chuqf 20141216 add for CR01426061 mtk_shared_sdcard end
#################KERNEL END#############################

#################BSP END################################

#################ADNROID BEGIN##########################
# Gionee BSP1 yangqb modify for GBL7318 basecode start
LCM_HEIGHT=1280
LCM_WIDTH=720
BOOT_LOGO=hd720
MTK_AAL_SUPPORT=yes
MTK_LCM_PHYSICAL_ROTATION_HW=yes
MTK_ULTRA_DIMMING_SUPPORT=yes
# Gionee BSP1 yangqb modify for GBL7318 basecode end

# Gionee BSP1 yangqb modify for GBL7503 basecode start
MTK_IPOH_SUPPORT = no
MTK_IPO_SUPPORT = no
# Gionee BSP1 yangqb modify for GBL7503 basecode end

#Gionee litao 20150324 modify for camera begin
CUSTOM_HAL_IMGSENSOR = s5k3m2_mipi_raw s5k3m2_ofilm_ojlbp01 ov8858R2A_mipi_raw
CUSTOM_HAL_MAIN_IMGSENSOR = s5k3m2_mipi_raw s5k3m2_ofilm_ojlbp01
CUSTOM_HAL_SUB_IMGSENSOR = ov8858R2A_mipi_raw
CUSTOM_KERNEL_IMGSENSOR = s5k3m2_mipi_raw s5k3m2_ofilm_ojlbp01 ov8858R2A_mipi_raw
CUSTOM_KERNEL_MAIN_IMGSENSOR = s5k3m2_mipi_raw s5k3m2_ofilm_ojlbp01
CUSTOM_KERNEL_SUB_IMGSENSOR = ov8858R2A_mipi_raw
#Gionee litao 20150324 modify for camera end

#Gionee zhaocuiqin 20150630 modify for camera af begin
#AF start
CONFIG_MTK_LENS_DW9714AF_SUPPORT=yes

CUSTOM_HAL_LENS=dummy_lens dw9714af
CUSTOM_HAL_MAIN_BACKUP_LENS=dummy_lens
CUSTOM_HAL_SUB_BACKUP_LENS=dummy_lens
CUSTOM_HAL_MAIN_LENS=dw9714af
CUSTOM_HAL_SUB_LENS=dummy_lens 

CUSTOM_KERNEL_LENS=dummy_lens dw9714af
CUSTOM_KERNEL_MAIN_BACKUP_LENS=dummy_lens
CUSTOM_KERNEL_SUB_BACKUP_LENS=dummy_lens
CUSTOM_KERNEL_MAIN_LENS=dw9714af
CUSTOM_KERNEL_SUB_LENS=dummy_lens 
#AF end

CONFIG_CUSTOM_KERNEL_FLASHLIGHT="constant_flashlight"
CUSTOM_HAL_FLASHLIGHT=constant_flashlight
#Gionee zhaocuiqin 20150630 modify for camera af end

#Gionee yanghanming  20150306
MTK_LTE_SUPPORT=yes
MTK_ENABLE_MD1=yes
MTK_MD1_SUPPORT=6
MTK_SHARE_MODEM_CURRENT=2
MTK_SHARE_MODEM_SUPPORT=2
MTK_WORLD_PHONE=yes
MTK_UMTS_TDD128_MODE=yes
CUSTOM_MODEM=CBL7503A01_A_LTTG_MODEM CBL7503A01_A_LWG_MODEM
MTK_SIM_HOT_SWAP = yes
MTK_SIM_HOT_SWAP_COMMON_SLOT = yes
OPTR_SPEC_SEG_DEF = NONE
#Gionee yanghanming  20150306

#Gionee BSP1 yang_yang modify for CBL7503 sensor start
MTK_SENSOR_SUPPORT=no
CONFIG_MTK_SENSOR_SUPPORT=no
CONFIG_IIO=yes
CONFIG_IIO_BUFFER=yes
CONFIG_IIO_TRIGGER=yes
CONFIG_IIO_BUFFER_CB=yes
CONFIG_IIO_TRIGGERED_BUFFER=yes
CONFIG_IIO_KFIFO_BUF=yes
CONFIG_INPUT_POLLDEV=yes
CONFIG_CUSTOM_KERNEL_SENSORHUB="ST"
GN_CYWEE_SENSORHUB_SUPPORT=yes
CONFIG_GN_BSP_PS_STATIC_CALIBRATION=yes
#Gionee BSP1 yang_yang modify for CBL7503 sensor end

#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA begin
NXP_SMARTPA_SUPPORT=tfa9890
CONFIG_GN_BSP_AUDIO_SUPPORT=yes
CONFIG_GN_BSP_MTK_NXP_RESET_SUPPORT=yes
#Gionee BSP1 zhangliu 2015-03-25 add for Smart PA end

#Gionee BSP1 zhangliu 2015-05-05 add for Audio begin
MTK_DUAL_MIC_SUPPORT=no
MTK_INCALL_NORMAL_DMNR=no
MTK_BESLOUDNESS_SUPPORT=no
MTK_MAGICONFERENCE_SUPPORT=no
MTK_AUDIO_MIC_INVERSE=no
#Gionee BSP1 zhangliu 2015-05-05 add for Audio end

# Gionee BSP1 yangqb modify for GBL7503 basecode start
MTK_NFC_SUPPORT=no
MTK_BEAM_PLUS_SUPPORT=no
MTK_NFC_ADDON_SUPPORT=no
MTK_NFC_FW_MT6605=no
MTK_NFC_HCE_SUPPORT=no
MTK_NFC_MT6605=no
MTK_NFC_OMAAC_GEMALTO=no
MTK_NFC_OMAAC_SUPPORT=no
MTK_WIFIWPSP2P_NFC_SUPPORT=no
MTK_NFC_GSMA_SUPPORT=no
# Gionee BSP1 yangqb modify for GBL703 basecode end
#Gionee BSP1 chuqf 20141216 add for CR01426061 mtk_shared_sdcard begin
MTK_SHARED_SDCARD=yes
#Gionee BSP1 chuqf 20141216 add for CR01426061 mtk_shared_sdcard end
#################ADNROID END############################

################################ APP BEGIN ################################
#################### GiONEE MTK APP CONFIG BEGIN ####################
#MTK_PRODUCT_LOCALES = zh_CN en_US
MTK_PRODUCT_LOCALES = en_US zh_CN zh_TW es_ES pt_BR ru_RU fr_FR de_DE tr_TR vi_VN ms_MY in_ID th_TH it_IT ar_EG hi_IN bn_IN ur_PK fa_IR pt_PT nl_NL el_GR hu_HU tl_PH ro_RO cs_CZ ko_KR km_KH iw_IL my_MM pl_PL es_US bg_BG hr_HR lv_LV lt_LT sk_SK uk_UA de_AT da_DK fi_FI nb_NO sv_SE en_GB hy_AM zh_HK et_EE ja_JP kk_KZ sr_RS sl_SI ca_ES
GN_RODUCT_AAPT_CONFIG = hdpi xhdpi xxhdpi
#################### GiONEE MTK APP CONFIG END ######################
GN_APK_MMITEST_SUPPORT = yes
GN_RW_GN_MMI_LTETDDANT = no
GN_RW_GN_MMI_KEYTEST_CAMERA = no
GN_RW_GN_MMI_KEYTEST_FOCUS = no
GN_RW_GN_MMI_KEYTEST_SEARCH = no
GN_RW_GN_MMI_KEYTEST_MENU = no
GN_RW_GN_MMI_TP_CROSS = no
GN_RW_GN_MMI_FLASH = no
GN_RW_GN_MMI_FLASH2 = no
GN_RW_GN_MMI_FINGERPRINTS = no
GN_RW_GN_MMI_TP_TEN = no
GN_RW_GN_MMI_IRTEST = no
GN_RW_GN_MMI_NFC = no
GN_RW_GN_MMI_NFC2 = no
GN_RW_GN_MMI_DOUBLETONE = no
GN_RW_GN_MMI_SETCOLOR = no
GN_RW_GN_MMI_FLASHLIGHT = no
################################## APP END ################################


###add by iuni hujianwei 20150604 start
GN3RD_GMS_SUPPORT = 64_5_1_r1
#GN3RD_GMS_APPLICATION = Phonesky Velvet SetupWizard OnlyChrome Drive Gmail Maps Music2 PlusOne Street Videos YouTube LatinImeGoogle Hangouts
GN3RD_GMS_APPLICATION = SetupWizard Phonesky Velvet OnlyChrome Drive Gmail Maps Music2  Street Videos YouTube LatinImeGoogle Hangouts

###add by iuni hujianwei 20150604 end
